﻿using System.Configuration;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Sodakoq.Domain.Models.Entities;

namespace Sodakoq.Domain.Models.Contexts
{
    public class CvManagementDbContext : DbContext
    {
        #region Properties

        /// <summary>
        ///     Key-value cache items.
        /// </summary>
        public virtual DbSet<KeyValueCacheItem> KeyValueCacheItems { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //base.OnModelCreating(dbModelBuilder);

            // Disable cascade delete.
            // This is for remove pluralization naming convention in database defined by Entity Framework.
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
                entity.Relational().TableName = entity.DisplayName();

            // Disable cascade delete.
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
                relationship.DeleteBehavior = DeleteBehavior.Restrict;

            // Execute entity configuration.
            modelBuilder.ApplyConfigurationsFromAssembly(
                typeof(EntityConfigurations.KeyValueCacheItemConfiguration).Assembly);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["MvvmStructureDbContext"].ConnectionString;
            optionsBuilder.UseSqlite(connectionString);
        }

        #endregion
    }
}