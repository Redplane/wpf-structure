﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sodakoq.Domain.Models.Entities;

namespace Sodakoq.Domain.Models.EntityConfigurations
{
    public class KeyValueCacheItemConfiguration : IEntityTypeConfiguration<KeyValueCacheItem>
    {
        #region Methods

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="entity"></param>
        public void Configure(EntityTypeBuilder<KeyValueCacheItem> entity)
        {
            entity.HasKey(x => x.Key);
            entity
                .Property(x => x.Key)
                .IsRequired();
        }

        #endregion
    }
}