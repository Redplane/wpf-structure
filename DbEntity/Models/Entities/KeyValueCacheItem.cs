﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Sodakoq.Domain.Models.Entities
{
    public class KeyValueCacheItem
    {
        #region Properties

        [Required]
        public string Key { get; set; }

        public string Value { get; set; }

        public DateTime? ExpiredTime { get; set; }

        #endregion

        #region Constructor

        public KeyValueCacheItem()
        {
        }

        public KeyValueCacheItem(string key, string value, DateTime? expiredTime = null)
        {
            Key = key;
            Value = value;
            ExpiredTime = expiredTime;
        }

        #endregion
    }
}