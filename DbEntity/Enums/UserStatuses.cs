﻿namespace Sodakoq.Domain.Enums
{
    public enum UserStatuses
    {
        Disabled,
        Pending,
        Active
    }
}