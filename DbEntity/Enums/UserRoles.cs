﻿namespace Sodakoq.Domain.Enums
{
    public enum UserRoles
    {
        User,
        Admin
    }
}