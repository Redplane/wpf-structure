﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Sodakoq.Infrastructure.Converters
{
    public class IntToGridLengthConverter : IValueConverter
    {
        /// <summary>
        ///     Convert int to GridLength.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return GridLength.Auto;

            var iValue = (int) value;
            if (iValue < 1)
                return GridLength.Auto;

            return new GridLength(iValue, GridUnitType.Pixel);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}