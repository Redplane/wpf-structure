﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Sodakoq.Infrastructure.Converters
{
    public class DateTimeToStringConverter : IValueConverter
    {
        #region Properties

        public string Format { get; set; }

        #endregion

        #region Constructor

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public virtual object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is DateTime utcTime))
                return string.Empty;

            if (string.IsNullOrEmpty(Format))
                return utcTime.ToString("D");

            return utcTime.ToString(Format);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}