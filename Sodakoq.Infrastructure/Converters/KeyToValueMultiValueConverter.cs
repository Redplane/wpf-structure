﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows.Data;

namespace Sodakoq.Infrastructure.Converters
{
    public class KeyToValueMultiValueConverter : IMultiValueConverter
    {
        /// <summary>
        ///     Retrieve values by search for dictionary key.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length != 2)
                throw new Exception(
                    $"{nameof(KeyToValueMultiValueConverter)} requires 2 parameters. Key - Dictionary.");

            var key = values[0];
            var dictionaryParameter = values[1];

            if (!(dictionaryParameter is IDictionary dictionary))
                throw new Exception("Dictionary is required");

            // No value is found for this key.
            if (dictionary.Contains(key))
                return null;

            return dictionary[key];
        }

        /// <summary>
        ///     Throws exception due to lack of implementation
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetTypes"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}