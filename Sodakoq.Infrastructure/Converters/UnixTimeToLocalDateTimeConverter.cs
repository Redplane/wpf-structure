﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Sodakoq.Infrastructure.Converters
{
    public class UnixTimeToLocalDateTimeConverter : IValueConverter
    {
        #region Constructor

        /// <summary>
        ///     Utc date time.
        /// </summary>
        private readonly DateTime _baseUtcDateTime;

        #endregion

        #region Constructor

        public UnixTimeToLocalDateTimeConverter()
        {
            _baseUtcDateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Convert unix time to date time.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            // Cast the value to double.
            try
            {
                var dValue = (double) value;
                var time = _baseUtcDateTime.AddMilliseconds(dValue).ToLocalTime();
                return time;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Convert back datetime to unix time.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is DateTime dateTime))
                return null;

            var utcTime = dateTime.ToUniversalTime();
            return (utcTime - _baseUtcDateTime).TotalMilliseconds;
        }

        #endregion
    }
}