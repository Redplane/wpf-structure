﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows.Data;

namespace Sodakoq.Infrastructure.Converters
{
    public class IsInDictionaryValueMultiValueConverter : IMultiValueConverter
    {
        /// <summary>
        ///     Check whether item is in dictionary value or not.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length != 3)
                throw new Exception(
                    $"{nameof(KeyToValueMultiValueConverter)} requires 3 parameters. Item - Key - Dictionary.");

            var item = values[0];
            var key = values[1];
            var dictionaryParameter = values[2];

            if (!(dictionaryParameter is IDictionary dictionary))
                throw new Exception("Dictionary is required");

            // Item is invalid.
            if (item == null)
                return false;

            // No value is found for this key.
            if (!dictionary.Contains(key))
                return false;

            var dictionaryValues = dictionary.Values;
            if (!(dictionaryValues is IList valuesList))
                return false;

            return valuesList.Contains(item);
        }

        /// <summary>
        ///     Throws exception due to lack of implementation.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetTypes"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}