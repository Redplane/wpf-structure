﻿using System;
using System.Collections;
using System.Globalization;
using System.Windows.Data;

namespace Sodakoq.Infrastructure.Converters
{
    public class IsInListMultiValueConverter : IMultiValueConverter
    {
        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="values"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var item = values[0];

            if (!(values[1] is IList collection))
                return false;

            return collection.Contains(item);
        }

        /// <summary>
        ///     Throws not implemented exception due to lack of implementation.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetTypes"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}