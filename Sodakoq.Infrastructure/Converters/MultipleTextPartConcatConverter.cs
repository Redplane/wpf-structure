﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Sodakoq.Infrastructure.Converters
{
    public class MultipleTextPartConcatConverter : IMultiValueConverter
    {
        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="values"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length < 1)
                return null;

            if (!(values is string[] textParts))
                return null;

            return string.Join(" - ", values);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}