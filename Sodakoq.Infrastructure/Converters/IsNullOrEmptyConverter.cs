﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Sodakoq.Infrastructure.Converters
{
    public class IsNullOrEmptyConverter : IValueConverter
    {
        /// <summary>
        ///     Convert object to boolean (IsNullOrEmpty)
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return true;

            if (!(value is string))
                return true;

            var keyword = (string) value;
            return string.IsNullOrEmpty(keyword);
        }

        /// <summary>
        ///     No implementation for reverse conversion.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}