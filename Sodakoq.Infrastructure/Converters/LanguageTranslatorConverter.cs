﻿using System;
using System.Globalization;
using System.Windows.Data;
using WPFLocalizeExtension.Engine;

namespace Sodakoq.Infrastructure.Converters
{
    public class LanguageTranslatorConverter : IValueConverter
    {
        /// <summary>
        ///     Convert a specific keyword
        /// </summary>
        /// <param name="source"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object source, Type targetType, object parameter, CultureInfo culture)
        {
            if (source == null || !(source is string))
                return source;

            var keyword = (string) source;
            var translatedItem = LocalizeDictionary.Instance
                .GetLocalizedObject(keyword, null, LocalizeDictionary.Instance.Culture);

            if (translatedItem == null)
                return source;

            return translatedItem.ToString();
        }

        /// <summary>
        ///     Not neccessary to implement this function.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}