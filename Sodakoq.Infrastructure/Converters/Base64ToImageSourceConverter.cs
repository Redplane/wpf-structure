﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Sodakoq.Infrastructure.Converters
{
    public class Base64ToImageSourceConverter : IValueConverter
    {
        /// <summary>
        ///     Convert base 64 encoded to image source.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Value is invalid.
            if (value == null)
                return null;

            // Value is not base-64 string.
            if (!(value is string))
                return null;

            try
            {
                var base64Content = (string) value;
                var bytes = System.Convert.FromBase64String(base64Content);
                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = new MemoryStream(bytes);
                bitmapImage.EndInit();

                return bitmapImage;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        ///     Conversion is one way, therefore, this method is not implemented.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}