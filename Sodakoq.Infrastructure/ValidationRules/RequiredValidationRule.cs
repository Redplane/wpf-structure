﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows.Controls;
using Mustache;
using WPFLocalizeExtension.Engine;

namespace Sodakoq.Infrastructure.ValidationRules
{
    public class RequiredValidationRule : ValidationRule
    {
        #region Properties

        /// <summary>
        ///     Name of validated field.
        /// </summary>
        public string FieldName { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // Translated message.
            var translatedMessage = "";

            var translatedGeneralValidationMessageInstance = LocalizeDictionary.Instance
                .GetLocalizedObject("MSG_SPECIFIC_FIELD_REQUIRED", null, LocalizeDictionary.Instance.Culture);

            if (translatedGeneralValidationMessageInstance == null)
            {
                translatedMessage = "Field is required";
            }
            else
            {
                var formatCompiler = new FormatCompiler();
                var generator = formatCompiler.Compile(translatedGeneralValidationMessageInstance.ToString());

                var keyValues = new Dictionary<string, object>();
                keyValues.Add(nameof(FieldName), FieldName);
                translatedMessage = generator.Render(keyValues);
            }

            if (value == null)
                return new ValidationResult(false, translatedMessage);

            if (value is string text)
                if (string.IsNullOrEmpty(text))
                    return new ValidationResult(false, translatedMessage);

            return new ValidationResult(true, "");
        }

        #endregion
    }
}