﻿namespace MainShared.ViewModels
{
    public class PaginationViewModel
    {
        #region Properties

        /// <summary>
        ///     Result page.
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        ///     Records per page.
        /// </summary>
        public int Records { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize model with no setting.
        /// </summary>
        public PaginationViewModel()
        {
        }

        /// <summary>
        ///     Initialize pagination model with settings.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="records"></param>
        public PaginationViewModel(int page, int records)
        {
            Page = page;
            Records = records;
        }

        #endregion
    }
}