﻿using System;
using MainShared.Enums;

namespace MainShared.ViewModels.Users
{
    public class UserViewModel
    {
        #region Properties

        public Guid Id { get; set; }

        public string FullName { get; set; }

        public string Photo { get; set; }

        public double Birthday { get; set; }

        public UserRoles Role { get; set; }

        public UserStatuses Status { get; set; }

        #endregion
    }
}