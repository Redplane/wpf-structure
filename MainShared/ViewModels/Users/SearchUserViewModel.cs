﻿using System.Collections.Generic;

namespace MainShared.ViewModels.Users
{
    public class SearchUserViewModel
    {
        #region Properties

        /// <summary>
        ///     Ids of users.
        /// </summary>
        public HashSet<int> Ids { get; set; }

        /// <summary>
        ///     List of first names.
        /// </summary>
        public HashSet<string> FirstNames { get; set; }

        /// <summary>
        ///     List of last names.
        /// </summary>
        public HashSet<string> LastNames { get; set; }

        /// <summary>
        ///     Birthday range.
        /// </summary>
        public RangeViewModel<double?, double?> Birthday { get; set; }

        /// <summary>
        ///     Pagination.
        /// </summary>
        public PaginationViewModel Pagination { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initialize search model.
        /// </summary>
        public SearchUserViewModel()
        {
        }

        /// <summary>
        ///     Initialize search model with settings.
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="firstNames"></param>
        /// <param name="lastNames"></param>
        /// <param name="birthday"></param>
        /// <param name="pagination"></param>
        public SearchUserViewModel(HashSet<int> ids, HashSet<string> firstNames = null, HashSet<string> lastNames = null,
            RangeViewModel<double?, double?> birthday = null, PaginationViewModel pagination = null)
        {
            Ids = ids;
            FirstNames = firstNames;
            LastNames = lastNames;
            Birthday = birthday;
            Pagination = pagination;
        }

        #endregion
    }
}