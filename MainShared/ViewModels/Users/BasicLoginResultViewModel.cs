﻿namespace MainShared.ViewModels.Users
{
    public class BasicLoginResultViewModel
    {
        #region Properties

        /// <summary>
        ///     Access token.
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        ///     Access token life time.
        /// </summary>
        public int AccessTokenLifeTime { get; set; }

        /// <summary>
        ///     Access token expired time.
        /// </summary>
        public double AccessTokenExpiredTime { get; set; }

        #endregion
    }
}