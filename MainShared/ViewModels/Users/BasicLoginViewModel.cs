﻿namespace MainShared.ViewModels.Users
{
    public class BasicLoginViewModel
    {
        #region Properties

        public string Email { get; set; }

        public string Password { get; set; }

        public string CaptchaCode { get; set; }

        #endregion

        #region Constructor

        public BasicLoginViewModel()
        {
        }

        public BasicLoginViewModel(string email, string password)
        {
            Email = email;
            Password = password;
        }

        public BasicLoginViewModel(string email, string password, string captchaCode)
        {
            Email = email;
            Password = password;
            CaptchaCode = captchaCode;
        }

        #endregion
    }
}