﻿using System;

namespace MainShared.ViewModels.Skills
{
    public class DeleteSkillViewModel
    {
        #region Properties

        /// <summary>
        ///     Whether skill existence should be wiped out ?
        /// </summary>
        public bool DeleteSkillExistence { get; set; } = false;

        /// <summary>
        ///     Id of user that his/her account and skill' relationship should be deleted.
        /// </summary>
        public Guid? UserId { get; set; }

        #endregion
    }
}