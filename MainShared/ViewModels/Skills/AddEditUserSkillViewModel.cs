﻿using System;

namespace MainShared.ViewModels.Skills
{
    public class AddEditUserSkillViewModel
    {
        #region Properties

        /// <summary>
        ///     Id of skill.
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        ///     Name of skill
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Skill point.
        /// </summary>
        public int Point { get; set; }

        /// <summary>
        ///     Id of skill category that skill belongs to.
        /// </summary>
        public Guid? SkillCategoryId { get; set; }

        #endregion
    }
}