﻿using System;
using System.Collections.Generic;

namespace MainShared.ViewModels.Skills
{
    public class LoadSkillViewModel
    {
        #region Properties

        public Guid? UserId { get; set; }

        public string SkillName { get; set; }

        public HashSet<Guid> SkillIds { get; set; }

        public PaginationViewModel Pagination { get; set; }

        #endregion
    }
}