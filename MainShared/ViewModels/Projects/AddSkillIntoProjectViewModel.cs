﻿using System;
using System.Collections.Generic;

namespace MainShared.ViewModels.Projects
{
    public class AddSkillIntoProjectViewModel
    {
        #region Properties

        /// <summary>
        /// Project id.
        /// </summary>
        public Guid ProjectId { get; set; }

        /// <summary>
        ///     List of existing skill ids that need adding to the project.
        /// </summary>
        public List<Guid> SkillIds { get; set; }

        #endregion
    }
}