﻿using System;
using System.Collections.Generic;

namespace MainShared.ViewModels.Projects
{
    public class AddEditProjectViewModel
    {
        #region Constructor

        public AddEditProjectViewModel()
        {
            Responsibilities = new List<string>();
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Project id.
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        ///     Project name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Project description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Time when project started.
        /// </summary>
        public double StartedTime { get; set; }

        /// <summary>
        ///     Time when project has been finished.
        /// </summary>
        public double? FinishedTime { get; set; }

        /// <summary>
        ///     List of responsibilities.
        /// </summary>
        public List<string> Responsibilities { get; set; }
        
        #endregion
    }
}