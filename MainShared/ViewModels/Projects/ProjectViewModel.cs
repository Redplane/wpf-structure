﻿using System;
using System.Collections.Generic;

namespace MainShared.ViewModels.Projects
{
    public class ProjectViewModel
    {
        #region Properties

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public double StartedTime { get; set; }

        public double? FinishedTime { get; set; }

        public List<string> Responsibilities { get; set; }

        #endregion
    }
}