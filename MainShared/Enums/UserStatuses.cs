﻿namespace MainShared.Enums
{
    public enum UserStatuses
    {
        Disabled,
        Pending,
        Active
    }
}