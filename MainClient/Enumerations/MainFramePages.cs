﻿namespace Sodakoq.Main.Enumerations
{
    public enum MainFramePages
    {
        ProjectManagement,
        SkillCategoryManagement,
        SkillManagement,
        ProfileManagement
    }
}