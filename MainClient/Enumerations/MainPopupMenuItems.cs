﻿namespace Sodakoq.Main.Enumerations
{
    public enum MainPopupMenuItems
    {
        None,
        Profile,
        SignOut
    }
}