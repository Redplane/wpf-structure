﻿namespace Sodakoq.Main.Enumerations
{
    public enum CaptchaKinds
    {
        None,
        Login,
        Register
    }
}