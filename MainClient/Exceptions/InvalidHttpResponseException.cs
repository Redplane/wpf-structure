﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace Sodakoq.Main.Exceptions
{
    public class InvalidHttpResponseException : Exception
    {
        #region Properties

        /// <summary>
        ///     Status code that thrown by http.
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; }

        #endregion

        #region Constructor

        public InvalidHttpResponseException(string message, HttpStatusCode httpStatusCode) : base(message)
        {
            HttpStatusCode = httpStatusCode;
        }

        public InvalidHttpResponseException(string message, HttpStatusCode httpStatusCode, Exception innerException) :
            base(message, innerException)
        {
            HttpStatusCode = httpStatusCode;
        }

        protected InvalidHttpResponseException(HttpStatusCode httpStatusCode, SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
            HttpStatusCode = httpStatusCode;
        }

        #endregion
    }
}