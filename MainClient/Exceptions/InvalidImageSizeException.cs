﻿using System;

namespace Sodakoq.Main.Exceptions
{
    public class InvalidImageSizeException : Exception
    {
        #region Properties

        public string InternalMessage { get; set; }

        #endregion

        #region Constructor

        public InvalidImageSizeException()
        {
        }

        public InvalidImageSizeException(string internalMessage)
        {
            InternalMessage = internalMessage;
        }

        #endregion
    }
}