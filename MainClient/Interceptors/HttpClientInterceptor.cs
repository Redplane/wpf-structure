﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Ioc;
using Sodakoq.Main.Constants;
using Sodakoq.Main.Exceptions;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Interceptors
{
    public class HttpClientInterceptor : HttpClientHandler
    {
        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="httpRequestMessage"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage httpRequestMessage,
            CancellationToken cancellationToken)
        {
            var localKeyValueCacheService = SimpleIoc.Default.GetInstance<IKeyValueCacheService>();
            var accessToken = localKeyValueCacheService.GetItem<string>(InMemoryKeyConstant.AccessToken);
            if (!string.IsNullOrEmpty(accessToken))
                httpRequestMessage.Headers.TryAddWithoutValidation("Authorization", $"Bearer {accessToken}");

            var httpResponseMessage = await base.SendAsync(httpRequestMessage, cancellationToken);
            if (httpResponseMessage.IsSuccessStatusCode)
                return httpResponseMessage;

            var szContent = await httpResponseMessage.Content.ReadAsStringAsync();
            throw new InvalidHttpResponseException(szContent, httpResponseMessage.StatusCode);
        }

        #endregion

        #region Constructor

        #endregion
    }
}