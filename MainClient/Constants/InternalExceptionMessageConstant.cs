﻿namespace Sodakoq.Main.Constants
{
    public class InternalExceptionMessageConstant
    {
        #region Properties

        /// <summary>
        ///     Invalid profile photo size.
        /// </summary>
        public const string ImageMustBeSquare = nameof(ImageMustBeSquare);

        #endregion
    }
}