﻿namespace Sodakoq.Main.Constants
{
    public class EventNameConstant
    {
        #region Properties

        public const string UpdateUserProfile = "update-user-profile";

        public const string UpdateMainViewTitle = "update-main-view-title";

        public const string OnMainFrameNavigated = "on-main-frame-navigated";

        /// <summary>
        ///     Event which will be fired when page is loaded.
        /// </summary>
        public const string OnMainFramePageLoaded = nameof(OnMainFramePageLoaded);

        /// <summary>
        ///     Event which will be fired when main frame is requested to be navigated.
        /// </summary>
        public const string NavigateMainFrame = nameof(NavigateMainFrame);

        /// <summary>
        ///     Event which will be fired when app frame is request to be navigated.
        /// </summary>
        public const string NavigateAppFrame = nameof(NavigateAppFrame);

        /// <summary>
        ///     Event which will be fired when app snack-bar is sent.
        /// </summary>
        public const string AddAppSnackbarMessage = nameof(AddAppSnackbarMessage);

        #endregion
    }
}