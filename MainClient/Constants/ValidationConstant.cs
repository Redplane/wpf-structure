﻿namespace Sodakoq.Main.Constants
{
    public class ValidationConstant
    {
        #region Properties

        /// <summary>
        ///     Normally, a project can contain only 30 skills.
        /// </summary>
        public const int MaxAvailableSkillsInProject = 30;

        #endregion
    }
}