﻿namespace Sodakoq.Main.Constants
{
    public class PaginationConstant
    {
        /// <summary>
        ///     Max users in user dashboard page.
        /// </summary>
        public const int MaxUsersInDashboard = 30;

        /// <summary>
        ///     Max projects in project dashboard page.
        /// </summary>
        public const int MaxProjectsInDashboard = 30;

        /// <summary>
        ///     Max skils in dashboard page.
        /// </summary>
        public const int MaxSkillsInDashboard = 30;

        /// <summary>
        ///     Max skill categories in profile page.
        /// </summary>
        public const int MaxSkillCategoriesInProfilePage = 30;

        /// <summary>
        ///     Max skill categories in dashboard.
        /// </summary>
        public const int MaxSkillCategoriesInDashboard = 30;
    }
}