﻿namespace Sodakoq.Main.Constants
{
    public class InMemoryKeyConstant
    {
        #region Properties

        /// <summary>
        ///     Current user profile key in in-memory db.
        /// </summary>
        public const string Profile = nameof(Profile);

        /// <summary>
        ///     Access token that user has which will be submited to api calls.
        /// </summary>
        public const string AccessToken = nameof(AccessToken);

        #endregion
    }
}