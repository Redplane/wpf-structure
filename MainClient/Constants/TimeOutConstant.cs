﻿namespace Sodakoq.Main.Constants
{
    public class TimeoutConstant
    {
        #region Properties

        /// <summary>
        ///     Default timeout (in seconds)
        /// </summary>
        public const int DefaultTimeout = 30;

        #endregion
    }
}