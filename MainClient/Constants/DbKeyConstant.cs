﻿namespace Sodakoq.Main.Constants
{
    public class DbKeyConstant
    {
        #region Properties

        /// <summary>
        ///     Authentication information.
        /// </summary>
        public const string Authentication = nameof(Authentication);

        #endregion
    }
}