﻿using System;
using GalaSoft.MvvmLight;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Controllers
{
    public class AppControllerBase : ViewModelBase
    {
        #region Properties

        /// <summary>
        ///     Translation service.
        /// </summary>
        private readonly ITranslateService _translateService;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize view model by using specific information.
        /// </summary>
        public AppControllerBase(ITranslateService translateService)
        {
            _translateService = translateService;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Add message to snack bar.
        /// </summary>
        /// <param name="snackbarMessageQueue"></param>
        /// <param name="message"></param>
        /// <param name="actionContent"></param>
        /// <param name="actionHandler"></param>
        /// <param name="bIsPromoted"></param>
        protected virtual void AddMessageToSnackBar(SnackbarMessageQueue snackbarMessageQueue, string message,
            string actionContent, Action actionHandler,
            bool bIsPromoted)
        {
            // Get translated message.
            var translatedMessage = _translateService
                .LoadTranslatedMessage(message);

            // Get translated action content.
            var translatedActionContent = _translateService
                .LoadTranslatedMessage(actionContent);

            var originalActionHandler = actionHandler;
            if (actionHandler == null)
                originalActionHandler = () => { };

            snackbarMessageQueue
                .Enqueue(translatedMessage ?? message, translatedActionContent ?? actionContent, originalActionHandler,
                    bIsPromoted);
        }

        #endregion
    }
}