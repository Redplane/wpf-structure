﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.Constants;
using Sodakoq.Main.Controllers.Dialogs;
using Sodakoq.Main.DataTransferObjects;
using Sodakoq.Main.DataTransferObjects.Skills;
using Sodakoq.Main.DataTransferObjects.Users;
using Sodakoq.Main.Models;
using Sodakoq.Main.Modules.CommonModule.LoadingModalModule;
using Sodakoq.Main.Properties;
using Sodakoq.Main.Services.Interfaces;
using Sodakoq.Main.Views.Dialogs;

namespace Sodakoq.Main.Controllers
{
    public class SkillController : ViewModelBase
    {
        #region Constructor

        public SkillController(ISkillService skillService, IMessageBusService messageBusService,
            ITranslateService translateService)
        {
            // Properties initialization.
            _loadSkillsCondition = new LoadSkillViewModel();
            _loadSkillsCondition.Pagination = new PaginationViewModel(1, PaginationConstant.MaxSkillsInDashboard);

            // Relay commands registration.
            ClickAddSkillRelayCommand = new RelayCommand(AddSkillCommand);
            ClickReloadSkillRelayCommand = new RelayCommand(ReloadSkillCommand);
            ClickDeleteSkillRelayCommand = new RelayCommand<SkillViewModel>(DeleteSkillCommand);
            OnPageLoadedRelayCommand = new RelayCommand(OnPageLoadedCommand);

            // Service binding.
            _skillService = skillService;
            _translateService = translateService;
            _messageBusService = messageBusService;

            messageBusService.FindEventSubject(EventNameConstant.UpdateUserProfile)
                .Subscribe(profile =>
                {
                    _profile = profile as UserViewModel;
                    _loadSkillsCondition.UserId = _profile?.Id;
                });
        }

        #endregion

        #region Cancellation tokens

        /// <summary>
        ///     Instance for cancelling adding user skill task.
        /// </summary>
        private CancellationTokenSource _addUserSkillCancellationTokenSource;

        /// <summary>
        ///     Cancellation source of user skills loading.
        /// </summary>
        private CancellationTokenSource _loadUserSkillsCancellationTokenSource;

        /// <summary>
        ///     Cancellation source of deleting user skill.
        /// </summary>
        private CancellationTokenSource _deleteUserSkillCancellationTokenSource;

        #endregion

        #region Properties

        /// <summary>
        ///     Search result of skills.
        /// </summary>
        private SearchResult<List<SkillViewModel>> _loadSkillsResult;

        /// <summary>
        ///     Search result of skills.
        /// </summary>
        public SearchResult<List<SkillViewModel>> LoadSkillsResult
        {
            get => _loadSkillsResult;
            set
            {
                Set(nameof(LoadSkillsResult), ref _loadSkillsResult, value);
                RaisePropertyChanged(nameof(Skills));
            }
        }

        /// <summary>
        ///     Get list of loaded skills
        /// </summary>
        public List<SkillViewModel> Skills => _loadSkillsResult?.Records;

        /// <summary>
        ///     Skill management dialog host identifier.
        /// </summary>
        public string SkillManagementDialogHost => "SkillManagementDialogHost";

        /// <summary>
        ///     Whether dialog is shown or not.
        /// </summary>
        private bool _bIsPopupShown;

        /// <summary>
        ///     Whether dialog is shown or not.
        /// </summary>
        public bool IsPopupShown
        {
            get => _bIsPopupShown;
            set => Set(nameof(IsPopupShown), ref _bIsPopupShown, value);
        }

        /// <summary>
        ///     Condition to load user skills.
        /// </summary>
        private readonly LoadSkillViewModel _loadSkillsCondition;


        /// <summary>
        ///     User profile.
        /// </summary>
        private UserViewModel _profile;

        #endregion

        #region Relay commands

        /// <summary>
        ///     Command which is fired when add skill button is clicked.
        /// </summary>
        public RelayCommand ClickAddSkillRelayCommand { get; }

        /// <summary>
        ///     Command which is fired when reload skills is clicked.
        /// </summary>
        public RelayCommand ClickReloadSkillRelayCommand { get; }

        /// <summary>
        ///     Command which is fired when delete skill is clicked.
        /// </summary>
        public RelayCommand<SkillViewModel> ClickDeleteSkillRelayCommand { get; }

        /// <summary>
        ///     Command which is fired when page is loaded.
        /// </summary>
        public RelayCommand OnPageLoadedRelayCommand { get; }

        #endregion

        #region Services

        /// <summary>
        ///     Service to handle skills operation.
        /// </summary>
        private readonly ISkillService _skillService;

        /// <summary>
        ///     Service to handle translation.
        /// </summary>
        private readonly ITranslateService _translateService;

        /// <summary>
        ///     Service to handle shared event during application lifetime.
        /// </summary>
        private readonly IMessageBusService _messageBusService;

        #endregion

        #region Methods

        /// <summary>
        ///     Command which is fired when add skill button is clicked.
        /// </summary>
        protected virtual async void AddSkillCommand()
        {
            // Clean up the model.
            var dataContext = new AddEditUserSkillDialogController();

            // Initialize a view.
            var view = new AddEditUserSkillDialogView();
            view.DataContext = dataContext;

            if (!(await DialogHost.Show(view, SkillManagementDialogHost) is AddEditUserSkillViewModel addEditUserSkillModel)
            )
                return;

            if (addEditUserSkillModel.Id != null)
                return;

            // Display loading indicator.
            var loadingView = new LoadingIndicatorDialogView();
            var loadingViewModel = new LoadingModalViewModel();
            loadingViewModel.IsCancelAvailable = true;
            loadingView.DataContext = loadingViewModel;

            // Display loading view.
            var displayLoadingIndicatorTask =
                DialogHost.Show(loadingView, SkillManagementDialogHost, OnAddingSkillOperationCancelled);

            // Add skill category task.
            if (_addUserSkillCancellationTokenSource != null)
                _addUserSkillCancellationTokenSource.Cancel();

            if (addEditUserSkillModel.SkillCategoryId == null)
                return;

            // Initialize model.
            var model = new AddUserSkillViewModel();
            model.UserId = _profile.Id;
            model.Name = addEditUserSkillModel.Name;
            model.Point = addEditUserSkillModel.Point;
            model.SkillCategoryId = addEditUserSkillModel.SkillCategoryId.Value;

            _addUserSkillCancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(30));
            var addSkillCategoryTask =
                _skillService.AddUserSkillAsync(model, _addUserSkillCancellationTokenSource.Token);

            // Get completed task.
            try
            {
                var completedTask = await Task.WhenAny(displayLoadingIndicatorTask, addSkillCategoryTask);
                if (completedTask != addSkillCategoryTask)
                    throw new TaskCanceledException();

                if (completedTask.IsCanceled)
                    throw new TaskCanceledException();

                LoadSkillsResult = await ReloadSkillsAsync();
            }
            catch (Exception exception)
            {
                if (exception is TaskCanceledException)
                    return;

                throw;
            }
            finally
            {
                _addUserSkillCancellationTokenSource = null;
                IsPopupShown = false;
            }
        }

        /// <summary>
        ///     Command which is fired when skill reload button is clicked.
        /// </summary>
        protected virtual async void ReloadSkillCommand()
        {
            LoadSkillsResult = await ReloadSkillsAsync();
        }

        /// <summary>
        ///     Command which is fired when delete skill is clicked
        /// </summary>
        /// <param name="skillModel"></param>
        protected virtual async void DeleteSkillCommand(SkillViewModel skillModel)
        {
            // Hide previously displayed dialog.
            IsPopupShown = false;

            // Skill delete confirmation dialog initialization.
            var view = new DeleteSkillDialogView();
            var viewModel = new DeleteSkillDialogController();
            viewModel.Skill = skillModel;
            view.DataContext = viewModel;

            // Display confimation dialog.
            var result = await DialogHost.Show(view, SkillManagementDialogHost);
            if (!result.Equals(true))
                return;

            try
            {
                if (_deleteUserSkillCancellationTokenSource != null &&
                    !_deleteUserSkillCancellationTokenSource.IsCancellationRequested)
                    _deleteUserSkillCancellationTokenSource.Cancel(false);

                var loadingView = new LoadingIndicatorDialogView();
                var loadingViewModel = new LoadingModalViewModel();
                loadingViewModel.IsCancelAvailable = true;
                loadingView.DataContext = loadingViewModel;

                var displayLoadingIndicatorTask = DialogHost.Show(loadingView, SkillManagementDialogHost,
                    OnDeletingUserSkillOperationCancelled);
                var deleteSkillModel = new DeleteSkillViewModel();
                deleteSkillModel.UserId = _profile.Id;
                deleteSkillModel.DeleteSkillExistence = false;

                _deleteUserSkillCancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(30));
                var deleteSkillTask = _skillService.DeleteUserSkillAsync(skillModel.Id, deleteSkillModel,
                    _deleteUserSkillCancellationTokenSource.Token);
                var completedTask = await Task.WhenAny(displayLoadingIndicatorTask, deleteSkillTask);

                // Delete skill task is cancelled.
                if (completedTask != deleteSkillTask)
                    throw new TaskCanceledException();

                if (completedTask.IsCanceled)
                    throw new TaskCanceledException();

                var snackbarMessage = new SnackbarMessageModel("MSG_DELETE_SKILL_SUCCESSFULLY",
                    "TITLE_DISMISS",
                    x => { });
                _messageBusService.PublishEvent(EventNameConstant.AddAppSnackbarMessage, snackbarMessage);

                var reloadSkillsTask = ReloadSkillsAsync();
                LoadSkillsResult = await reloadSkillsTask;
            }
            catch (Exception exception)
            {
                if (exception is TaskCanceledException)
                    return;

                throw;
            }
            finally
            {
                IsPopupShown = false;
            }
        }

        /// <summary>
        ///     Reload skill asynchronously.
        /// </summary>
        protected virtual async Task<SearchResult<List<SkillViewModel>>> ReloadSkillsAsync()
        {
            // Hide previous loading dialog.
            IsPopupShown = false;

            // Display loading dialog.
            var loadingDialogView = new LoadingIndicatorDialogView();
            var loadingDialogViewModel = new LoadingModalViewModel();
            loadingDialogViewModel.IsCancelAvailable = true;
            loadingDialogView.DataContext = loadingDialogViewModel;

            // Display popup.
            var displayLoadingDialogViewTask = DialogHost.Show(loadingDialogView, SkillManagementDialogHost,
                OnLoadUserSkillsOperationCancelled);

            // Load skill categories.
            if (_loadUserSkillsCancellationTokenSource != null)
                _loadUserSkillsCancellationTokenSource.Cancel();

            _loadUserSkillsCancellationTokenSource = new CancellationTokenSource();
            var loadUserSkillsTask =
                _skillService.LoadUserSkillsAsync(_loadSkillsCondition,
                    _loadUserSkillsCancellationTokenSource.Token);

            try
            {
                var completedTask = await Task.WhenAny(displayLoadingDialogViewTask, loadUserSkillsTask);

                if (!(completedTask is Task<SearchResult<List<SkillViewModel>>> loadUserSkillCategoriesResultTask))
                    return LoadSkillsResult;

                return await loadUserSkillCategoriesResultTask;
            }
            catch (Exception exception)
            {
                if (exception is TaskCanceledException)
                    return LoadSkillsResult;

                throw;
            }
            finally
            {
                // Close dialog programmatically.
                IsPopupShown = false;
            }
        }

        /// <summary>
        ///     Called when load user skill operation is cancelled.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        protected virtual void OnLoadUserSkillsOperationCancelled(object sender, DialogClosingEventArgs eventArgs)
        {
            if (_loadUserSkillsCancellationTokenSource == null)
                return;

            _loadUserSkillsCancellationTokenSource.Cancel();
            _loadUserSkillsCancellationTokenSource = null;
        }

        /// <summary>
        ///     Called when page is loaded.
        /// </summary>
        protected virtual async void OnPageLoadedCommand()
        {
            // Reload skills list.
            var reloadSkillsTask = ReloadSkillsAsync();

            // Update window title task.
            _messageBusService.PublishEvent(EventNameConstant.UpdateMainViewTitle, Resources.TITLE_SKILL_MANAGEMENT);
            LoadSkillsResult = await reloadSkillsTask;
        }

        /// <summary>
        ///     Called when adding skill operation is cancelled.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        protected virtual void OnAddingSkillOperationCancelled(object sender, DialogClosingEventArgs eventArgs)
        {
            if (_addUserSkillCancellationTokenSource == null)
                return;

            if (_addUserSkillCancellationTokenSource.IsCancellationRequested)
                return;

            _addUserSkillCancellationTokenSource.Cancel();
        }

        /// <summary>
        ///     Called when deleting user skill operation is cancelled.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        protected virtual void OnDeletingUserSkillOperationCancelled(object sender, DialogClosingEventArgs eventArgs)
        {
            if (_deleteUserSkillCancellationTokenSource == null)
                return;

            if (_deleteUserSkillCancellationTokenSource.IsCancellationRequested)
                return;

            _deleteUserSkillCancellationTokenSource.Cancel(true);
        }

        #endregion
    }
}