﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.Constants;
using Sodakoq.Main.Controllers.Dialogs;
using Sodakoq.Main.DataTransferObjects;
using Sodakoq.Main.DataTransferObjects.SkillCategories;
using Sodakoq.Main.DataTransferObjects.Users;
using Sodakoq.Main.Models;
using Sodakoq.Main.Models.Searches;
using Sodakoq.Main.Models.SkillCategories;
using Sodakoq.Main.Modules.CommonModule.LoadingModalModule;
using Sodakoq.Main.Services.Interfaces;
using Sodakoq.Main.Views.Dialogs;

namespace Sodakoq.Main.Controllers
{
    public class SkillCategoryController : ViewModelBase
    {
        #region Constructor

        public SkillCategoryController(ISkillCategoryService skillCategoryService,
            IMessageBusService sharedService, ITranslateService translateService)
        {
            // Relay command initialization.
            ClickAddSkillCategoryRelayCommand = new RelayCommand(ClickAddSkillCategoryCommand);
            ClickEditSkillCategoryRelayCommand =
                new RelayCommand<SkillCategoryViewModel>(ClickEditSkillCategoryCommand);
            ClickDeleteSkillCategoryRelayCommand =
                new RelayCommand<SkillCategoryViewModel>(ClickDeleteSkillCategoryCommand);

            ClickReloadSkillCategoriesRelayCommand = new RelayCommand(ClickReloadSkillCategoriesCommand);
            OnPageLoadedRelayCommand =
                new RelayCommand(OnPageLoadedCommand);

            // Property binding.
            _loadUserSkillCategoryCondition = new LoadUserSkillCategoryModel();
            _loadUserSkillCategoryCondition.Pagination =
                new PaginationViewModel(1, PaginationConstant.MaxSkillCategoriesInDashboard);

            // Service binding.
            _skillCategoryService = skillCategoryService;
            _sharedService = sharedService;

            sharedService
                .FindEventSubject(EventNameConstant.UpdateUserProfile)
                .Subscribe(profile => { _profile = profile as UserViewModel; });
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Command which is fired when add skill category is called.
        /// </summary>
        protected virtual async void ClickAddSkillCategoryCommand()
        {
            // Clean up the model.
            var dataContext = SimpleIoc.Default.GetInstance<AddEditSkillCategoryDialogController>();
            dataContext.Model = new AddEditSkillCategoryModel();

            // Initialize a view.
            var view = new AddEditSkillCategoryDialogView();

            if (!(await DialogHost.Show(view, SkillCategoryManagementDialogHost) is
                AddEditSkillCategoryModel model))
                return;

            if (model.Id != null)
                return;

            // Cancel previous request.
            if (_loadUserSkillCategoryCancellationSource != null &&
                !_loadUserSkillCategoryCancellationSource.IsCancellationRequested)
            {
                _loadUserSkillCategoryCancellationSource.Cancel();
                _loadUserSkillCategoryCancellationSource = null;
            }

            // Display loading indicator.
            var loadingDialogView = new LoadingIndicatorDialogView();
            var loadingDialogViewModel = new LoadingModalViewModel();
            loadingDialogViewModel.IsCancelAvailable = true;
            var displayLoadingDialogTask = DialogHost.Show(loadingDialogView, SkillCategoryManagementDialogHost,
                OnReloadSkillCategoriesOperationCancelled);

            // Add skill category task.
            if (_addSkillCategoryCancellationTokenSource != null &&
                !_addSkillCategoryCancellationTokenSource.IsCancellationRequested)
                _addSkillCategoryCancellationTokenSource.Cancel();

            _addSkillCategoryCancellationTokenSource =
                new CancellationTokenSource(TimeSpan.FromSeconds(TimeoutConstant.DefaultTimeout));
            var addSkillCategoryTask =
                _skillCategoryService.AddSkillCategoryAsync(model, _addSkillCategoryCancellationTokenSource.Token);

            // Get completed task.
            try
            {
                var completedTask = await Task.WhenAny(displayLoadingDialogTask, addSkillCategoryTask);
                if (completedTask != addSkillCategoryTask || completedTask.IsCanceled)
                    throw new TaskCanceledException();

                LoadSkillCategoriesResult = await ReloadSkillCategoriesAsync();
            }
            catch (Exception exception)
            {
                if (exception is TaskCanceledException)
                    return;

                throw;
            }
            finally
            {
                IsPopupShown = false;
            }
        }

        /// <summary>
        ///     Command which is fired when edit skill category is called.
        /// </summary>
        protected virtual async void ClickEditSkillCategoryCommand(SkillCategoryViewModel skillCategory)
        {
            // Clean up the model.
            var dataContext = SimpleIoc.Default.GetInstance<AddEditSkillCategoryDialogController>();
            var addEditSkillCategoryModel = new AddEditSkillCategoryModel();
            addEditSkillCategoryModel.Id = skillCategory.Id;
            addEditSkillCategoryModel.Name = skillCategory.Name;
            addEditSkillCategoryModel.CreatedTime = skillCategory.CreatedTime;
            dataContext.Model = addEditSkillCategoryModel;

            // Initialize a view.
            var view = new AddEditSkillCategoryDialogView();

            if (!(await DialogHost.Show(view) is AddEditSkillCategoryModel model))
                return;

            // Not in edit-mode.
            if (model.Id == null)
                return;

            // Cancel previous request.
            if (_editSkillCategoryCancellationTokenSource != null &&
                !_editSkillCategoryCancellationTokenSource.IsCancellationRequested)
            {
                _editSkillCategoryCancellationTokenSource.Cancel(false);
                _editSkillCategoryCancellationTokenSource = null;
            }

            // Display loading indicator.
            var loadingDialogView = new LoadingIndicatorDialogView();
            var loadingDialogViewModel = new LoadingModalViewModel();
            loadingDialogViewModel.IsCancelAvailable = true;
            loadingDialogView.DataContext = loadingDialogViewModel;
            var displayLoadingIndicatorTask = DialogHost.Show(loadingDialogView, SkillCategoryManagementDialogHost,
                OnEditSkillCategoryOperationCancelled);

            // Add skill category task.
            _editSkillCategoryCancellationTokenSource =
                new CancellationTokenSource(TimeSpan.FromSeconds(TimeoutConstant.DefaultTimeout));
            var editSkillCategoryTask =
                _skillCategoryService.EditSkillCategoryAsync(model, _editSkillCategoryCancellationTokenSource.Token);

            // Get completed task.
            try
            {
                var completedTask = await Task.WhenAny(displayLoadingIndicatorTask, editSkillCategoryTask);
                if (completedTask != editSkillCategoryTask || completedTask.IsCanceled)
                    throw new TaskCanceledException();

                LoadSkillCategoriesResult = await ReloadSkillCategoriesAsync();
            }
            catch (Exception exception)
            {
                if (exception is TaskCanceledException)
                    return;

                throw;
            }
            finally
            {
                IsPopupShown = false;
            }
        }

        /// <summary>
        ///     Command which is fired when delete skill category button is clicked.
        /// </summary>
        /// <param name="skillCategory"></param>
        protected virtual async void ClickDeleteSkillCategoryCommand(SkillCategoryViewModel skillCategory)
        {
            // Get dialog data context.
            var dialogContext = SimpleIoc.Default.GetInstance<DeleteSkillCategoryDialogController>();

            // Initialize model.
            var model = new DeleteSkillCategoryModel();
            model.UserId = _profile.Id;
            model.SkillCategoryName = skillCategory.Name;
            model.SkillCategoryId = skillCategory.Id;
            dialogContext.Model = model;

            // View initialization.
            var view = new DeleteSkillCategoryDialogView();
            view.DataContext = dialogContext;

            try
            {
                // Display confirmation dialog.
                var dialogResult = await DialogHost.Show(view, SkillCategoryManagementDialogHost);

                if (!(dialogResult is DeleteSkillCategoryModel deleteSkillCategoryModel))
                    return;

                // Cancel previous request.
                if (_deleteSkillCategoryCancellationTokenSource != null &&
                    !_deleteSkillCategoryCancellationTokenSource.IsCancellationRequested)
                {
                    _deleteSkillCategoryCancellationTokenSource.Cancel();
                    _deleteSkillCategoryCancellationTokenSource = null;
                }

                // Initialize cancellcation token source.
                _deleteSkillCategoryCancellationTokenSource =
                    new CancellationTokenSource(TimeSpan.FromSeconds(TimeoutConstant.DefaultTimeout));

                // Delete skill category.
                var deleteSkillCategoryTask = _skillCategoryService.DeleteSkillCategoryAsync(
                    deleteSkillCategoryModel.SkillCategoryId,
                    deleteSkillCategoryModel, _deleteSkillCategoryCancellationTokenSource.Token);

                // Display loading dialog.
                var loadingIndicatorView = new LoadingIndicatorDialogView();
                var loadIndicatorViewModel = new LoadingModalViewModel();
                loadIndicatorViewModel.IsCancelAvailable = false;
                loadingIndicatorView.DataContext = loadIndicatorViewModel;
                var displayLoadingDialogTask = DialogHost.Show(loadingIndicatorView, SkillCategoryManagementDialogHost);

                var completedTask = await Task.WhenAny(deleteSkillCategoryTask, displayLoadingDialogTask);
                if (completedTask != deleteSkillCategoryTask || completedTask.IsCanceled)
                    return;

                // Reload skill categories.
                var reloadSkillCategoriesTask = ReloadSkillCategoriesAsync();

                // Display successful message.
                _sharedService
                    .PublishEvent(EventNameConstant.AddAppSnackbarMessage,
                        new SnackbarMessageModel("MSG_SKILL_CATEGORY_HAS_BEEN_DELETED_SUCCESSFULLY", "TITLE_DISMISS",
                            x => { }));

                await reloadSkillCategoriesTask;
            }
            catch (Exception exception)
            {
                if (exception is TaskCanceledException)
                    return;

                throw;
            }
            finally
            {
                IsPopupShown = false;
            }
        }

        /// <summary>
        ///     Command which is fired when skill category is forced to be reloaded.
        /// </summary>
        protected virtual async void ClickReloadSkillCategoriesCommand()
        {
            // Reload skill categories.
            LoadSkillCategoriesResult = await ReloadSkillCategoriesAsync();
        }

        /// <summary>
        ///     Called when skill category management page is loaded.
        /// </summary>
        protected virtual async void OnPageLoadedCommand()
        {
            // Update main view title.
            _sharedService.PublishEvent(EventNameConstant.UpdateMainViewTitle,
                "TITLE_SKILL_CATEGORY_MANAGEMENT");

            // Load skill categories when page is loaded.
            var reloadSkillCategoriesTask = ReloadSkillCategoriesAsync();

            LoadSkillCategoriesResult = await reloadSkillCategoriesTask;
        }

        /// <summary>
        ///     Called when load user skill categories operation cancelled.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        protected void OnLoadUserSkillCategoriesOperationCancelled(object sender, DialogClosingEventArgs eventArgs)
        {
            if (_loadUserSkillCategoryCancellationSource == null)
                return;

            _loadUserSkillCategoryCancellationSource.Cancel();
        }

        /// <summary>
        ///     Reload skill categories asynchronously.
        /// </summary>
        protected virtual async Task<SearchResult<List<SkillCategoryViewModel>>> ReloadSkillCategoriesAsync()
        {
            // Display loading dialog.
            var loadingDialogView = new LoadingIndicatorDialogView();
            var loadingDialogViewModel = new LoadingModalViewModel();
            loadingDialogView.DataContext = loadingDialogViewModel;

            // Display popup.
            var displayLoadingDialogViewTask = DialogHost
                .Show(loadingDialogView, SkillCategoryManagementDialogHost, OnReloadSkillCategoriesOperationCancelled);

            // Load skill categories.
            _loadUserSkillCategoryCancellationSource = new CancellationTokenSource(TimeSpan.FromSeconds(30));
            var loadUserSkillCategoriesTask =
                _skillCategoryService.LoadUserSkillCategoriesAsync(_loadUserSkillCategoryCondition,
                    _loadUserSkillCategoryCancellationSource.Token);

            try
            {
                var completedTask = await Task.WhenAny(displayLoadingDialogViewTask, loadUserSkillCategoriesTask);
                if (completedTask != loadUserSkillCategoriesTask)
                    return LoadSkillCategoriesResult;

                return loadUserSkillCategoriesTask.Result;
            }
            catch (Exception exception)
            {
                if (exception is TaskCanceledException)
                    return LoadSkillCategoriesResult;

                throw;
            }
            finally
            {
                IsPopupShown = false;
            }
        }

        #endregion

        #region Events

        /// <summary>
        ///     Called when edit skill category operation is cancelled.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="eventArgs"></param>
        protected virtual void OnEditSkillCategoryOperationCancelled(object o, DialogClosingEventArgs eventArgs)
        {
            if (_editSkillCategoryCancellationTokenSource == null)
                return;

            if (_editSkillCategoryCancellationTokenSource.IsCancellationRequested)
                return;

            _editSkillCategoryCancellationTokenSource.Cancel();
            _editSkillCategoryCancellationTokenSource = null;
        }

        /// <summary>
        ///     Called when reload skill categories operation is cancelled.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="eventArgs"></param>
        protected virtual void OnReloadSkillCategoriesOperationCancelled(object o, DialogClosingEventArgs eventArgs)
        {
            if (_loadUserSkillCategoryCancellationSource == null)
                return;

            if (_loadUserSkillCategoryCancellationSource.IsCancellationRequested)
                return;

            _loadUserSkillCategoryCancellationSource.Cancel();
            _loadUserSkillCategoryCancellationSource = null;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Skill category result.
        /// </summary>
        private SearchResult<List<SkillCategoryViewModel>> _loadSkillCategoriesResult;

        /// <summary>
        ///     List of skill categories.
        /// </summary>
        public List<SkillCategoryViewModel> SkillCategories => _loadSkillCategoriesResult?.Records;

        /// <summary>
        ///     Search result of skill categories.
        /// </summary>
        public SearchResult<List<SkillCategoryViewModel>> LoadSkillCategoriesResult
        {
            get => _loadSkillCategoriesResult;
            protected set
            {
                Set(nameof(LoadSkillCategoriesResult), ref _loadSkillCategoriesResult, value);
                RaisePropertyChanged(nameof(SkillCategories));
            }
        }

        /// <summary>
        ///     Load user skill category model.
        /// </summary>
        private readonly LoadUserSkillCategoryModel _loadUserSkillCategoryCondition;

        /// <summary>
        ///     Whether skill categories has been loaded or not.
        /// </summary>
        private bool _bHasSkillCategoriesLoaded;

        /// <summary>
        ///     Whether skill categories has been loaded or not.
        /// </summary>
        public bool HasSkillCategoriesLoaded
        {
            get => _bHasSkillCategoriesLoaded;
            private set => Set(nameof(HasSkillCategoriesLoaded), ref _bHasSkillCategoriesLoaded, value);
        }

        /// <summary>
        ///     Whether popup is shown or not.
        /// </summary>
        private bool _bIsPopupShown;

        /// <summary>
        ///     Whether popup is shown or not.
        /// </summary>
        public bool IsPopupShown
        {
            get => _bIsPopupShown;
            set => Set(nameof(IsPopupShown), ref _bIsPopupShown, value);
        }

        /// <summary>
        ///     Skill category management dialog host.
        /// </summary>
        public string SkillCategoryManagementDialogHost => nameof(SkillCategoryManagementDialogHost);

        /// <summary>
        ///     /Service to handle skill category operation.
        /// </summary>
        private readonly ISkillCategoryService _skillCategoryService;

        /// <summary>
        ///     Shared service in application.
        /// </summary>
        private readonly IMessageBusService _sharedService;

        /// <summary>
        ///     User profile.
        /// </summary>
        private UserViewModel _profile;

        #endregion

        #region Relay commands

        /// <summary>
        ///     Relay command which is fired when add skill category button is clicked.
        /// </summary>
        public RelayCommand ClickAddSkillCategoryRelayCommand { get; }

        /// <summary>
        ///     Relay command which is fred when edit skill category button is clicked.
        /// </summary>
        public RelayCommand<SkillCategoryViewModel> ClickEditSkillCategoryRelayCommand { get; }

        /// <summary>
        ///     Relay command which is fired when delete skill category is clicked.
        /// </summary>
        public RelayCommand<SkillCategoryViewModel> ClickDeleteSkillCategoryRelayCommand { get; }

        /// <summary>
        ///     Relay command which is for reloading skill categories.
        /// </summary>
        public RelayCommand ClickReloadSkillCategoriesRelayCommand { get; }

        /// <summary>
        ///     Command which is fired when skill category management page is loaded.
        /// </summary>
        public RelayCommand OnPageLoadedRelayCommand { get; }

        #endregion

        #region Cancellation token source

        /// <summary>
        ///     Cancellation source of skill category loading.
        /// </summary>
        private CancellationTokenSource _loadUserSkillCategoryCancellationSource;

        /// <summary>
        ///     Cancellation source of adding skill category.
        /// </summary>
        private CancellationTokenSource _addSkillCategoryCancellationTokenSource;

        /// <summary>
        ///     Cancellation source of editing skill category.
        /// </summary>
        private CancellationTokenSource _editSkillCategoryCancellationTokenSource;

        /// <summary>
        ///     Cancellation token of deleting skill category.
        /// </summary>
        private CancellationTokenSource _deleteSkillCategoryCancellationTokenSource;

        #endregion
    }
}