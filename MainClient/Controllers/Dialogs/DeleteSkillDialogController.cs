﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.DataTransferObjects.Skills;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Controllers.Dialogs
{
    public class DeleteSkillDialogController : ViewModelBase
    {
        #region Services

        /// <summary>
        ///     Service to handle translation.
        /// </summary>
        private readonly ITranslateService _translateService;

        #endregion

        #region Constructor

        public DeleteSkillDialogController()
        {
            _translateService = SimpleIoc.Default.GetInstance<ITranslateService>();

            // Relay commands registration.
            ClickOkRelayCommand = new RelayCommand(ClickOkCommand);
            ClickCancelRelayCommand = new RelayCommand(ClickCancelCommand);
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Message that displayed on confirmation dialog.
        /// </summary>
        private string _message;

        /// <summary>
        ///     Message that displayed on confirmation dialog.
        /// </summary>
        public string Message
        {
            get => _message;
            private set => Set(nameof(Message), ref _message, value);
        }

        /// <summary>
        ///     Skill information.
        /// </summary>
        private SkillViewModel _skill;

        /// <summary>
        ///     Skill that needs deleting.
        /// </summary>
        public SkillViewModel Skill
        {
            get => _skill;
            set
            {
                Set(nameof(Skill), ref _skill, value);
                if (value == null)
                {
                    Message = "";
                    return;
                }

                var translatedMessage = _translateService.LoadTranslatedMessage("MSG_CONFIRM_DELETE_SKILL", value);
                Message = translatedMessage;
            }
        }

        #endregion

        #region Relay commands

        /// <summary>
        ///     Command which is fired when ok button is clicked.
        /// </summary>
        public RelayCommand ClickOkRelayCommand { get; }

        /// <summary>
        ///     Command which is fired when cancel button is clicked.
        /// </summary>
        public RelayCommand ClickCancelRelayCommand { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     Command which is fired when OK button is clicked.
        /// </summary>
        protected virtual void ClickOkCommand()
        {
            DialogHost.CloseDialogCommand.Execute(true, null);
        }

        /// <summary>
        ///     Command which is fired when Cancel button is clicked.
        /// </summary>
        protected virtual void ClickCancelCommand()
        {
            DialogHost.CloseDialogCommand.Execute(false, null);
        }

        #endregion
    }
}