﻿using System;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.DataTransferObjects;
using Sodakoq.Main.DataTransferObjects.SkillCategories;
using Sodakoq.Main.DataTransferObjects.Skills;
using Sodakoq.Main.Models.Searches;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Controllers.Dialogs
{
    public class AddEditUserSkillDialogController : ViewModelBase
    {
        #region Properties

        /// <summary>
        ///     Model for information binding.
        /// </summary>
        private AddEditUserSkillViewModel _model;

        /// <summary>
        ///     Model for information binding.
        /// </summary>
        public AddEditUserSkillViewModel Model
        {
            get => _model;
            set
            {
                AddEditUserSkillViewModel model;
                if (value == null)
                    model = new AddEditUserSkillViewModel();
                else
                    model = value;
                Set(nameof(Model), ref _model, model);
                OriginalSkillName = model.Name;
            }
        }

        /// <summary>
        ///     Original skill name.
        /// </summary>
        private string _originalSkillName;

        /// <summary>
        ///     Name of original skill.
        /// </summary>
        public string OriginalSkillName
        {
            get => _originalSkillName;
            private set => Set(nameof(OriginalSkillName), ref _originalSkillName, value);
        }

        /// <summary>
        ///     List of available skill categories.
        /// </summary>
        private List<SkillCategoryViewModel> _availableSkillCategories;

        /// <summary>
        ///     Available skill categories that user can select.
        /// </summary>
        public List<SkillCategoryViewModel> AvailableSkillCategories
        {
            get => _availableSkillCategories;
            private set => Set(nameof(AvailableSkillCategories), ref _availableSkillCategories, value);
        }

        /// <summary>
        ///     Skill categoy name which is being searched.
        /// </summary>
        private string _searchingSkillCategoryName;

        /// <summary>
        ///     Skill category name which is being searched.
        /// </summary>
        public string SearchingSkillCategoryName
        {
            get => _searchingSkillCategoryName;
            set => Set(nameof(SearchingSkillCategoryName), ref _searchingSkillCategoryName, value);
        }

        #endregion

        #region Relay commands

        /// <summary>
        ///     Relay command which is raised when close button is clicked.
        /// </summary>
        public RelayCommand ClickCloseDialogRelayCommand { get; }

        /// <summary>
        ///     Relay command which is raised when ok button is clicked.
        /// </summary>
        public RelayCommand ClickOkDialogRelayCommand { get; }

        /// <summary>
        ///     Relay command which is fired when skill category combo box is loaded.
        /// </summary>
        public RelayCommand<ComboBox> OnSkillCategoryComboBoxLoadedRelayCommand { get; }

        #endregion

        #region Constructor

        public AddEditUserSkillDialogController()
        {
            // Property initialization.
            Model = new AddEditUserSkillViewModel();
            AvailableSkillCategories = new List<SkillCategoryViewModel>();

            // Relay commands registration.
            ClickCloseDialogRelayCommand = new RelayCommand(ClickCloseDialogComman);
            ClickOkDialogRelayCommand = new RelayCommand(ClickOkDialogCommand);
            OnSkillCategoryComboBoxLoadedRelayCommand = new RelayCommand<ComboBox>(OnSkillCategoryComboBoxLoaded);
        }

        /// <summary>
        ///     Called when skill category combo box is loaded.
        /// </summary>
        /// <param name="comboBox"></param>
        protected virtual void OnSkillCategoryComboBoxLoaded(ComboBox comboBox)
        {
            var input = (TextBox) comboBox.Template.FindName("PART_EditableTextBox", comboBox);
            if (input == null)
                return;

            // Get skill category service.
            var skillCategoryService = SimpleIoc.Default.GetInstance<ISkillCategoryService>();

            Observable.FromEventPattern
                <KeyEventHandler, KeyEventArgs>(
                    h => input.KeyUp += h,
                    h => input.KeyUp -= h)
                .Throttle(TimeSpan.FromMilliseconds(400))
                .ObserveOnDispatcher()
                .Select(x => input.Text)
                .DistinctUntilChanged()
                .Do(x => AvailableSkillCategories = new List<SkillCategoryViewModel>())
                .ObserveOn(Scheduler.Default)
                .Select(x => Observable.FromAsync(() =>
                {
                    var loadUserSkillCategoryModel = new LoadUserSkillCategoryModel();
                    loadUserSkillCategoryModel.Name = SearchingSkillCategoryName;
                    loadUserSkillCategoryModel.Pagination = new PaginationViewModel(1, 10);
                    return skillCategoryService.LoadUserSkillCategoriesAsync(loadUserSkillCategoryModel);
                }))
                .Switch()
                .ObserveOnDispatcher()
                .Subscribe(loadSkillCategoriesResult =>
                {
                    AvailableSkillCategories = loadSkillCategoriesResult.Records;
                });

            // Manually trigger key up event to initially load skill categories.
            var key = Key.Back; // Key to send
            var routedEvent = Keyboard.KeyUpEvent; // Event to send

            var keyEventArgs = new KeyEventArgs(Keyboard.PrimaryDevice,
                PresentationSource.FromVisual(input) ?? throw new InvalidOperationException(), 0, key);
            keyEventArgs.RoutedEvent = routedEvent;
            input.RaiseEvent(keyEventArgs);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Called when close button is clicked.
        /// </summary>
        protected virtual void ClickCloseDialogComman()
        {
            DialogHost.CloseDialogCommand.Execute(null, null);
        }

        /// <summary>
        ///     Called when ok button is clicked.
        /// </summary>
        protected virtual void ClickOkDialogCommand()
        {
            DialogHost.CloseDialogCommand.Execute(Model, null);
        }

        #endregion
    }
}