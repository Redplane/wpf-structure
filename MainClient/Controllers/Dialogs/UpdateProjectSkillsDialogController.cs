﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.Constants;
using Sodakoq.Main.DataTransferObjects;
using Sodakoq.Main.DataTransferObjects.Projects;
using Sodakoq.Main.DataTransferObjects.Skills;
using Sodakoq.Main.DataTransferObjects.Users;
using Sodakoq.Main.Models;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Controllers.Dialogs
{
    public class UpdateProjectSkillsDialogController : ViewModelBase
    {
        #region Services

        private readonly IMessageBusService _messageBusService;

        #endregion

        #region Properties

        /// <summary>
        ///     Profile information.
        /// </summary>
        public UserViewModel Profile { get; set; }

        /// <summary>
        ///     Project information.
        /// </summary>
        public ProjectViewModel Project { get; set; }

        /// <summary>
        ///     Skills which have been added by user.
        /// </summary>
        private List<CheckboxItemModel<SkillViewModel>> _addedSkills;

        /// <summary>
        ///     Skills which have been added by user.
        /// </summary>
        public List<CheckboxItemModel<SkillViewModel>> AddedSkills
        {
            get
            {
                if (_addedSkills == null)
                    _addedSkills = new List<CheckboxItemModel<SkillViewModel>>();
                return _addedSkills;
            }
            set
            {
                var addedSkills = value;
                if (addedSkills == null)
                    addedSkills = new List<CheckboxItemModel<SkillViewModel>>();

                Set(nameof(AddedSkills), ref _addedSkills, addedSkills);

                CheckAnySkillsSelected();
                CheckAllSkillsSelected();
            }
        }

        /// <summary>
        ///     Whether any skill has been selected or not.
        /// </summary>
        private bool _bHasAnySkillSelected;

        /// <summary>
        ///     Whether any skill has been selected or not.
        /// </summary>
        public bool HasAnySkillSelected
        {
            get => _bHasAnySkillSelected;
            set => Set(nameof(HasAnySkillSelected), ref _bHasAnySkillSelected, value);
        }

        /// <summary>
        ///     Whether all skills are selected or not.
        /// </summary>
        private bool _bHaveAllSkillsSelected;

        /// <summary>
        ///     Whether all responsibilities have been selected or not.
        /// </summary>
        public bool HaveAllSkillsSelected
        {
            get => _bHaveAllSkillsSelected;
            private set => Set(nameof(HaveAllSkillsSelected), ref _bHaveAllSkillsSelected, value);
        }

        /// <summary>
        ///     Selected skill.
        /// </summary>
        private SkillViewModel _selectedSkill;

        /// <summary>
        ///     Selected skill.
        /// </summary>
        public SkillViewModel SelectedSkill
        {
            get => _selectedSkill;
            set => Set(nameof(SelectedSkill), ref _selectedSkill, value);
        }

        /// <summary>
        ///     Available skills for being selected.
        /// </summary>
        private List<SkillViewModel> _availableSkills;

        /// <summary>
        ///     Available skills for being selected.
        /// </summary>
        public List<SkillViewModel> AvailableSkills
        {
            get => _availableSkills;
            set => Set(nameof(AvailableSkills), ref _availableSkills, value);
        }

        /// <summary>
        ///     Load skills observer.
        /// </summary>
        private IObservable<SearchResult<List<SkillViewModel>>> _loadSkillsObserver;

        /// <summary>
        ///     Skill name which is input by user for searching.
        /// </summary>
        private string _typedSkillName;

        /// <summary>
        ///     Skill name which is input by user for searching.
        /// </summary>
        public string TypedSkillName
        {
            get => _typedSkillName;
            set => Set(nameof(TypedSkillName), ref _typedSkillName, value);
        }

        #endregion

        #region Relay commands

        /// <summary>
        ///     Raised when delete selected skills button is clicked.
        /// </summary>
        public RelayCommand ClickDeleteSelectedSkillsRelayCommand { get; }

        /// <summary>
        ///     Raised when add skill button is clicked.
        /// </summary>
        public RelayCommand ClickAddSkillRelayCommand { get; }

        /// <summary>
        ///     Relay command which is fired when skills all skills are selected.
        /// </summary>
        public RelayCommand ClickSelectAllSkillsRelayCommand { get; }

        /// <summary>
        ///     Raised when component is loaded.
        /// </summary>
        public RelayCommand OnComponentLoadedRelayCommand { get; }

        /// <summary>
        ///     Raised when skills combobox is loaded.
        /// </summary>
        public RelayCommand<ComboBox> OnSkillsComboboxLoadedRelayCommand { get; }

        /// <summary>
        ///     Raised when Ok button is clicked.
        /// </summary>
        public RelayCommand OnOkClickedRelayCommand { get; }

        /// <summary>
        ///     Raised when Cancel button is clicked.
        /// </summary>
        public RelayCommand OnCancelClickedRelayCommand { get; }

        /// <summary>
        ///     Raised when skill input key is pressed down.
        /// </summary>
        public RelayCommand<KeyEventArgs> OnSkillInputKeyPressedDownRelayCommand { get; }

        /// <summary>
        ///     Raised when skill checked status is changed.
        /// </summary>
        public RelayCommand OnSkillCheckedStatusChangedRelayCommand { get; }

        #endregion

        #region Constructor

        public UpdateProjectSkillsDialogController()
        {
            // Property initialization.
            _addedSkills = new List<CheckboxItemModel<SkillViewModel>>();

            ClickDeleteSelectedSkillsRelayCommand = new RelayCommand(ClickDeleteSelectedSkillsCommand);
            ClickAddSkillRelayCommand = new RelayCommand(ClickAddSkillCommand);
            OnComponentLoadedRelayCommand = new RelayCommand(OnComponentLoadedCommand);
            ClickSelectAllSkillsRelayCommand = new RelayCommand(ClickSelectAllSkillsCommand);
            OnSkillsComboboxLoadedRelayCommand = new RelayCommand<ComboBox>(OnSkillsComboBoxLoadedCommand);
            OnOkClickedRelayCommand = new RelayCommand(OnOkClickedCommand);
            OnCancelClickedRelayCommand = new RelayCommand(OnCancelClickedCommand);
            OnSkillInputKeyPressedDownRelayCommand = new RelayCommand<KeyEventArgs>(OnSkillInputKeyPressedDownCommand);
            OnSkillCheckedStatusChangedRelayCommand = new RelayCommand(OnSkillCheckedStatusChangedCommand);

            // Get message bus service.
            _messageBusService = SimpleIoc.Default.GetInstance<IMessageBusService>();
            _messageBusService.FindEventSubject(EventNameConstant.UpdateUserProfile)
                .Subscribe(OnProfileUpdated);
        }


        public UpdateProjectSkillsDialogController(SkilledProjectViewModel skilledProject) : this()
        {
            Project = skilledProject;
            if (skilledProject == null)
                AddedSkills = new List<CheckboxItemModel<SkillViewModel>>();
            else
                AddedSkills = skilledProject.Skills.Select(x => new CheckboxItemModel<SkillViewModel>(x.Name, x))
                    .ToList();
        }

        #endregion

        #region Command implementations

        /// <summary>
        ///     Raised when delete selected skill button is clicked.
        /// </summary>
        protected virtual void ClickDeleteSelectedSkillsCommand()
        {
            // Added skills list is empty.
            if (AddedSkills == null || AddedSkills.Count < 1)
                return;

            // Find all skills which have been selected.
            AddedSkills = AddedSkills.Where(x => !x.IsSelected).ToList();
        }

        /// <summary>
        ///     Raised when add skill button is clicked.
        /// </summary>
        protected virtual void ClickAddSkillCommand()
        {
            // No skill has been selected.
            if (SelectedSkill == null)
                return;

            // Skill has been selected before.
            if (_addedSkills.Any(x => x.Name.Equals(SelectedSkill.Name)))
                return;

            var addedSkills = new List<CheckboxItemModel<SkillViewModel>>(_addedSkills);
            addedSkills.Add(new CheckboxItemModel<SkillViewModel>(SelectedSkill.Name, SelectedSkill, false));
            AddedSkills = addedSkills;
            AvailableSkills = AvailableSkills
                .Where(x => AddedSkills.All(y => x.Id != y.Value.Id))
                .ToList();

            SelectedSkill = null;
        }

        /// <summary>
        ///     Raised when component is loaded.
        /// </summary>
        protected virtual void OnComponentLoadedCommand()
        {
        }

        /// <summary>
        ///     Fired when all skill selection are selected.
        /// </summary>
        protected virtual void ClickSelectAllSkillsCommand()
        {
            var haveAllSkillsSelected = HaveAllSkillsSelected;
            var addedSkills = new List<CheckboxItemModel<SkillViewModel>>(_addedSkills);
            foreach (var addedSkill in addedSkills)
                addedSkill.IsSelected = !haveAllSkillsSelected;

            AddedSkills = addedSkills;
        }

        /// <summary>
        ///     Called when skills combobox is loaded.
        /// </summary>
        /// <summary>
        ///     Called when skills combo box is loaded.
        /// </summary>
        /// <param name="comboBox"></param>
        protected void OnSkillsComboBoxLoadedCommand(ComboBox comboBox)
        {
            if (comboBox == null)
                return;

            var skillService = SimpleIoc.Default.GetInstance<ISkillService>();

            _loadSkillsObserver = Observable.FromEventPattern
                <KeyEventHandler, KeyEventArgs>(
                    h => comboBox.KeyUp += h,
                    h => comboBox.KeyUp -= h)
                .Throttle(TimeSpan.FromMilliseconds(400))
                .ObserveOnDispatcher()
                .Select(x => TypedSkillName?.Trim())
                .DistinctUntilChanged()
                .Do(x => AvailableSkills = new List<SkillViewModel>())
                .ObserveOn(Scheduler.Default)
                .Select(x => Observable.FromAsync(() =>
                {
                    var loadSkillsCondition = new LoadSkillViewModel();
                    loadSkillsCondition.SkillIds = null;
                    loadSkillsCondition.UserId = Profile?.Id;
                    loadSkillsCondition.SkillName = TypedSkillName;
                    loadSkillsCondition.Pagination = new PaginationViewModel(1, 20);
                    return skillService.LoadUserSkillsAsync(loadSkillsCondition);
                }))
                .Switch()
                .ObserveOnDispatcher();

            _loadSkillsObserver.Subscribe(loadSkillCategoriesResult =>
            {
                var availableSkills = loadSkillCategoriesResult?.Records;
                if (availableSkills == null)
                {
                    AvailableSkills = new List<SkillViewModel>();
                    return;
                }

                // Clear selected item.
                SelectedSkill = null;

                // Ignore selected skills.
                availableSkills = availableSkills.Where(x => AddedSkills.All(y => x.Id != y.Value.Id)).ToList();
                AvailableSkills = new List<SkillViewModel>(availableSkills);
            });

            // Manually trigger key up event to initially load skill categories.
            var key = Key.Back; // Key to send
            var routedEvent = Keyboard.KeyUpEvent; // Event to send

            var keyEventArgs = new KeyEventArgs(Keyboard.PrimaryDevice,
                PresentationSource.FromVisual(comboBox) ?? throw new InvalidOperationException(), 0, key);
            keyEventArgs.RoutedEvent = routedEvent;
            comboBox.RaiseEvent(keyEventArgs);
        }

        /// <summary>
        ///     Called when ok button is clicked.
        /// </summary>
        protected virtual void OnOkClickedCommand()
        {
            if (AddedSkills == null || AddedSkills.Count < 1)
            {
                DialogHost.CloseDialogCommand.Execute(new List<SkillViewModel>(), null);
                return;
            }

            DialogHost.CloseDialogCommand.Execute(AddedSkills.Select(x => x.Value).ToList(), null);
        }

        /// <summary>
        ///     Called when cancel button is clicked.
        /// </summary>
        protected virtual void OnCancelClickedCommand()
        {
            DialogHost.CloseDialogCommand.Execute(null, null);
        }

        /// <summary>
        ///     Called when skill input is pressed down.
        /// </summary>
        /// <param name="eventArgs"></param>
        protected virtual void OnSkillInputKeyPressedDownCommand(KeyEventArgs eventArgs)
        {
            // Pressed key is not the Enter or Return one.
            if (eventArgs.Key != Key.Enter && eventArgs.Key != Key.Return)
                return;

            // Key is hold.
            if (eventArgs.IsRepeat)
                return;

            // No skill is selected at the moment.
            if (SelectedSkill == null)
                return;

            ClickAddSkillCommand();
        }

        /// <summary>
        ///     Called when profile information is updated.
        /// </summary>
        /// <param name="o"></param>
        protected virtual void OnProfileUpdated(object o)
        {
            if (!(o is UserViewModel profile))
                return;

            Profile = profile;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Check whether any skills are selected or not.
        /// </summary>
        protected virtual void CheckAnySkillsSelected()
        {
            if (AddedSkills == null || AddedSkills.Count < 1)
            {
                HasAnySkillSelected = false;
                return;
            }

            HasAnySkillSelected = AddedSkills.Any(x => x.IsSelected);
        }

        /// <summary>
        ///     Check when all skills are selected.
        /// </summary>
        protected virtual void CheckAllSkillsSelected()
        {
            HaveAllSkillsSelected = !AddedSkills.Any(x => !x.IsSelected);
        }

        /// <summary>
        ///     Raised when skill checked status changed.
        /// </summary>
        protected virtual void OnSkillCheckedStatusChangedCommand()
        {
            CheckAllSkillsSelected();
            CheckAnySkillsSelected();
        }

        #endregion
    }
}