﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MaterialDesignThemes.Wpf;

namespace Sodakoq.Main.Controllers.Dialogs
{
    public class YesNoDialogController : ViewModelBase
    {
        #region Constructor

        public YesNoDialogController()
        {
            ClickOkRelayCommand = new RelayCommand(ClickOkCommand);
            ClickCancelRelayCommand = new RelayCommand(ClickCancelCommand);
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Message that displayed on the dialog screen.
        /// </summary>
        private string _message;

        /// <summary>
        ///     Message that displayed on the dialog screen.
        /// </summary>
        public string Message
        {
            get => _message;
            set => Set(nameof(Message), ref _message, value);
        }

        /// <summary>
        ///     Relay command which is fired when Ok button is clicked.
        /// </summary>
        public RelayCommand ClickOkRelayCommand { get; }

        /// <summary>
        ///     Relay command which is fired when Cancel button is clicked.
        /// </summary>
        public RelayCommand ClickCancelRelayCommand { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     Command which is fired when Ok button is clicked.
        /// </summary>
        protected virtual void ClickOkCommand()
        {
            DialogHost.CloseDialogCommand.Execute(true, null);
        }

        /// <summary>
        ///     Command which is fired when Cancel button is clicked.
        /// </summary>
        protected virtual void ClickCancelCommand()
        {
            DialogHost.CloseDialogCommand.Execute(false, null);
        }

        #endregion
    }
}