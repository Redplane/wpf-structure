﻿using System.Windows;
using GalaSoft.MvvmLight.Command;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.Constants;
using Sodakoq.Main.Models;
using Sodakoq.Main.Models.SkillCategories;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Controllers.Dialogs
{
    public class AddEditSkillCategoryDialogController : AppControllerBase
    {
        #region Constructor

        public AddEditSkillCategoryDialogController(IValidationService validationService,
            IMessageBusService sharedService,
            ITranslateService translateService) : base(translateService)
        {
            // Model initialization.
            Model = new AddEditSkillCategoryModel();

            // Service binding.
            _validationService = validationService;
            _translateService = translateService;
            _sharedService = sharedService;

            // Relay command initialization.
            ClickCloseDialogRelayCommand = new RelayCommand(ClickCloseDialogCommand);
            ClickAddEditSkillCategoryRelayCommand =
                new RelayCommand<DependencyObject>(ClickAddEditSkillCategoryCommand);
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Validation service.
        /// </summary>
        private readonly IValidationService _validationService;

        private readonly ITranslateService _translateService;

        private readonly IMessageBusService _sharedService;


        /// <summary>
        ///     Command which is raised when close dialog button is clicked.
        /// </summary>
        public RelayCommand ClickCloseDialogRelayCommand { get; }

        /// <summary>
        ///     Command which is raised when user confirms adding | editing skill category.
        /// </summary>
        public RelayCommand<DependencyObject> ClickAddEditSkillCategoryRelayCommand { get; }

        /// <summary>
        ///     Model for information binding.
        /// </summary>
        private AddEditSkillCategoryModel _model;

        /// <summary>
        ///     Model for information binding.
        /// </summary>
        public AddEditSkillCategoryModel Model
        {
            get => _model;
            set
            {
                // Update add | edit skill category model.
                Set(nameof(AddEditSkillCategoryModel), ref _model, value);

                if (value != null && !string.IsNullOrEmpty(value.Name))
                    OriginalSkillCategoryName = value.Name;
            }
        }

        /// <summary>
        ///     Original skill category name.
        /// </summary>
        private string _originalSkillCategoryName;

        /// <summary>
        ///     Original skill category name.
        /// </summary>
        public string OriginalSkillCategoryName
        {
            get => _originalSkillCategoryName;
            private set => Set(nameof(OriginalSkillCategoryName), ref _originalSkillCategoryName, value);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Command which is raised when close button is clicked.
        /// </summary>
        protected virtual void ClickCloseDialogCommand()
        {
            DialogHost.CloseDialogCommand.Execute(null, null);
        }

        /// <summary>
        ///     Command which is fired when user adds | edits skill category.
        /// </summary>
        protected virtual void ClickAddEditSkillCategoryCommand(DependencyObject addEditSkillCategoryContainer)
        {
            // Check whether input parameters have any validation errors or not.
            if (!_validationService.IsControlValid(addEditSkillCategoryContainer))
            {
                var validationSnackbarMessage =
                    new SnackbarMessageModel("MSG_INFORMATION_NOT_VALID", "TITLE_DISMISS", x => { });
                _sharedService.PublishEvent(EventNameConstant.AddAppSnackbarMessage, validationSnackbarMessage);
                return;
            }

            DialogHost.CloseDialogCommand.Execute(Model, null);
        }

        #endregion
    }
}