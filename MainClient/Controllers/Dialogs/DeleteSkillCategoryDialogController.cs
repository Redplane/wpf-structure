﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.Models.SkillCategories;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Controllers.Dialogs
{
    public class DeleteSkillCategoryDialogController : ViewModelBase
    {
        #region Constructor

        public DeleteSkillCategoryDialogController(ITranslateService translateService)
        {
            // Property binding.
            _model = new DeleteSkillCategoryModel();

            // Service binding.
            _translateService = translateService;

            // Relay commands registration.
            ClickCancelRelayCommand = new RelayCommand(ClickCancelCommand);
            ClickOkRelayCommand = new RelayCommand(ClickOkCommand);
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Service for handling translation.
        /// </summary>
        private readonly ITranslateService _translateService;

        /// <summary>
        ///     Translated message about record deletion.
        /// </summary>
        private string _translatedDeleteMessage;

        /// <summary>
        ///     Translated message about record deletion.
        /// </summary>
        public string TranslatedDeleteMessage
        {
            get => _translatedDeleteMessage;
            private set => Set(nameof(TranslatedDeleteMessage), ref _translatedDeleteMessage, value);
        }

        /// <summary>
        ///     Model for information binding.
        /// </summary>
        private DeleteSkillCategoryModel _model;

        /// <summary>
        ///     Model for information binding.
        /// </summary>
        public DeleteSkillCategoryModel Model
        {
            get => _model;
            set
            {
                if (value == null)
                    _model = new DeleteSkillCategoryModel();
                else
                    _model = value;

                TranslatedDeleteMessage =
                    _translateService.LoadTranslatedMessage("MSG_CONFIRM_DELETE_SKILL_CATEGORY", _model);

                RaisePropertyChanged(nameof(Model));
            }
        }

        #region Relay command

        /// <summary>
        ///     Relay command which is fired when cancel button is clicked.
        /// </summary>
        public RelayCommand ClickCancelRelayCommand { get; }

        /// <summary>
        ///     Relay command which is fired when ok button is clicked.
        /// </summary>
        public RelayCommand ClickOkRelayCommand { get; }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Command which is fired when cancel button is clicked.
        /// </summary>
        protected virtual void ClickCancelCommand()
        {
            // Close dialog without passing any value.
            DialogHost.CloseDialogCommand.Execute(null, null);
        }

        /// <summary>
        ///     Command which is fired when ok button is clicked.
        /// </summary>
        protected virtual void ClickOkCommand()
        {
            DialogHost.CloseDialogCommand.Execute(Model, null);
        }

        #endregion
    }
}