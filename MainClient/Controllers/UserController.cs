﻿using System.Collections.Generic;
using System.Threading;
using System.Windows.Controls;
using GalaSoft.MvvmLight.Command;
using Sodakoq.Main.Constants;
using Sodakoq.Main.DataTransferObjects;
using Sodakoq.Main.DataTransferObjects.Users;
using Sodakoq.Main.Models;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Controllers
{
    public class UserController : AppControllerBase
    {
        #region Constructors

        /// <summary>
        ///     Initialize view model with injectors.
        /// </summary>
        public UserController(IUserService userService, ITranslateService translateService) : base(
            translateService)
        {
            SearchUserResult = new SearchResult<List<UserViewModel>>(new List<UserViewModel>(), 0);
            SeachUserViewCondition = new SearchUserViewModel();
            SeachUserViewCondition.Pagination = new PaginationViewModel(1, PaginationConstant.MaxUsersInDashboard);

            // Inject services.
            _userService = userService;

            OnUserPanelScrolledRelayCommand = new RelayCommand<ScrollChangedEventArgs>(OnUserPanelScrolled);
            OnUserDashboardLoadedRelayCommand = new RelayCommand(OnUserDashboardLoaded);
        }

        #endregion

        #region Properties

        /// <summary>
        ///     List of users in the systems that has been searched.
        /// </summary>
        public SearchResult<List<UserViewModel>> SearchUserResult { get; private set; }

        /// <summary>
        ///     Conditions for searching users.
        /// </summary>
        public SearchUserViewModel SeachUserViewCondition { get; }

        /// <summary>
        ///     Number of loaded users.
        /// </summary>
        private int _iLoadedUsers;

        /// <summary>
        ///     Check whether users list is being loaded or not.
        /// </summary>
        private bool _bLoadingUsers;

        /// <summary>
        ///     Check whether users list is being loaded or not.
        /// </summary>
        public bool IsLoadingUsers
        {
            get => _bLoadingUsers;
            set => Set(ref _bLoadingUsers, value, nameof(IsLoadingUsers));
        }

        /// <summary>
        ///     Service to handle user in database operations.
        /// </summary>
        private readonly IUserService _userService;

        #endregion

        #region Relay commands

        /// <summary>
        ///     Command which is fired when user panel is scrolled.
        /// </summary>
        public RelayCommand<ScrollChangedEventArgs> OnUserPanelScrolledRelayCommand { get; }

        /// <summary>
        ///     Command which is fired when user dashboard is loaded.
        /// </summary>
        public RelayCommand OnUserDashboardLoadedRelayCommand { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     Called when user panel is scrolled.
        /// </summary>
        /// <param name="scrollChangedEventArgs"></param>
        private async void OnUserPanelScrolled(ScrollChangedEventArgs scrollChangedEventArgs)
        {
            if (scrollChangedEventArgs == null)
                return;

            if (!(scrollChangedEventArgs.Source is ScrollViewer))
                return;

            var scrollViewer = (ScrollViewer) scrollChangedEventArgs.Source;
            if (scrollViewer == null)
                return;

            if (!scrollViewer.VerticalOffset.Equals(scrollViewer.ScrollableHeight))
                return;

            if (IsLoadingUsers)
                return;

            // No more users can be loaded.
            if (_iLoadedUsers >= SearchUserResult.TotalRecords)
                return;

            // Increase page by 1.
            SeachUserViewCondition.Pagination.Page++;

            // Lock controls.
            IsLoadingUsers = true;

            var loadUserResult = await _userService
                .SearchAsync(SeachUserViewCondition, CancellationToken.None);

            SearchUserResult.Records.AddRange(loadUserResult.Records);
            _iLoadedUsers = loadUserResult.TotalRecords;
            IsLoadingUsers = false;
        }

        /// <summary>
        ///     Called when user dashboard is loaded.
        /// </summary>
        private async void OnUserDashboardLoaded()
        {
            // User is being loaded.
            if (IsLoadingUsers)
                return;

            // Lock management controls.
            IsLoadingUsers = true;

            var loadUserResult = await _userService
                .SearchAsync(SeachUserViewCondition, CancellationToken.None);

            SearchUserResult = loadUserResult;
            _iLoadedUsers = loadUserResult.TotalRecords;
            IsLoadingUsers = false;
            RaisePropertyChanged(nameof(SearchUserResult));
        }

        #endregion
    }
}