﻿using System;
using System.ComponentModel;
using System.Windows.Controls;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.Constants;
using Sodakoq.Main.Models;
using Sodakoq.Main.Modules.LoginModule;
using Sodakoq.Main.Services.Interfaces;
using WPFLocalizeExtension.Engine;

namespace Sodakoq.Main.Controllers
{
    public class AppController : ViewModelBase
    {
        #region Constructor

        public AppController(ITranslateService translateService, IMessageBusService sharedService)
        {
            // Service binding.
            _sharedService = sharedService;
            _translateService = translateService;

            AppSnackBarMessageQueue = new SnackbarMessageQueue();

            // Event registration.
            _sharedService.FindEventSubject(EventNameConstant.AddAppSnackbarMessage)
                .Subscribe(OnSnackbarMessageReceived);

            // Relay command registration.
            OnMainFrameLoadedRelayCommand = new RelayCommand<Frame>(OnMainFrameLoadedCommand);
        }

        #endregion

        #region Relay commands

        /// <summary>
        ///     Command which is raised when main frame is loaded.
        /// </summary>
        public RelayCommand<Frame> OnMainFrameLoadedRelayCommand { get; }

        #endregion

        #region Services

        /// <summary>
        ///     Service to handle shared operations.
        /// </summary>
        private readonly IMessageBusService _sharedService;

        /// <summary>
        ///     Service to handle translation.
        /// </summary>
        private readonly ITranslateService _translateService;

        #endregion

        #region Properties

        /// <summary>
        ///     App title.
        /// </summary>
        private string _appTitle;

        /// <summary>
        ///     Application title.
        /// </summary>
        public string AppTitle
        {
            get => _appTitle;
            set
            {
                _appTitle = value;
                RaisePropertyChanged(nameof(AppTitle));
            }
        }

        /// <summary>
        ///     Instance of main frame.
        /// </summary>
        private Frame _appFrame;

        /// <summary>
        ///     App snackbar message queue.
        /// </summary>
        public SnackbarMessageQueue AppSnackBarMessageQueue { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     Called when app window is loaded.
        /// </summary>
        protected virtual void OnMainFrameLoadedCommand(Frame frame)
        {
            _appFrame = frame;
            _appFrame.Content = new LoginPage();

            // Hook to app frame navigation event.
            _sharedService.FindEventSubject(EventNameConstant.NavigateAppFrame)
                .Subscribe(OnAppFrameNavigated);

            LocalizeDictionary
                .Instance
                .PropertyChanged += OnCultureChanged;
        }

        /// <summary>
        ///     Reload window title.
        /// </summary>
        /// <param name="localizeDictionary"></param>
        /// <returns></returns>
        protected virtual string ReloadWindowTitle(LocalizeDictionary localizeDictionary)
        {
            // Update application title.
            var translatedAppMainTitle =
                localizeDictionary.GetLocalizedObject("TITLE_CV_MANAGEMENT", null,
                    localizeDictionary.Culture);

            if (translatedAppMainTitle != null)
                return translatedAppMainTitle.ToString();

            return "";
        }

        #endregion

        #region Events

        /// <summary>
        ///     Called when culture is changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="propertyChangedEventArgs"></param>
        protected virtual void OnCultureChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (!(sender is LocalizeDictionary localizeDictionary))
                return;

            AppTitle = ReloadWindowTitle(localizeDictionary);
        }

        /// <summary>
        ///     Called when app frame is requested to be navigated.
        /// </summary>
        /// <param name="instance"></param>
        protected virtual void OnAppFrameNavigated(object instance)
        {
            if (!(instance is Page page))
                return;

            _appFrame.Content = page;
        }

        /// <summary>
        ///     Called when app snackbar message is sent.
        /// </summary>
        /// <param name="message"></param>
        protected virtual void OnSnackbarMessageReceived(object message)
        {
            if (!(message is SnackbarMessageModel snackbarMessage))
                return;

            var content = snackbarMessage.Content;
            var actionContent = snackbarMessage.ActionContent;

            if (snackbarMessage.ShouldContentTranslated)
            {
                content = _translateService.LoadTranslatedMessage(content);
                actionContent = _translateService.LoadTranslatedMessage(actionContent);
            }

            if (string.IsNullOrEmpty(actionContent))
                actionContent = "";

            // Action command is defined.
            if (snackbarMessage.ActionHandler != null)
            {
                AppSnackBarMessageQueue.Enqueue(content, actionContent,
                    snackbarMessage.ActionHandler, snackbarMessage.IsPromoted, snackbarMessage.IsDuplicatable);
                return;
            }

            AppSnackBarMessageQueue.Enqueue(content);
        }

        #endregion
    }
}