﻿using System;
using Sodakoq.Domain.Enums;
using Sodakoq.Main.Constants;
using Sodakoq.Main.DataTransferObjects.Users;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Controllers
{
    public class AuthorizedController : AppController
    {
        #region Constructor

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="sharedService"></param>
        /// <param name="translateService"></param>
        public AuthorizedController(IMessageBusService sharedService,
            ITranslateService translateService) : base(translateService, sharedService)
        {
            if (IsInDesignMode)
            {
                Profile = new UserViewModel();
                Profile.Id = Guid.NewGuid();
                Profile.FullName = "Linh Nguyen";
                Profile.Birthday = 777225600000;
                Profile.Photo = "https://via.placeholder.com/512x512";
                Profile.Role = UserRoles.Admin;
            }

            // Service binding.
            _sharedService = sharedService;
            var updateProfileSubject = _sharedService.FindEventSubject(EventNameConstant.UpdateUserProfile);
            updateProfileSubject.Subscribe(profile => { Profile = profile as UserViewModel; });
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Profile info
        /// </summary>
        private UserViewModel _user;

        /// <summary>
        ///     User profile.
        /// </summary>
        public UserViewModel Profile
        {
            get => _user;
            set => Set(nameof(Profile), ref _user, value);
        }

        private readonly IMessageBusService _sharedService;

        #endregion

        #region Methods

        #endregion
    }
}