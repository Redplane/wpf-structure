using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Navigation;
using GalaSoft.MvvmLight.Command;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.Constants;
using Sodakoq.Main.Enumerations;
using Sodakoq.Main.Models;
using Sodakoq.Main.Modules.DashboardModule;
using Sodakoq.Main.Modules.LoginModule;
using Sodakoq.Main.Modules.ProjectModule;
using Sodakoq.Main.Properties;
using Sodakoq.Main.Services.Interfaces;
using Sodakoq.Main.Views;

namespace Sodakoq.Main.Controllers
{
    /// <summary>
    ///     This class contains properties that the main View can data bind to.
    ///     <para>
    ///         Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    ///     </para>
    ///     <para>
    ///         You can also use Blend to data bind with the tool's support.
    ///     </para>
    ///     <para>
    ///         See http://www.galasoft.ch/mvvm
    ///     </para>
    /// </summary>
    public class MainController : AppControllerBase
    {
        #region Constructor

        public MainController(IUserService userService, IMessageBusService messageBusService,
            ITranslateService translateService,
            IKeyValueCacheService keyValueCacheService) : base(translateService)
        {
            // Property initialization.
            MainSnackBarMessageQueue = new SnackbarMessageQueue();

            var availableMainFramePages = new Dictionary<string, MainFramePages>();
            availableMainFramePages.Add(Resources.TITLE_USER_PROFILE, MainFramePages.ProfileManagement);
            availableMainFramePages.Add(Resources.TITLE_SKILLS, MainFramePages.SkillManagement);
            availableMainFramePages.Add(Resources.TITLE_PROJECTS, MainFramePages.ProjectManagement);
            availableMainFramePages.Add(Resources.TITLE_SKILL_CATEGORIES, MainFramePages.SkillCategoryManagement);
            AvailableMainFramePages = availableMainFramePages;

            MainPopupMenuItems = new List<KeyValuePair<string, MainPopupMenuItems>>();
            MainPopupMenuItems.Add(new KeyValuePair<string, MainPopupMenuItems>(Resources.TITLE_SIGN_OUT,
                Enumerations.MainPopupMenuItems.SignOut));

            // Relay command initialization.
            OnMainFrameLoadedRelayCommand = new RelayCommand<Frame>(OnMainFrameLoaded);
            OnPopupMenuItemClicked = new RelayCommand<MainPopupMenuItems>(OnPopupMenuClicked);
            OnDialogHostLoadedRelayCommand = new RelayCommand<DialogHost>(OnDialogHostLoadedCommand);

            // Message channel registration.
            var updateMainViewTitleSubject = messageBusService.FindEventSubject(EventNameConstant.UpdateMainViewTitle);
            updateMainViewTitleSubject.Subscribe(OnMainViewTitleUpdated);

            // Service handle.
            _messageBusService = messageBusService;
            _userService = userService;
            _localKeyValueCacheService = keyValueCacheService;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Main frame.
        /// </summary>
        private Frame _frame;

        /// <summary>
        ///     Application title.
        /// </summary>
        private string _title;

        /// <summary>
        ///     Application title.
        /// </summary>
        public string Title
        {
            get => _title;
            set => Set(nameof(Title), ref _title, value);
        }

        /// <summary>
        ///     Available page in the main frame.
        /// </summary>
        private IDictionary<string, MainFramePages> _availableMainFramePages;

        /// <summary>
        ///     Available pages in the main frame.
        /// </summary>
        public IDictionary<string, MainFramePages> AvailableMainFramePages
        {
            get => _availableMainFramePages;
            set => Set(nameof(AvailableMainFramePages), ref _availableMainFramePages, value);
        }

        /// <summary>
        ///     The page which has been selected.
        /// </summary>
        private MainFramePages _selectedPage;

        /// <summary>
        ///     Get | set selected page.
        /// </summary>
        public MainFramePages SelectedPage
        {
            get => _selectedPage;
            set
            {
                Set(nameof(SelectedPage), ref _selectedPage, value);
                if (_navigationService != null)
                    switch (value)
                    {
                        case MainFramePages.ProfileManagement:
                            _frame.Content = new DashboardPage();
                            break;

                        case MainFramePages.ProjectManagement:
                            _frame.Content = new ProjectView();
                            break;

                        case MainFramePages.SkillCategoryManagement:
                            _frame.Content = new SkillCategoryView();
                            break;

                        case MainFramePages.SkillManagement:
                            _frame.Content = new SkillView();
                            break;

                        default:
                            break;
                    }
            }
        }

        /// <summary>
        ///     Snackbar of main view.
        /// </summary>
        public SnackbarMessageQueue MainSnackBarMessageQueue { get; set; }

        /// <summary>
        ///     Main popup menu items.
        /// </summary>
        public IList<KeyValuePair<string, MainPopupMenuItems>> MainPopupMenuItems { get; }

        /// <summary>
        ///     Main view dialog host.
        /// </summary>
        public string MainViewDialogHost => nameof(MainViewDialogHost);

        /// <summary>
        ///     Whether dialog is shown or not.
        /// </summary>
        private bool _bIsPopupShown;

        /// <summary>
        ///     Whether dialog is shown or not.
        /// </summary>
        public bool IsPopupShown
        {
            get => _bIsPopupShown;
            set => Set(nameof(IsPopupShown), ref _bIsPopupShown, value);
        }

        /// <summary>
        ///     Dialog host which is for displaying popup.
        /// </summary>
        private DialogHost _dialogHost;

        #endregion

        #region Relay commands

        /// <summary>
        ///     Raised when main popup item is clicked.
        /// </summary>
        public RelayCommand<MainPopupMenuItems> OnPopupMenuItemClicked { get; }

        /// <summary>
        ///     Raised when main frame is loaded
        /// </summary>
        public RelayCommand<Frame> OnMainFrameLoadedRelayCommand { get; }

        /// <summary>
        ///     Raised when dialog host is loaded.
        /// </summary>
        public RelayCommand<DialogHost> OnDialogHostLoadedRelayCommand { get; }

        #endregion

        #region  Services

        /// <summary>
        ///     Navigation service.
        /// </summary>
        private NavigationService _navigationService;

        /// <summary>
        ///     Services that handles message operations.
        /// </summary>
        private readonly IMessageBusService _messageBusService;

        /// <summary>
        ///     Service that handles user operations.
        /// </summary>
        private readonly IUserService _userService;

        /// <summary>
        ///     Service that handles key-value cache services.
        /// </summary>
        private readonly IKeyValueCacheService _localKeyValueCacheService;

        #endregion

        #region Methods

        /// <summary>
        ///     Called when main frame is loaded.
        /// </summary>
        /// <param name="frame"></param>
        protected virtual void OnMainFrameLoaded(Frame frame)
        {
            _frame = frame;
            _navigationService = _frame.NavigationService;
            _navigationService.Navigated += OnNavigationCompleted;

            // Set default title.
            Title = "TITLE_CV_MANAGEMENT";

            // Default page selection.
            SelectedPage = MainFramePages.ProjectManagement;

            // Event registration.
            _messageBusService
                .FindEventSubject(EventNameConstant.NavigateMainFrame)
                .Subscribe(OnMainFrameNavigated);
        }

        /// <summary>
        ///     Called when navigation completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="navigationEventArgs"></param>
        protected virtual void OnNavigationCompleted(object sender, NavigationEventArgs navigationEventArgs)
        {
            _navigationService.RemoveBackEntry();
        }

        /// <summary>
        ///     Called when main view title is updated.
        /// </summary>
        /// <param name="message"></param>
        protected virtual void OnMainViewTitleUpdated(object message)
        {
            if (!(message is string title))
                return;

            Title = title;
        }

        /// <summary>
        ///     Called when popup menu item is clicked.
        /// </summary>
        /// <param name="mainPopupMenuItems"></param>
        protected virtual async void OnPopupMenuClicked(MainPopupMenuItems mainPopupMenuItems)
        {
            if (mainPopupMenuItems == Enumerations.MainPopupMenuItems.SignOut)
                await LogoutAsync();
        }

        /// <summary>
        ///     Log user out of system.
        /// </summary>
        protected virtual async Task LogoutAsync()
        {
            // Clear local cache.
            await _userService.ClearLoginResultFromDbAsync();

            // Remove access token from in-memory cache.
            _localKeyValueCacheService.RemoveItem(InMemoryKeyConstant.AccessToken);

            _messageBusService.PublishEvent(EventNameConstant.NavigateAppFrame, new LoginPage());
        }

        #endregion

        #region Events

        /// <summary>
        ///     Called when dialog host loaded.
        /// </summary>
        /// <param name="dialogHost"></param>
        protected virtual void OnDialogHostLoadedCommand(DialogHost dialogHost)
        {
            _dialogHost = dialogHost;
        }

        /// <summary>
        ///     Called when main frame is navigated.
        /// </summary>
        /// <param name="state"></param>
        protected virtual void OnMainFrameNavigated(object state)
        {
            if (!(state is MainFrameNavigationModel navigationModel))
                return;

            SelectedPage = navigationModel.TargetedPage;
        }

        #endregion
    }
}