/*
  In AppContext.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:MainClient"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using System;
using System.Net.Http;
using AutoMapper;
using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;
using Sodakoq.Domain.Models.Contexts;
using Sodakoq.Main.Controllers.Dialogs;
using Sodakoq.Main.Interceptors;
using Sodakoq.Main.Models.AutoMapperProfiles;
using Sodakoq.Main.Modules.DashboardModule;
using Sodakoq.Main.Modules.LoginModule;
using Sodakoq.Main.Modules.ProjectModule;
using Sodakoq.Main.Modules.SplashModule;
using Sodakoq.Main.Services.Implementations;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Controllers
{
    /// <summary>
    ///     This class contains static references to all the view models in the
    ///     application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}


            // Register database context.
            SimpleIoc.Default.Register<CvManagementDbContext>();

            // Initialize http client handler.
            SimpleIoc.Default.Register<HttpClientInterceptor>();
            SimpleIoc.Default.Register(() =>
            {
                // Get registered interceptor.
                var httpClientInterceptor = SimpleIoc.Default.GetInstance<HttpClientInterceptor>();

                // Initialize HttpClient.
                var httpClient = new HttpClient(httpClientInterceptor);
                //httpClient.BaseAddress = new Uri("https://ge2domjuhj3gk4ttnfxw4lzrfyydu3lpmnvwk4roobzgs43nfz4w23a.prism.stoplight.io/");
                //httpClient.BaseAddress = new Uri("http://localhost:28000/");
                httpClient.BaseAddress = new Uri("http://localhost:61356/");

                // By default, content should be in json format.
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                return httpClient;
            });

            SimpleIoc.Default.Register<IUserService, UserService>();
            SimpleIoc.Default.Register<IProjectService, ProjectService>();
            SimpleIoc.Default.Register<ISkillService, SkillService>();
            SimpleIoc.Default.Register<ISkillCategoryService, SkillCategoryService>();
            SimpleIoc.Default.Register<IValidationService, ValidationService>();
            SimpleIoc.Default.Register<IConnectivityService, ConnectivityService>();
            SimpleIoc.Default.Register<ITranslateService, TranslateService>();
            SimpleIoc.Default.Register<IMessageBusService, MessageBusService>();
            SimpleIoc.Default.Register<IFileService, FileService>();
            SimpleIoc.Default.Register<IKeyValueCacheService, KeyValueCacheService>();

            // Implement view model.
            SimpleIoc.Default.Register<MainController>();
            SimpleIoc.Default.Register<AppController>();
            SimpleIoc.Default.Register<LoginViewModel>();
            SimpleIoc.Default.Register<UserController>();
            SimpleIoc.Default.Register<SplashViewModel>();
            SimpleIoc.Default.Register<DashboardViewModel>();
            SimpleIoc.Default.Register<SkillCategoryController>();
            SimpleIoc.Default.Register<AddEditSkillCategoryDialogController>();
            SimpleIoc.Default.Register<DeleteSkillCategoryDialogController>();
            SimpleIoc.Default.Register<SkillController>();
            SimpleIoc.Default.Register<ProjectManagementViewModel>();

            var mapperConfiguration = new MapperConfiguration(expression =>
            {
                expression.AddProfile<AutoMapperMainProfile>();
            });
            
            IMapper mapper = new Mapper(mapperConfiguration);
            SimpleIoc.Default.Register(() => mapper);
        }

        #endregion

        public static void Cleanup()
        {
        }

        #region Properties

        #endregion

        #region Prperties

        /// <summary>
        ///     Main page context
        /// </summary>
        public MainController MainViewContext => ServiceLocator.Current.GetInstance<MainController>();

        /// <summary>
        ///     Application context
        /// </summary>
        public AppController AppContext => ServiceLocator.Current.GetInstance<AppController>();

        /// <summary>
        ///     Login context
        /// </summary>
        public LoginViewModel LoginContext => ServiceLocator.Current.GetInstance<LoginViewModel>();

        /// <summary>
        ///     User dashboard context
        /// </summary>
        public UserController UserDashboard => ServiceLocator.Current.GetInstance<UserController>();

        /// <summary>
        ///     Splash window context.
        /// </summary>
        public SplashViewModel SplashContext => ServiceLocator.Current.GetInstance<SplashViewModel>();

        /// <summary>
        ///     Context for profile page.
        /// </summary>
        public DashboardViewModel DashboardContext => ServiceLocator.Current.GetInstance<DashboardViewModel>();

        /// <summary>
        ///     Skill category management context.
        /// </summary>
        public SkillCategoryController SkillCategoryManagementContext =>
            ServiceLocator.Current.GetInstance<SkillCategoryController>();

        /// <summary>
        ///     Add | edit skill category context.
        /// </summary>
        public AddEditSkillCategoryDialogController AddEditSkillCategoryDialogContext =>
            ServiceLocator.Current.GetInstance<AddEditSkillCategoryDialogController>();

        /// <summary>
        ///     Delete skill category context.
        /// </summary>
        public DeleteSkillCategoryDialogController DeleteSkillCategoryDialogContext =>
            ServiceLocator.Current.GetInstance<DeleteSkillCategoryDialogController>();

        /// <summary>
        ///     Skill management context.
        /// </summary>
        public SkillController SkillManagementContext =>
            ServiceLocator.Current.GetInstance<SkillController>();

        /// <summary>
        ///     Project management context.
        /// </summary>
        public ProjectManagementViewModel ProjectManagementContext =>
            ServiceLocator.Current.GetInstance<ProjectManagementViewModel>();

        #endregion
    }
}