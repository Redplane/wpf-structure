﻿using System.Windows.Controls;

namespace Sodakoq.Main.Modules.ProjectModule.DetailedProjectModal
{
    /// <summary>
    ///     Interaction logic for AddEditProjectDialogView.xaml
    /// </summary>
    public partial class DetailedProjectModalPage : UserControl
    {
        public DetailedProjectModalPage()
        {
            InitializeComponent();
        }
    }
}