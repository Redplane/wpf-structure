﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AutoMapper;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.DataTransferObjects.Projects;
using Sodakoq.Main.Models;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Modules.ProjectModule.DetailedProjectModal
{
    public class DetailedProjectModalViewModel : ViewModelBase
    {
        #region Constructor

        public DetailedProjectModalViewModel()
        {
            // List of checkbox items in the system.
            _addedResponsibilities = new List<CheckboxItemModel<string>>();

            // Initialize model binding.
            Model = new AddEditProjectViewModel();
            AddedResponsibilities = new List<CheckboxItemModel<string>>();

            // Service binding.
            _validationService = SimpleIoc.Default.GetInstance<IValidationService>();
            _mapper = SimpleIoc.Default.GetInstance<IMapper>();

            // Relay commands registration.
            ClickSelectAllResponsibilitiesRelayCommand = new RelayCommand<ListView>(SelectAllResponsibilitiesCommand);
            ClickToggleAddResponsibilityRelayCommand = new RelayCommand(ClickToggleAddResponsibilityCommand);
            ClickAddResponsibilityRelayCommand = new RelayCommand(ClickAddResponsibility);
            ClickDeleteSelectedResponsibilitiesRelayCommand =
                new RelayCommand(ClickDeleteSelectedResponsibilitiesCommand);
            ClickCancelRelayCommand = new RelayCommand(ClickCancelCommand);
            ClickOkRelayCommand = new RelayCommand<DependencyObject>(ClickOkCommand);
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Model for information binding.
        /// </summary>
        private AddEditProjectViewModel _model;

        /// <summary>
        ///     Model for information binding.
        /// </summary>
        public AddEditProjectViewModel Model
        {
            get => _model;
            set
            {
                if (value == null)
                    _model = new AddEditProjectViewModel();
                else
                    _model = value;

                // Update checkbox items.
                AddedResponsibilities = _model.Responsibilities
                    .Select(x => new CheckboxItemModel<string>(x, x, false))
                    .ToList();

                RaisePropertyChanged(nameof(Model));
            }
        }

        /// <summary>
        ///     Whether project has been finished or not.
        /// </summary>
        private bool _bHasProjectFinished;

        /// <summary>
        ///     Whether project has been finished or not.
        /// </summary>
        public bool HasProjectFinished
        {
            get => _bHasProjectFinished;
            set => Set(nameof(HasProjectFinished), ref _bHasProjectFinished, value);
        }

        /// <summary>
        ///     Whether all responsibilities have been selected or not.
        /// </summary>
        private bool _bHaveAllResponsibilitiesSelected;

        /// <summary>
        ///     Whether all responsibilities have been selected or not.
        /// </summary>
        public bool HaveAllResponsibilitiesSelected
        {
            get => _bHaveAllResponsibilitiesSelected;
            private set => Set(nameof(HaveAllResponsibilitiesSelected), ref _bHaveAllResponsibilitiesSelected, value);
        }


        /// <summary>
        ///     Whether any responsibility selected.
        /// </summary>
        private bool _bHasAnyResponsibilitySelected;

        /// <summary>
        ///     Whether any responsibilities have been selected or not.
        /// </summary>
        public bool HasAnyResponsibilitySelected
        {
            get => _bHasAnyResponsibilitySelected;
            private set => Set(nameof(HasAnyResponsibilitySelected), ref _bHasAnyResponsibilitySelected, value);
        }

        /// <summary>
        ///     Checkbox items.
        /// </summary>
        private List<CheckboxItemModel<string>> _addedResponsibilities;

        /// <summary>
        ///     Checkbox items.
        /// </summary>
        public List<CheckboxItemModel<string>> AddedResponsibilities
        {
            get => _addedResponsibilities;
            set
            {
                Set(nameof(AddedResponsibilities), ref _addedResponsibilities, value);

                // Validate checkbox.
                CheckAnyResponsibilitySelected();
                CheckAllResponsibilitiesSelected();

                // Event registration.
                if (value != null)
                    foreach (var item in value)
                        item.OnItemSelectionChanged += OnItemSelectionChanged;
            }
        }

        /// <summary>
        ///     Whether responsibility is being added or not.
        /// </summary>
        private bool _bIsAddingResponsibility;

        /// <summary>
        ///     Whether responsibility is being added or not.
        /// </summary>
        public bool IsAddingResponsibility
        {
            get => _bIsAddingResponsibility;
            set => Set(nameof(IsAddingResponsibility), ref _bIsAddingResponsibility, value);
        }

        /// <summary>
        ///     Responsibility which is being added to project.
        /// </summary>
        private string _addingResponsibility;

        /// <summary>
        ///     Responsibility which is being added to project.
        /// </summary>
        public string AddingResponsibility
        {
            get => _addingResponsibility;
            set => Set(nameof(AddingResponsibility), ref _addingResponsibility, value);
        }

        /// <summary>
        ///     Responsibility name which is input by user for searching.
        /// </summary>
        private string _typedResponsibilityName;

        /// <summary>
        ///     Responsibility name which is input by user for searching.
        /// </summary>
        public string TypedResponsibilityName
        {
            get => _typedResponsibilityName;
            set => Set(nameof(TypedResponsibilityName), ref _typedResponsibilityName, value);
        }

        #endregion

        #region Relay commands

        /// <summary>
        ///     Relay command which is fired when all responsibilities should be selected.
        /// </summary>
        public RelayCommand<ListView> ClickSelectAllResponsibilitiesRelayCommand { get; }

        /// <summary>
        ///     Relay command which is fired when add responsibility functionality is toggled.
        /// </summary>
        public RelayCommand ClickToggleAddResponsibilityRelayCommand { get; }

        /// <summary>
        ///     Relay command which is fired when add responsibility button is clicked.
        /// </summary>
        public RelayCommand ClickAddResponsibilityRelayCommand { get; }

        /// <summary>
        ///     Relay command which is fired when delete selected responsibilities button is clicked.
        /// </summary>
        public RelayCommand ClickDeleteSelectedResponsibilitiesRelayCommand { get; }

        /// <summary>
        ///     Relay command which ís fired when cancel button is clicked.
        /// </summary>
        public RelayCommand ClickCancelRelayCommand { get; }

        /// <summary>
        ///     Relay command which is fired when ok button is clicked.
        /// </summary>
        public RelayCommand<DependencyObject> ClickOkRelayCommand { get; }
        
        #endregion

        #region Services

        private readonly IValidationService _validationService;

        private readonly IMapper _mapper;

        #endregion

        #region Methods

        /// <summary>
        ///     Command which is for selecting all responsibilities in the project.
        /// </summary>
        protected virtual void SelectAllResponsibilitiesCommand(ListView listView)
        {
            var haveAllResponsibilitiesSelected = HaveAllResponsibilitiesSelected;
            var addedResponsibilities = new List<CheckboxItemModel<string>>(_addedResponsibilities);
            foreach (var addedResponsibility in addedResponsibilities)
                addedResponsibility.IsSelected = !haveAllResponsibilitiesSelected;

            AddedResponsibilities = addedResponsibilities;
        }

        /// <summary>
        ///     Command which is for toggling add responsibility in the project.
        /// </summary>
        protected virtual void ClickToggleAddResponsibilityCommand()
        {
            IsAddingResponsibility = !IsAddingResponsibility;
        }

        /// <summary>
        ///     Command which is called when add responsibility is clicked.
        /// </summary>
        protected virtual void ClickAddResponsibility()
        {
            if (string.IsNullOrEmpty(AddingResponsibility))
                return;

            if (AddedResponsibilities.Any(x =>
                x.Value.Equals(AddingResponsibility, StringComparison.InvariantCultureIgnoreCase)))
                return;

            var addedResponsibilities = new List<CheckboxItemModel<string>>(AddedResponsibilities);
            addedResponsibilities.Add(new CheckboxItemModel<string>(AddingResponsibility, AddingResponsibility, false));
            AddedResponsibilities = new List<CheckboxItemModel<string>>(addedResponsibilities);
            AddingResponsibility = "";
        }

        /// <summary>
        ///     Called when delete selected responsibilities button is clicked.
        /// </summary>
        protected virtual void ClickDeleteSelectedResponsibilitiesCommand()
        {
            var availableResponsibilities = _addedResponsibilities.Where(x => !x.IsSelected).ToList();
            AddedResponsibilities = new List<CheckboxItemModel<string>>(availableResponsibilities);
        }

        /// <summary>
        ///     Check whether any responsibility selected.
        /// </summary>
        protected virtual void CheckAnyResponsibilitySelected()
        {
            HasAnyResponsibilitySelected = AddedResponsibilities.Any(x => x.IsSelected);
        }

        /// <summary>
        ///     Check whether all responsibilities have been selected or not.
        /// </summary>
        protected virtual void CheckAllResponsibilitiesSelected()
        {
            HaveAllResponsibilitiesSelected = !AddedResponsibilities.Any(x => !x.IsSelected);
        }

        /// <summary>
        ///     Called when item is selected.
        /// </summary>
        /// <param name="bIsChecked"></param>
        /// <param name="checkboxItemModel"></param>
        protected virtual void OnItemSelectionChanged(bool bIsChecked, CheckboxItemModel<string> checkboxItemModel)
        {
            CheckAnyResponsibilitySelected();
            CheckAllResponsibilitiesSelected();
        }

        /// <summary>
        ///     Called when cancel button is clicked.
        /// </summary>
        protected virtual void ClickCancelCommand()
        {
            DialogHost.CloseDialogCommand.Execute(null, null);
        }

        /// <summary>
        ///     Called when ok button is clicked.
        /// </summary>
        protected virtual void ClickOkCommand(DependencyObject dependencyObject)
        {
            if (!_validationService.IsControlValid(dependencyObject))
                return;

            if (AddedResponsibilities != null && AddedResponsibilities.Count > 0)
                Model.Responsibilities = AddedResponsibilities.Select(x => x.Name).ToList();

            DialogHost.CloseDialogCommand.Execute(Model, null);
        }

        /// <summary>
        ///     Setup model.
        /// </summary>
        /// <param name="project"></param>
        public void SetupModel(SkilledProjectViewModel project)
        {
            if (project == null)
                return;

            var model = _mapper.Map<AddEditProjectViewModel>(project);
            if (model == null)
            {
                model = new AddEditProjectViewModel();
                HasProjectFinished = false;
            }
            else
            {
                HasProjectFinished = model.FinishedTime != null;
            }

            Model = model;
        }

        #endregion
    }
}