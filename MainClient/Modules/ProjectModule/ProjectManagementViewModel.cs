﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.Constants;
using Sodakoq.Main.Controllers.Dialogs;
using Sodakoq.Main.DataTransferObjects;
using Sodakoq.Main.DataTransferObjects.Projects;
using Sodakoq.Main.DataTransferObjects.Skills;
using Sodakoq.Main.Models;
using Sodakoq.Main.Models.Projects;
using Sodakoq.Main.Modules.CommonModule.LoadingModalModule;
using Sodakoq.Main.Modules.ProjectModule.DetailedProjectModal;
using Sodakoq.Main.Properties;
using Sodakoq.Main.Services.Interfaces;
using Sodakoq.Main.Views.Dialogs;

namespace Sodakoq.Main.Modules.ProjectModule
{
    public class ProjectManagementViewModel : ViewModelBase
    {
        #region Constructor

        public ProjectManagementViewModel(IMapper mapper, IProjectService projectService,
            ITranslateService translateService, IMessageBusService messageBusService)
        {
            // Property binding.
            _loadProjectModel = new LoadProjectViewModel();
            _loadProjectModel.Pagination = new PaginationViewModel(1, PaginationConstant.MaxProjectsInDashboard);

            // Service binding.
            _mapper = mapper;
            _projectService = projectService;
            _translateService = translateService;
            _messageBusService = messageBusService;

            // Relay command registration.
            ClickEditAddProjectRelayCommand = new RelayCommand<SkilledProjectViewModel>(AddEditProjectCommand);
            ClickReloadProjectsRelayCommand = new RelayCommand(ClickReloadProjectsCommand);
            ClickDeleteProjectRelayCommand = new RelayCommand<SkilledProjectViewModel>(ClickDeleteProjectCommand);
            OnPageLoadedRelayCommand = new RelayCommand(OnPageLoadedCommand);
            OnAddEditProjectSkillClickedRelayCommand = new RelayCommand<SkilledProjectViewModel>(OnAddEditProjectSkillClicked);
        }
        
        #endregion

        #region Services

        private readonly IMapper _mapper;

        /// <summary>
        ///     Service to handle projects operation.
        /// </summary>
        private readonly IProjectService _projectService;

        /// <summary>
        ///     Service to handle translation.
        /// </summary>
        private readonly ITranslateService _translateService;

        /// <summary>
        ///     Services to handle shared operations.
        /// </summary>
        private readonly IMessageBusService _messageBusService;

        #endregion

        #region Properties

        /// <summary>
        ///     List of projects that user has.
        /// </summary>
        private List<SkilledProjectViewModel> _skilledProjects;

        /// <summary>
        ///     Get list of projects that user has.
        /// </summary>
        public List<SkilledProjectViewModel> Projects
        {
            get => _skilledProjects;
            private set => Set(nameof(Projects), ref _skilledProjects, value);
        }

        /// <summary>
        ///     Load projects result.
        /// </summary>
        private SearchResult<List<SkilledProjectViewModel>> _loadProjectsResult;

        /// <summary>
        ///     Load projects result.
        /// </summary>
        public SearchResult<List<SkilledProjectViewModel>> LoadProjectsResult
        {
            get => _loadProjectsResult;
            private set
            {
                Set(nameof(LoadProjectsResult), ref _loadProjectsResult, value);
                Projects = value?.Records;
            }
        }

        /// <summary>
        ///     Model for loading project.
        /// </summary>
        private readonly LoadProjectViewModel _loadProjectModel;

        /// <summary>
        ///     Project management dialog host.
        /// </summary>
        public string ProjectManagementDialogHost => nameof(ProjectManagementDialogHost);

        /// <summary>
        ///     Whether popup is shown or not.
        /// </summary>
        private bool _bIsPopupShown;

        /// <summary>
        ///     Whether popup is shown or not.
        /// </summary>
        public bool IsPopupShown
        {
            get => _bIsPopupShown;
            set => Set(nameof(IsPopupShown), ref _bIsPopupShown, value);
        }

        /// <summary>
        /// Cancellation token source for adding skills into projects.
        /// </summary>
        private CancellationTokenSource _addSkillsIntoProjectCancellationTokenSource;

        #endregion

        #region Cancellation token source

        /// <summary>
        ///     Load project cancellation token source.
        /// </summary>
        private CancellationTokenSource _loadProjectsCancellationTokenSource;

        /// <summary>
        ///     Add project cancellation token source.
        /// </summary>
        private CancellationTokenSource _addProjectCancellationTokenSource;

        /// <summary>
        ///     Edit project cancellation token source.
        /// </summary>
        private CancellationTokenSource _editProjectCancellationTokenSource;

        #endregion

        #region Relay command

        /// <summary>
        ///     Relay command which is fired when add project button is clicked.
        /// </summary>
        public RelayCommand<SkilledProjectViewModel> ClickEditAddProjectRelayCommand { get; }

        /// <summary>
        ///     Relay command which is fired when reload project button is clicked.
        /// </summary>
        public RelayCommand ClickReloadProjectsRelayCommand { get; }

        /// <summary>
        ///     Relay command which is fired when delete project button is clicked.
        /// </summary>
        public RelayCommand<SkilledProjectViewModel> ClickDeleteProjectRelayCommand { get; }

        /// <summary>
        ///     Fired when page is loaded.
        /// </summary>
        public RelayCommand OnPageLoadedRelayCommand { get; }

        /// <summary>
        /// Raised when add/edit project skills button is clicked.
        /// </summary>
        public RelayCommand<SkilledProjectViewModel> OnAddEditProjectSkillClickedRelayCommand { get; }
        
        #endregion

        #region Methods

        /// <summary>
        ///     Show project editor popup asynchronously.
        /// </summary>
        /// <param name="initialModel"></param>
        /// <returns></returns>
        protected virtual async Task<AddEditProjectViewModel> ShowProjectEditorPopupAsync(
            SkilledProjectViewModel initialModel)
        {
            var view = new DetailedProjectModalPage();
            var viewModel = new DetailedProjectModalViewModel();
            viewModel.SetupModel(initialModel);
            view.DataContext = viewModel;

            var output = await DialogHost.Show(view, ProjectManagementDialogHost);
            if (output == null || !(output is AddEditProjectViewModel addEditProjectModel))
                return null;

            return addEditProjectModel;
        }

        /// <summary>
        ///     Add project to system asynchronously.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="startedTime"></param>
        /// <param name="finishedTime"></param>
        /// <param name="responsibilities"></param>
        /// <returns></returns>
        protected virtual async Task<DataTransferObjects.Projects.ProjectViewModel> AddProjectAsync(string name, string description,
            double startedTime,
            double? finishedTime, List<string> responsibilities)
        {
            // Cancel previous task.
            if (_addProjectCancellationTokenSource != null &&
                !_addProjectCancellationTokenSource.IsCancellationRequested)
                _addProjectCancellationTokenSource.Cancel();

            _addProjectCancellationTokenSource =
                new CancellationTokenSource(TimeSpan.FromSeconds(TimeoutConstant.DefaultTimeout));

            var addProjectModel = new AddProjectViewModel(name,
                description,
                startedTime, finishedTime,
                responsibilities);

            // Add project asynchronously.
            return
                await _projectService.AddProjectAsync(addProjectModel, _addProjectCancellationTokenSource.Token);
        }

        /// <summary>
        ///     Edit project in the system asynchronously.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="startedTime"></param>
        /// <param name="finishedTime"></param>
        /// <param name="responsibilities"></param>
        /// <returns></returns>
        protected virtual async Task<DataTransferObjects.Projects.ProjectViewModel> EditProjectAsync(Guid id, string name, string description,
            double startedTime,
            double? finishedTime, List<string> responsibilities)
        {
            if (_editProjectCancellationTokenSource != null &&
                !_editProjectCancellationTokenSource.IsCancellationRequested)
                _editProjectCancellationTokenSource.Cancel();

            _editProjectCancellationTokenSource =
                new CancellationTokenSource(TimeSpan.FromSeconds(TimeoutConstant.DefaultTimeout));

            var editProjectModel = new EditProjectViewModel(name, description,
                startedTime,
                finishedTime,
                responsibilities);

            return await _projectService.EditProjectAsync(id, editProjectModel,
                _editProjectCancellationTokenSource.Token);
        }

        /// <summary>
        ///     Command which is raised when add project button is clicked.
        /// </summary>
        protected virtual async void AddEditProjectCommand(SkilledProjectViewModel model)
        {
            var view = new DetailedProjectModalPage();
            var viewModel = new DetailedProjectModalViewModel();
            viewModel.SetupModel(model);
            view.DataContext = viewModel;

            var dialogModel = await DialogHost.Show(view, ProjectManagementDialogHost);
            if (dialogModel == null)
                return;

            if (!(dialogModel is AddEditProjectViewModel addEditProjectModel))
                return;

            // Initialize add edit project task.
            Task addEditProjectTask;
            Task displayLoadingDialogTask;

            // Display loading dialog.
            var loadingDialogView = new LoadingIndicatorDialogView();
            var loadingDialogViewModel = new LoadingModalViewModel();
            loadingDialogViewModel.IsCancelAvailable = false;
            loadingDialogView.DataContext = loadingDialogViewModel;

            // Close previously displayed dialog.
            IsPopupShown = false;

            if (addEditProjectModel.Id == null)
            {
                // Add project asynchronously.
                addEditProjectTask = AddProjectAsync(addEditProjectModel.Name, addEditProjectModel.Description,
                    addEditProjectModel.StartedTime, addEditProjectModel.FinishedTime,
                    addEditProjectModel.Responsibilities);

                displayLoadingDialogTask = DialogHost.Show(loadingDialogView, ProjectManagementDialogHost,
                    OnAddProjectOperationCancelled);
            }
            else
            {
                addEditProjectTask = EditProjectAsync(addEditProjectModel.Id.Value, addEditProjectModel.Name,
                    addEditProjectModel.Description, addEditProjectModel.StartedTime,
                    addEditProjectModel.FinishedTime, addEditProjectModel.Responsibilities);

                displayLoadingDialogTask = DialogHost.Show(loadingDialogView, ProjectManagementDialogHost,
                    OnEditProjectOperationCancelled);
            }

            try
            {
                // Wait for all task to complete.
                var completedTask = await Task.WhenAny(addEditProjectTask, displayLoadingDialogTask);
                if (completedTask != addEditProjectTask || completedTask.IsCanceled)
                    return;

                // Display snack message.
                var snackMessage = new SnackbarMessageModel();
                snackMessage.Content = addEditProjectModel.Id != null
                    ? Resources.MSG_ADD_PROJECT_SUCCESSFULLY
                    : Resources.MSG_EDIT_PROJECT_SUCCESSFULLY;
                snackMessage.ActionContent = Resources.TITLE_DISMISS;
                snackMessage.ActionHandler = x => { };
                _messageBusService.PublishEvent(EventNameConstant.AddAppSnackbarMessage, snackMessage);

                // Wait for all tasks to be completed.
                LoadProjectsResult = await ReloadProjectsAsync();
            }
            catch (Exception exception)
            {
                if (exception is TaskCanceledException)
                    return;

                throw;
            }
            finally
            {
                IsPopupShown = false;
            }
        }

        /// <summary>
        ///     Command which is raised when reload project button is clicked.
        /// </summary>
        protected virtual async void ClickReloadProjectsCommand()
        {
            LoadProjectsResult = await ReloadProjectsAsync();
        }

        /// <summary>
        ///     Command which is raised when delete project button is clicked.
        /// </summary>
        protected virtual async void ClickDeleteProjectCommand(DataTransferObjects.Projects.ProjectViewModel project)
        {
            // Display confirmation message.
            var translatedMessage = _translateService.LoadTranslatedMessage("MSG_SURE_TO_DELETE_PROJECT", project);
            var view = new YesNoDialogView();
            var viewModel = new YesNoDialogController();
            viewModel.Message = translatedMessage;
            view.DataContext = viewModel;

            var dialogResult = await DialogHost.Show(view, ProjectManagementDialogHost);
            if (!dialogResult.Equals(true))
                return;

            // Display loading dialog view.
            var loadingDialogView = new LoadingIndicatorDialogView();
            var loadingDialogViewModel = new LoadingModalViewModel();
            loadingDialogViewModel.IsCancelAvailable = false;
            var displayLoadingDialogTask = DialogHost.Show(loadingDialogView, ProjectManagementDialogHost);

            // Call api to delete project.
            var deleteProjectTask = _projectService.DeleteProjectAsync(project.Id);

            try
            {
                var completedTask = await Task.WhenAny(displayLoadingDialogTask, deleteProjectTask);
                var snackbarMessage = new SnackbarMessageModel();
                snackbarMessage.ActionContent = Resources.TITLE_DISMISS;
                snackbarMessage.ActionHandler = b => { };

                if (completedTask != deleteProjectTask)
                {
                    snackbarMessage.Content = "MSG_FAIL_TO_DELETE_PROJECT";
                    _messageBusService.PublishEvent(EventNameConstant.AddAppSnackbarMessage, snackbarMessage);
                    return;
                }

                // Publish snackbar message task.
                snackbarMessage.Content = "MSG_DELETE_PROJECT_SUCCESSFULLY";
                _messageBusService.PublishEvent(EventNameConstant.AddAppSnackbarMessage, snackbarMessage);

                // Reload projects list.
                var reloadProjectsTask = ReloadProjectsAsync();
                LoadProjectsResult = await reloadProjectsTask;
            }
            catch (Exception exception)
            {
                if (exception is TaskCanceledException)
                    return;

                throw;
            }
            finally
            {
                IsPopupShown = false;
            }
        }

        /// <summary>
        ///     Reload project asynchronously.
        /// </summary>
        protected virtual async Task<SearchResult<List<SkilledProjectViewModel>>> ReloadProjectsAsync()
        {
            if (_loadProjectsCancellationTokenSource != null)
                _loadProjectsCancellationTokenSource.Cancel();
            _loadProjectsCancellationTokenSource = new CancellationTokenSource();

            // Close the previous dialog host.
            IsPopupShown = false;

            // Project loading dialog initialization.
            var loadingDialogView = new LoadingIndicatorDialogView();
            var loadingDialogViewModel = new LoadingModalViewModel();
            loadingDialogViewModel.IsCancelAvailable = true;
            loadingDialogView.DataContext = loadingDialogViewModel;

            var displayLoadingDialogTask = DialogHost.Show(loadingDialogView, ProjectManagementDialogHost,
                OnReloadProjectsLoadingDialogCancelled);

            // Load projects result task.
            var loadProjectsResultTask =
                _projectService.LoadProjectsAsync(_loadProjectModel, _loadProjectsCancellationTokenSource.Token);

            try
            {
                var completedTask = await Task.WhenAny(loadProjectsResultTask, displayLoadingDialogTask);
                if (completedTask != loadProjectsResultTask || completedTask.IsCanceled)
                    return LoadProjectsResult;

                // Load project results.
                var loadedProjectsResult = await loadProjectsResultTask;
                List<SkilledProjectViewModel> loadedProjects = null;
                if (loadedProjectsResult != null && loadedProjectsResult.Records != null)
                    loadedProjects = _mapper.Map<List<SkilledProjectViewModel>>(loadedProjectsResult.Records);

                if (loadedProjects != null && loadedProjects.Count > 0)
                {
                    // Get skills that are used in the project.
                    var loadProjectSkillCondition = new LoadProjectUsedSkillModel();
                    loadProjectSkillCondition.ProjectIds = loadedProjects.Select(x => x.Id).Distinct().ToList();
                    loadProjectSkillCondition.Pagination =
                        new PaginationViewModel(1, ValidationConstant.MaxAvailableSkillsInProject);

                    // Find project skills asynchronously.
                    var projectIdToSkillsMap = await FindProjectSkillsAsync(loadProjectSkillCondition);

                    // Update skills list into loaded projects.
                    foreach (var loadedProject in loadedProjects)
                    {
                        if (!projectIdToSkillsMap.ContainsKey(loadedProject.Id))
                            continue;

                        loadedProject.Skills = projectIdToSkillsMap[loadedProject.Id];
                    }
                }

                var loadSkilledProjectResult = new SearchResult<List<SkilledProjectViewModel>>();
                loadSkilledProjectResult.Records = loadedProjects;
                loadSkilledProjectResult.TotalRecords = loadedProjectsResult?.TotalRecords ?? 0;

                return loadSkilledProjectResult;
            }
            catch (Exception exception)
            {
                if (exception is TaskCanceledException)
                    return LoadProjectsResult;

                throw;
            }
            finally
            {
                IsPopupShown = false;
            }
        }

        /// <summary>
        ///     Reload skills that have been used by projects using specific conditions.
        /// </summary>
        protected virtual async Task<Dictionary<Guid, List<SkillViewModel>>> FindProjectSkillsAsync(
            LoadProjectUsedSkillModel model, CancellationToken cancellationToken = default(CancellationToken))
        {
            // Skills that are used in he project.
            var projectIdToSkillMap = new Dictionary<Guid, List<SkillViewModel>>();

            // Project ids are not defined.
            if (model == null || model.ProjectIds == null || model.ProjectIds.Count < 1)
                return projectIdToSkillMap;

            var loadProjectUsedSkillsResult =
                await _projectService.LoadSkilledProjectsAsync(model, cancellationToken);

            projectIdToSkillMap = loadProjectUsedSkillsResult
                ?.Records
                ?.GroupBy(x => x.Id)
                .ToDictionary(x => x.Key, x => x.SelectMany(t => t.Skills).ToList());

            if (projectIdToSkillMap == null)
                return new Dictionary<Guid, List<SkillViewModel>>();
            return projectIdToSkillMap;
        }

        /// <summary>
        ///     Called when reload project dialog is cancelled.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="eventArgs"></param>
        protected virtual void OnReloadProjectsLoadingDialogCancelled(object o, DialogClosingEventArgs eventArgs)
        {
            if (_loadProjectsCancellationTokenSource == null)
                return;

            _loadProjectsCancellationTokenSource.Cancel();
            _loadProjectsCancellationTokenSource = null;
        }

        /// <summary>
        ///     Called when page is loaded.
        /// </summary>
        protected virtual async void OnPageLoadedCommand()
        {
            _messageBusService.PublishEvent(EventNameConstant.UpdateMainViewTitle,
                Resources.TITLE_PROJECT_MANAGEMENT);

            var reloadProjectsTask = ReloadProjectsAsync();
            LoadProjectsResult = await reloadProjectsTask;
        }

        /// <summary>
        ///     Called when add project operation is cancelled.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="eventArgs"></param>
        protected virtual void OnAddProjectOperationCancelled(object o, DialogClosingEventArgs eventArgs)
        {
            if (_addProjectCancellationTokenSource == null)
                return;

            if (_addProjectCancellationTokenSource.IsCancellationRequested)
                return;

            _addProjectCancellationTokenSource.Cancel();
        }

        /// <summary>
        ///     Called when edit project operation is cancelled.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="eventArgs"></param>
        protected virtual void OnEditProjectOperationCancelled(object o, DialogClosingEventArgs eventArgs)
        {
            if (_editProjectCancellationTokenSource == null)
                return;

            if (_editProjectCancellationTokenSource.IsCancellationRequested)
                return;

            _editProjectCancellationTokenSource.Cancel();
        }

        /// <summary>
        /// Raisd when add edit project skill is clicked.
        /// </summary>
        protected virtual async void OnAddEditProjectSkillClicked(SkilledProjectViewModel skilledProject)
        {
            // Display add edit project skills dialog.
            var view = new AddEditProjectSkillDialogVIew();
            var viewModel = new UpdateProjectSkillsDialogController(skilledProject);
            view.DataContext = viewModel;
            var addEditProjectSkillResult = await DialogHost.Show(view, ProjectManagementDialogHost);

            if (!(addEditProjectSkillResult is List<SkillViewModel> addingSkills))
                return;
            
            // Cancel previous task.
            if (_addSkillsIntoProjectCancellationTokenSource != null && !_addSkillsIntoProjectCancellationTokenSource.IsCancellationRequested)
            {
                _addSkillsIntoProjectCancellationTokenSource.Cancel();
                _addProjectCancellationTokenSource = null;
            }

            // Initialize new cancellation token.
            _addSkillsIntoProjectCancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(TimeoutConstant.DefaultTimeout));

            // Close current dialog host.
            IsPopupShown = false;

            // Display loading dialog.
            var loadingDialogView = new LoadingIndicatorDialogView();
            var loadingDialogController = new LoadingModalViewModel();
            loadingDialogController.IsCancelAvailable = true;
            loadingDialogView.DataContext = loadingDialogController;

            // Display loading dialog view task.
            var displayLoadingDialogTask = DialogHost.Show(loadingDialogView, ProjectManagementDialogHost, OnAddSkillsToProjectOperationCancelled);

            // Add skills into project task.
            var addSkillsIntoProjectViewModel = new AddSkillIntoProjectViewModel();
            addSkillsIntoProjectViewModel.ProjectId = skilledProject.Id;
            addSkillsIntoProjectViewModel.SkillIds = addingSkills.Select(x => x.Id).ToList();

            var addSkillsIntoProjectTask = _projectService.UpdateProjectSkillsAsync(addSkillsIntoProjectViewModel, _addSkillsIntoProjectCancellationTokenSource.Token);

            try
            {
                // Wait for any task to be completed.
                var completedTask = await Task.WhenAny(displayLoadingDialogTask, addSkillsIntoProjectTask);
                if (completedTask != addSkillsIntoProjectTask)
                    throw new TaskCanceledException();

                // Find the project that is being edited in the list.
                // This will prevent user from making an useless request to server to update project status.
                var addedSkills = addSkillsIntoProjectTask.Result;

                var availableProjects = new List<SkilledProjectViewModel>(Projects);

                foreach (var project in availableProjects)
                {
                    if (project.Id != skilledProject.Id)
                        continue;

                    project.Skills = addedSkills;
                }

                Projects = availableProjects;
            }
            catch (Exception exception)
            {
                if (!(exception is TaskCanceledException))
                    throw;
            }
            finally
            {
                IsPopupShown = false;
            }

        }

        /// <summary>
        /// Called when add skills into project operation is cancelled.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        protected virtual void OnAddSkillsToProjectOperationCancelled(object sender, DialogClosingEventArgs eventArgs)
        {
            if (_addSkillsIntoProjectCancellationTokenSource == null)
                return;

            if (_addSkillsIntoProjectCancellationTokenSource.IsCancellationRequested)
                return;

            _addSkillsIntoProjectCancellationTokenSource.Cancel();
            _addSkillsIntoProjectCancellationTokenSource = null;
        }

        #endregion
    }
}