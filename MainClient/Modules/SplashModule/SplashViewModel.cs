﻿using System;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Sodakoq.Main.Constants;
using Sodakoq.Main.Services.Interfaces;
using Sodakoq.Main.Views.Windows;

namespace Sodakoq.Main.Modules.SplashModule
{
    public class SplashViewModel : ViewModelBase
    {
        #region Constructor

        /// <summary>
        ///     Initialize splash view model.
        /// </summary>
        public SplashViewModel(IUserService userService,
            IMessageBusService messageBusService, IKeyValueCacheService localKeyValueCacheService)
        {
            // Service registration.
            _userService = userService;
            _messageBusService = messageBusService;
            _localKeyValueCacheService = localKeyValueCacheService;

            // Relay command registration.
            OnWindowLoadedRelayCommand = new RelayCommand<Window>(OnWindowLoaded);
        }

        #endregion

        #region Relay commands 

        /// <summary>
        ///     Relay command which is raised when window is loaded.
        /// </summary>
        public virtual RelayCommand<Window> OnWindowLoadedRelayCommand { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     Called when window is loaded.
        /// </summary>
        protected virtual void OnWindowLoaded(Window splashWindow)
        {
            Task.Factory.StartNew(() =>
            {
                // Number of tasks which have been loaded.
                var loadedTasks = 0;

                // Find user stored access token.
                var loadUserAccessTokenObservable = Observable.FromAsync(() => _userService.FindUserAccessTokenAsync());
                loadUserAccessTokenObservable.Finally(() => loadedTasks++);
                loadUserAccessTokenObservable
                    .Subscribe(accessToken =>
                        _localKeyValueCacheService.AddOrUpdateItem(DbKeyConstant.Authentication, accessToken));

                Observable
                    .Merge(loadUserAccessTokenObservable)
                    .Subscribe(accessToken =>
                    {
                        Application.Current.Dispatcher
                            .Invoke(() =>
                            {
                                var appWindow = new AppWindow();
                                Application.Current.MainWindow = appWindow;
                                splashWindow.Close();
                                appWindow.Show();
                            });
                    });
            });
        }

        #endregion

        #region Properties

        /// <summary>
        ///     How much percentage that loading has been done.
        /// </summary>
        private int _loadingProgress;

        /// <summary>
        ///     Loading progress.
        /// </summary>
        public int LoadingProgress
        {
            get => _loadingProgress;
            private set
            {
                _loadingProgress = value;
                RaisePropertyChanged(nameof(LoadingProgress));
            }
        }

        #endregion

        #region Services

        /// <summary>
        ///     Service to handle user operation.
        /// </summary>
        private readonly IUserService _userService;

        /// <summary>
        ///     Service which is shared across application.
        /// </summary>
        private readonly IMessageBusService _messageBusService;

        /// <summary>
        ///     Service which to handle in-memory key-value storage.
        /// </summary>
        private readonly IKeyValueCacheService _localKeyValueCacheService;

        #endregion
    }
}