﻿using System.Windows;

namespace Sodakoq.Main.Modules.SplashModule
{
    /// <summary>
    ///     Interaction logic for SplashWindow.xaml
    /// </summary>
    public partial class SplashWindow : Window
    {
        public SplashWindow()
        {
            InitializeComponent();
        }
    }
}