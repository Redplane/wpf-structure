﻿using System.Windows.Controls;

namespace Sodakoq.Main.Modules.LoginModule
{
    /// <summary>
    ///     Interaction logic for Login.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        public LoginPage()
        {
            InitializeComponent();
        }
    }
}