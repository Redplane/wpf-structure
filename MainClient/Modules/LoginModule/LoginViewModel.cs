using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.Constants;
using Sodakoq.Main.Controllers.Dialogs;
using Sodakoq.Main.DataTransferObjects.Users;
using Sodakoq.Main.Enumerations;
using Sodakoq.Main.Modules.CommonModule.CaptchaModalModule;
using Sodakoq.Main.Modules.CommonModule.LoadingModalModule;
using Sodakoq.Main.Services.Interfaces;
using Sodakoq.Main.Views;
using Sodakoq.Main.Views.Dialogs;
using WPFLocalizeExtension.Engine;

namespace Sodakoq.Main.Modules.LoginModule
{
    public class LoginViewModel : ViewModelBase
    {
        #region Constructor

        public LoginViewModel(IUserService userService,
            IValidationService validationService, IConnectivityService connectivityService,
            IMessageBusService messageBusService,
            ITranslateService translateService,
            IKeyValueCacheService localKeyValueCacheService)
        {
            // Model initialization.
            LoginModel = new BasicLoginViewModel();

#if DEBUG
            LoginModel = new BasicLoginViewModel("redplane_dt@yahoo.com.vn", "administrator", "12345");
#endif
            LoginSnackBarMessageQueue = new SnackbarMessageQueue();

            // Relay command registration.
            LoginRelayCommand = new RelayCommand<DependencyObject>(LoginCommand);
            OnPageLoadedRelayCommand = new RelayCommand(OnPageLoadedCommand);

            var availableLanguages = new ObservableCollection<KeyValuePair<string, string>>();
            availableLanguages.Add(new KeyValuePair<string, string>("TITLE_LANGUAGE_ENGLISH", "en"));
            availableLanguages.Add(new KeyValuePair<string, string>("TITLE_LANGUAGE_VIETNAMESE", "vi-VN"));

            AvailableLanguages = availableLanguages;
            SelectedLanguage = AvailableLanguages[0];

            // Service binding.
            _userService = userService;
            _validationService = validationService;
            _connectivityService = connectivityService;
            _translateService = translateService;
            _messageBusService = messageBusService;
            _localKeyValueCacheService = localKeyValueCacheService;
        }

        #endregion

        #region Events

        /// <summary>
        ///     Called when user presses cancel button to cancel login operation.
        /// </summary>
        protected virtual void OnLoginOperationCancelled(object sender, DialogClosingEventArgs eventArgs)
        {
            if (_loginCancellationTokenSource == null || _loginCancellationTokenSource.IsCancellationRequested)
                return;

            _loginCancellationTokenSource
                .Cancel(true);
        }

        /// <summary>
        ///     Called when profile loading operation is cancelled.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected virtual void OnProfileLoadingOperationCancelled(object sender, DialogClosingEventArgs args)
        {
            if (_loadUserProfileCancellationTokenSource == null ||
                _loadUserProfileCancellationTokenSource.IsCancellationRequested)
                return;

            _loadUserProfileCancellationTokenSource.Cancel();
            _loadUserProfileCancellationTokenSource = null;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Command which is fired when login is clicked.
        /// </summary>
        protected virtual async void LoginCommand(DependencyObject loginContainer)
        {
            // Check whether input parameters have any validation errors or not.
            if (!_validationService.IsControlValid(loginContainer))
            {
                AddMessageToSnackBar("MSG_VALIDATION_FAILED", "TITLE_DISMISS", null, true);
                return;
            }

            // Check whether user has internet connection or not.
            if (!_connectivityService.HasInternetConnection())
                return;

            // Display captcha modal
            var captchaDialogViewModel = new CaptchaModalViewModel();
            captchaDialogViewModel.CaptchaEndPoint = "https://www.google.com";

            var resolveCaptchaResult = await _userService
                .ResolveCaptchaAsync(LoginDialogHost, CaptchaKinds.Login);

            // Captcha has't been resolved or invalid captcha kind.
            if (!resolveCaptchaResult.IsSuccessful || resolveCaptchaResult.CaptchaKind != CaptchaKinds.Login)
            {
                AddMessageToSnackBar("MSG_INVALID_CAPTCHA_RESPONSE", "TITLE_DISMISS", null, true);
                return;
            }

            // Initialize loading indicator dialog.
            var view = new LoadingIndicatorDialogView();

            // Create cancellation token source.
            _loginCancellationTokenSource =
                new CancellationTokenSource(TimeSpan.FromSeconds(TimeoutConstant.DefaultTimeout));

            // Login result task.
            LoginModel.CaptchaCode = resolveCaptchaResult.CaptchaCode;
            var loginTask = _userService
                .BasicLoginAsync(LoginModel, _loginCancellationTokenSource.Token);

            // Login dialog result task.
            var displayLoginIndicatorTask = DialogHost.Show(view, LoginDialogHost, OnLoginOperationCancelled);

            try
            {
                //_appService.RedirectAppPage(new MainView());
                var completedTask = await Task.WhenAny(loginTask, displayLoginIndicatorTask);

                // User signed in successfully.
                if (completedTask != loginTask || completedTask.IsCanceled)
                    throw new TaskCanceledException();

                // Update login result to local database.
                var updateLoginResultToLocalDatabaseTask = _userService
                    .StoreLoginResultIntoDbAsync(loginTask.Result);

                // Update local cache item.
                _localKeyValueCacheService.AddOrUpdateItem(InMemoryKeyConstant.AccessToken,
                    loginTask.Result.AccessToken);

                // Load user profile.
                var loadUserProfileTask = _userService
                    .FindUserProfileAsync();

                // Run update login result & load user profile.
                await Task.WhenAll(updateLoginResultToLocalDatabaseTask, loadUserProfileTask);

                // Save profile information into in-memory cache.
                _localKeyValueCacheService.AddOrUpdateItem(InMemoryKeyConstant.Profile,
                    loadUserProfileTask.Result);

                // Publish profile update profile event.
                _messageBusService.PublishEvent(EventNameConstant.UpdateUserProfile,
                    loadUserProfileTask.Result);

                // Redirect user to main page.
                var dashboardView = new MainView();
                _messageBusService.PublishEvent(EventNameConstant.NavigateAppFrame, dashboardView);
            }
            catch (Exception exception)
            {
                // A task can be cancelled.
                if (exception is TaskCanceledException)
                    return;

                throw;
            }
            finally
            {
                // Close dialog programmatically.
                IsPopupShown = false;
            }
        }

        /// <summary>
        ///     Called when login page is loaded.
        /// </summary>
        protected virtual async void OnPageLoadedCommand()
        {
            // Display loading screen.
            var loadingDialogView = new LoadingIndicatorDialogView();
            var loadingDialogViewModel = new LoadingModalViewModel();
            loadingDialogViewModel.IsCancelAvailable = true;
            var displayLoadingMessageTask =
                DialogHost.Show(loadingDialogView, LoginDialogHost, OnProfileLoadingOperationCancelled);

            // Cancel previous operations.
            if (_loadUserProfileCancellationTokenSource != null &&
                !_loadUserProfileCancellationTokenSource.IsCancellationRequested)
                _loadUserProfileCancellationTokenSource.Cancel(false);

            // Initialize cancellation token source for profile loading operation.
            _loadUserProfileCancellationTokenSource =
                new CancellationTokenSource(TimeSpan.FromSeconds(TimeoutConstant.DefaultTimeout));

            // Get user profile.
            var loadUserProfileTask = _userService
                .FindUserProfileAsync(_loadUserProfileCancellationTokenSource.Token);

            try
            {
                var completedTask = await Task.WhenAny(displayLoadingMessageTask, loadUserProfileTask);
                if (completedTask != loadUserProfileTask || completedTask.IsCanceled)
                    throw new TaskCanceledException();

                // Update user profile to local cache.
                _messageBusService.PublishEvent(EventNameConstant.UpdateUserProfile,
                    loadUserProfileTask.Result);

                // Redirect window to main window.
                var dashboardView = new MainView();
                _messageBusService.PublishEvent(EventNameConstant.NavigateAppFrame, dashboardView);
            }
            catch (Exception exception)
            {
                if (exception is TaskCanceledException)
                    return;
            }
            finally
            {
                IsPopupShown = false;
            }
        }

        /// <summary>
        ///     Add message to snack bar.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="actionContent"></param>
        /// <param name="actionHandler"></param>
        /// <param name="bIsPromoted"></param>
        protected virtual void AddMessageToSnackBar(string message, string actionContent, Action actionHandler,
            bool bIsPromoted)
        {
            // Get translated message.
            var translatedMessage = _translateService
                .LoadTranslatedMessage(message);

            // Get translated action content.
            var translatedActionContent = _translateService
                .LoadTranslatedMessage(actionContent);

            var originalActionHandler = actionHandler;
            if (actionHandler == null)
                originalActionHandler = () => { };

            LoginSnackBarMessageQueue
                .Enqueue(translatedMessage ?? message, translatedActionContent ?? actionContent, originalActionHandler,
                    bIsPromoted);
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Language which is selected.
        /// </summary>
        private KeyValuePair<string, string> _selectedLanguage;

        /// <summary>
        ///     Language which is currently selected.
        /// </summary>
        public KeyValuePair<string, string> SelectedLanguage
        {
            get => _selectedLanguage;
            set
            {
                LocalizeDictionary.Instance.Culture = new CultureInfo(value.Value);
                Set(nameof(SelectedLanguage), ref _selectedLanguage, value);
            }
        }

        /// <summary>
        ///     Whether component is busy or not.
        /// </summary>
        private bool _bIsPopupShown;

        /// <summary>
        ///     Whether component is busy or not.
        /// </summary>
        public bool IsPopupShown
        {
            get => _bIsPopupShown;
            set
            {
                _bIsPopupShown = value;
                RaisePropertyChanged(nameof(IsPopupShown));
            }
        }

        /// <summary>
        ///     List of available languages in the application.
        /// </summary>
        public ObservableCollection<KeyValuePair<string, string>> AvailableLanguages { get; }

        /// <summary>
        ///     Model which is for logging user into system.
        /// </summary>
        public BasicLoginViewModel LoginModel { get; set; }

        /// <summary>
        ///     Snack bar message queue.
        /// </summary>
        public SnackbarMessageQueue LoginSnackBarMessageQueue { get; set; }

        /// <summary>
        ///     Login dialog host instance.
        /// </summary>
        public string LoginDialogHost => nameof(LoginDialogHost);

        #endregion

        #region Services

        // User service.
        private readonly IUserService _userService;

        /// <summary>
        ///     Service which is for controls validation.
        /// </summary>
        private readonly IValidationService _validationService;

        /// <summary>
        ///     Connectivity service.
        /// </summary>
        private readonly IConnectivityService _connectivityService;

        /// <summary>
        ///     Service for handling translation.
        /// </summary>
        private readonly ITranslateService _translateService;

        /// <summary>
        ///     Service to handle shared operations.
        /// </summary>
        private readonly IMessageBusService _messageBusService;

        /// <summary>
        ///     Service to store key-value in memory.
        /// </summary>
        private readonly IKeyValueCacheService _localKeyValueCacheService;

        #endregion

        #region Relay commands

        /// <summary>
        ///     Relay command which is raised when login is clicked.
        /// </summary>
        public RelayCommand<DependencyObject> LoginRelayCommand { get; }

        /// <summary>
        ///     Relay command which is raised when login page is being loadeded.
        /// </summary>
        public RelayCommand OnPageLoadedRelayCommand { get; }

        #endregion

        #region Cancellation token 

        /// <summary>
        ///     Source cancellation token to cancel login operation.
        /// </summary>
        private CancellationTokenSource _loginCancellationTokenSource;

        /// <summary>
        ///     Cancellation token source to load user profile.
        /// </summary>
        private CancellationTokenSource _loadUserProfileCancellationTokenSource;

        #endregion
    }
}