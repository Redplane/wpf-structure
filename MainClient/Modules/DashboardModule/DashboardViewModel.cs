﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using LiveCharts;
using LiveCharts.Wpf;
using SkiaSharp;
using Sodakoq.Main.Constants;
using Sodakoq.Main.DataTransferObjects;
using Sodakoq.Main.DataTransferObjects.Projects;
using Sodakoq.Main.DataTransferObjects.SkillCategories;
using Sodakoq.Main.DataTransferObjects.Skills;
using Sodakoq.Main.DataTransferObjects.Users;
using Sodakoq.Main.Models;
using Sodakoq.Main.Models.SkillCategories;
using Sodakoq.Main.Properties;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Modules.DashboardModule
{
    public class DashboardViewModel : ViewModelBase
    {
        #region Constructor

        public DashboardViewModel(
            IMessageBusService messageBusService,
            ISkillCategoryService skillCategoryService,
            ISkillService skillService, IProjectService projectService,
            IUserService userService,
            IFileService fileService,
            IKeyValueCacheService localKeyValueCacheService)
        {
            // Services binding.
            _skillCategoryService = skillCategoryService;
            _skillService = skillService;
            _projectService = projectService;
            _fileService = fileService;
            _userService = userService;
            _messageBusService = messageBusService;

            // Properties binding.
            _skillCategoriesSkillsSeries = new SeriesCollection();
            _profile = localKeyValueCacheService.GetItem<UserViewModel>(InMemoryKeyConstant.Profile);
            _loadCategorizedCategorizedSkillsCondition = new LoadCategorizedSkillModel();
            _loadCategorizedCategorizedSkillsCondition.UserId = _profile.Id;
            _loadCategorizedCategorizedSkillsCondition.Pagination =
                new PaginationViewModel(1, PaginationConstant.MaxSkillCategoriesInProfilePage);

            // Relay command binding.
            OnProfileLoadedRelayCommand = new RelayCommand(OnProfileLoadedCommand);
            ClickUploadPhotoRelayCommand = new RelayCommand(OnPhotoUploaderSelected);

            // Event binding.
            messageBusService.FindEventSubject(EventNameConstant.UpdateUserProfile)
                .Subscribe(OnProfileUpdated);
        }

        #endregion

        #region Events

        /// <summary>
        ///     Called when profile is updated.
        /// </summary>
        /// <param name="o"></param>
        protected virtual void OnProfileUpdated(object o)
        {
            if (!(o is UserViewModel user))
                return;

            Profile = user;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Called when profile page is loaded.
        /// </summary>
        protected virtual void OnProfileLoadedCommand()
        {
            // Mark skill category to be being loaded.
            IsLoadingSkillCategory = true;

            // Mark projects to be loaded.
            IsLoadingProjects = true;

            // Mark skills to be loaded.
            IsLoadingSkills = true;

            // Update main view title.
            _messageBusService.PublishEvent(EventNameConstant.UpdateMainViewTitle, Resources.TITLE_PROFILE);

            // Load categorized skills.
            var loadCategorizedSkillsTask = _skillCategoryService
                .LoadCategorizedSkillsAsync(_loadCategorizedCategorizedSkillsCondition);

            Observable
                .FromAsync(() => loadCategorizedSkillsTask)
                .Subscribe(OnCategorizedSkillsLoaded);

            var loadSkillsCondition = new LoadSkillViewModel();
            loadSkillsCondition.UserId = _profile.Id;
            loadSkillsCondition.Pagination = new PaginationViewModel(1, 0);

            var loadSkillsTask = _skillService
                .LoadUserSkillsAsync(loadSkillsCondition);

            Observable
                .FromAsync(() => loadSkillsTask)
                .Subscribe(OnSkillsLoaded);

            var loadProjectsCondition = new LoadProjectViewModel();
            loadProjectsCondition.UserId = _profile.Id;
            loadProjectsCondition.Pagination = new PaginationViewModel(1, 0);

            var loadProjectsTask = _projectService
                .LoadProjectsAsync(loadProjectsCondition);

            Observable
                .FromAsync(() => loadProjectsTask)
                .Subscribe(OnProjectsLoaded);
        }

        /// <summary>
        ///     Called when skill categories' skills have been loaded.
        /// </summary>
        /// <param name="loadSkillCategoriesSkillsResult"></param>
        protected virtual void OnCategorizedSkillsLoaded(
            SearchResult<List<SkillCategoryViewModel>> loadSkillCategoriesSkillsResult)
        {
            // Get list of skill categories.
            var skillCategories = loadSkillCategoriesSkillsResult.Records;

            // Skill categories' skills series collection.
            var skillCategoriesSkillsSeriesCollection = new SeriesCollection();

            Application
                .Current
                .Dispatcher.Invoke(() =>
                {
                    foreach (var skillCategory in skillCategories)
                    {
                        // Initialize pie series.
                        var skillCategoriesSkillsSeries = new PieSeries();

                        skillCategoriesSkillsSeries.Title = skillCategory.Name;
                        skillCategoriesSkillsSeries.DataLabels = true;
                        skillCategoriesSkillsSeries.Values = new ChartValues<int>();
                        skillCategoriesSkillsSeries.Values.Add(skillCategory.Skills.Count);

                        skillCategoriesSkillsSeriesCollection.Add(skillCategoriesSkillsSeries);
                    }

                    SkillCategoriesSkillChartSeries = skillCategoriesSkillsSeriesCollection;
                    SkillCategories = skillCategories;
                    IsLoadingSkillCategory = false;
                });
        }

        /// <summary>
        ///     Called when photo uploader is selected.
        /// </summary>
        protected virtual async void OnPhotoUploaderSelected()
        {
            try
            {
                // Mark the photo to be uploaded.
                IsUploadingPhoto = true;

                // Pick image file.
                var path = _fileService.SelectFile();
                if (string.IsNullOrEmpty(path))
                    return;

                // Proceed sk bitmap.
                var skBitmap = _fileService.ProceedProfilePhoto(path);
                var skImage = SKImage.FromPixels(skBitmap.PeekPixels());
                var skData = skImage.Encode(SKEncodedImageFormat.Png, 100);

                var photo = skData.ToArray();
                var user = await _userService
                    .UploadProfilePhotoAsync(null, photo);

                if (user == null)
                    return;

                // Publish message about profile update.
                _messageBusService.PublishEvent(EventNameConstant.UpdateUserProfile, user);

                // Send a snackbar message.
                var snackbarMessage =
                    new SnackbarMessageModel(Resources.MSG_PHOTO_UPLOADED_SUCCESSFULLY, Resources.TITLE_DISMISS);
                _messageBusService.PublishEvent(EventNameConstant.AddAppSnackbarMessage, snackbarMessage);
            }
            catch (Exception exception)
            {
                if (!(exception is BadImageFormatException))
                    throw;
            }
            finally
            {
                IsUploadingPhoto = false;
            }
        }

        /// <summary>
        ///     Called when on skills have been loaded.
        /// </summary>
        /// <param name="loadSkillsResult"></param>
        protected virtual void OnSkillsLoaded(SearchResult<List<SkillViewModel>> loadSkillsResult)
        {
            TotalSkills = loadSkillsResult.TotalRecords;
            IsLoadingSkills = false;
        }

        /// <summary>
        ///     Called when projects have been loaded.
        /// </summary>
        /// <param name="loadProjectResults"></param>
        protected virtual void OnProjectsLoaded(SearchResult<List<ProjectViewModel>> loadProjectResults)
        {
            TotalProject = loadProjectResults.TotalRecords;
            IsLoadingProjects = false;
        }

        #endregion

        #region Services

        /// <summary>
        ///     Skill category service.
        /// </summary>
        private readonly ISkillCategoryService _skillCategoryService;

        /// <summary>
        ///     Skill service.
        /// </summary>
        private readonly ISkillService _skillService;

        /// <summary>
        ///     Project service.
        /// </summary>
        private readonly IProjectService _projectService;

        /// <summary>
        ///     Service that handles user operation.
        /// </summary>
        private readonly IUserService _userService;

        /// <summary>
        ///     File service.
        /// </summary>
        private readonly IFileService _fileService;

        /// <summary>
        ///     Service to handles message transaction in the application.
        /// </summary>
        private readonly IMessageBusService _messageBusService;

        #endregion

        #region Relay commands

        /// <summary>
        ///     Command which is raised when profile page is loaded.
        /// </summary>
        public RelayCommand OnProfileLoadedRelayCommand { get; }

        /// <summary>
        ///     Command which is raised when upload photo button is clicked.
        /// </summary>
        public RelayCommand ClickUploadPhotoRelayCommand { get; }

        #endregion

        #region Properties

        /// <summary>
        ///     Skill categories skills series.
        /// </summary>
        private SeriesCollection _skillCategoriesSkillsSeries;

        /// <summary>
        ///     Skill categories skills series.
        /// </summary>
        public SeriesCollection SkillCategoriesSkillChartSeries
        {
            get => _skillCategoriesSkillsSeries;
            private set
            {
                _skillCategoriesSkillsSeries = value;
                RaisePropertyChanged(nameof(SkillCategoriesSkillChartSeries));
            }
        }

        /// <summary>
        ///     Condition to load categorized skills.
        /// </summary>
        private readonly LoadCategorizedSkillModel _loadCategorizedCategorizedSkillsCondition;

        /// <summary>
        ///     Whether skill category is being loaded.
        /// </summary>
        private bool _bIsLoadingSkillCategory;

        /// <summary>
        ///     Whether skill category is being loaded.
        /// </summary>
        public bool IsLoadingSkillCategory
        {
            get => _bIsLoadingSkillCategory;
            private set => Set(nameof(IsLoadingSkillCategory), ref _bIsLoadingSkillCategory, value);
        }

        /// <summary>
        ///     Whether projects are being loaded or not.
        /// </summary>
        private bool _bIsLoadingProjects;

        /// <summary>
        ///     Whether projects are being loaded or not.
        /// </summary>
        public bool IsLoadingProjects
        {
            get => _bIsLoadingProjects;
            set => Set(nameof(IsLoadingProjects), ref _bIsLoadingProjects, value);
        }

        /// <summary>
        ///     Whether skills are being loaded or not.
        /// </summary>
        private bool _bIsLoadingSkills;

        /// <summary>
        ///     Whether skills are being loaded or not.
        /// </summary>
        public bool IsLoadingSkills
        {
            get => _bIsLoadingSkills;
            set => Set(nameof(IsLoadingSkills), ref _bIsLoadingSkills, value);
        }

        /// <summary>
        ///     List of loaded skill categories.
        /// </summary>
        private List<SkillCategoryViewModel> _skillCategories;

        /// <summary>
        ///     List of skill categories.
        /// </summary>
        public List<SkillCategoryViewModel> SkillCategories
        {
            get => _skillCategories;
            private set => Set(nameof(SkillCategories), ref _skillCategories, value);
        }

        /// <summary>
        ///     Number of projects user has completed.
        /// </summary>
        private int _totalProjects;

        /// <summary>
        ///     Number of project user has completed
        /// </summary>
        public int TotalProject
        {
            get => _totalProjects;
            private set => Set(nameof(TotalProject), ref _totalProjects, value);
        }

        /// <summary>
        ///     Number of skills user has.
        /// </summary>
        private int _totalSkills;

        /// <summary>
        ///     Number of skills user has.
        /// </summary>
        public int TotalSkills
        {
            get => _totalSkills;
            private set => Set(nameof(TotalSkills), ref _totalSkills, value);
        }

        /// <summary>
        ///     Whether photo is being uploaded.
        /// </summary>
        private bool _bIsUploadingPhoto;

        /// <summary>
        ///     Whether photo is being uploaded
        /// </summary>
        public bool IsUploadingPhoto
        {
            get => _bIsUploadingPhoto;
            private set => Set(nameof(IsUploadingPhoto), ref _bIsUploadingPhoto, value);
        }

        /// <summary>
        ///     User profile in the application.
        /// </summary>
        private UserViewModel _profile;

        /// <summary>
        ///     User profile in application.
        /// </summary>
        public UserViewModel Profile
        {
            get => _profile;
            private set => Set(nameof(Profile), ref _profile, value);
        }

        #endregion
    }
}