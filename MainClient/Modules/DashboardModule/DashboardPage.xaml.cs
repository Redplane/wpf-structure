﻿using System.Windows.Controls;

namespace Sodakoq.Main.Modules.DashboardModule
{
    /// <summary>
    ///     Interaction logic for ProfileView.xaml
    /// </summary>
    public partial class DashboardPage : Page
    {
        public DashboardPage()
        {
            InitializeComponent();
        }
    }
}