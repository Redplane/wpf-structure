﻿using System.Diagnostics;
using System.Windows;
using CefSharp.Wpf;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.Enumerations;
using Sodakoq.Main.Models.CommandArguments;
using Sodakoq.Main.Models.JavascriptObjects;

namespace Sodakoq.Main.Modules.CommonModule.CaptchaModalModule
{
    public class CaptchaModalViewModel : ViewModelBase
    {
        #region Constructor

        public CaptchaModalViewModel()
        {
            OnChromiumBrowserInitializedRelayCommand =
                new RelayCommand<ChromiumWebBrowser>(OnChromiumBrowserInitialized);
            OnChromiumLoadingStateChangedRelayCommand =
                new RelayCommand<OnChromiumLoadingStateChangedCommandArgument>(OnChromiumLoadingStateChanged);

            ClickCloseCaptchaModalRelayCommand = new RelayCommand(OnCloseCaptchaModalClicked);
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Captcha end-point.
        /// </summary>
        public string CaptchaEndPoint { get; set; }

        /// <summary>
        ///     Command which is called when chromium browser is initialized.
        /// </summary>
        public RelayCommand<ChromiumWebBrowser> OnChromiumBrowserInitializedRelayCommand { get; set; }

        /// <summary>
        ///     Command which is called when chromium loading state changed.
        /// </summary>
        public RelayCommand<OnChromiumLoadingStateChangedCommandArgument> OnChromiumLoadingStateChangedRelayCommand
        {
            get;
            set;
        }

        /// <summary>
        ///     Command which is raised when close button on captcha modal is clicked.
        /// </summary>
        public RelayCommand ClickCloseCaptchaModalRelayCommand { get; set; }

        /// <summary>
        ///     Whether browser is loading or not.
        /// </summary>
        private bool _bIsBrowserLoading;

        public bool IsBrowserLoading
        {
            get => _bIsBrowserLoading;
            set
            {
                _bIsBrowserLoading = value;
                RaisePropertyChanged(nameof(IsBrowserLoading));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Called when chromium browser initialization is changed.
        /// </summary>
        /// <param name="chromiumWebBrowser"></param>
        protected virtual void OnChromiumBrowserInitialized(ChromiumWebBrowser chromiumWebBrowser)
        {
            var resolveCaptchaJavascriptObject = new CaptchaResolve();
            resolveCaptchaJavascriptObject
                .OnCaptchaResolved += ResolveCaptchaJavascriptObjectOnOnCaptchaResolved;

            chromiumWebBrowser
                .JavascriptObjectRepository
                .Register("loginCaptchaBoundObjectAsync", resolveCaptchaJavascriptObject, true);
        }

        /// <summary>
        ///     Called when chromium browser loading state is changed.
        /// </summary>
        /// <param name="loadingStateChangedEventArgs"></param>
        protected virtual void OnChromiumLoadingStateChanged(
            OnChromiumLoadingStateChangedCommandArgument loadingStateChangedEventArgs)
        {
            if (loadingStateChangedEventArgs == null)
                return;

            var eventArgs = loadingStateChangedEventArgs.EventArgs;
            if (eventArgs == null)
                return;

            IsBrowserLoading = eventArgs.IsLoading;
            Debug.WriteLine(IsBrowserLoading);
        }

        /// <summary>
        ///     Called when close button on captcha modal dialog is clicked.
        /// </summary>
        protected virtual void OnCloseCaptchaModalClicked()
        {
            var resolveCaptchaCommandArgument = new ResolveCaptchaResult();
            resolveCaptchaCommandArgument.CaptchaCode = null;
            resolveCaptchaCommandArgument.IsSuccessful = false;

            DialogHost
                .CloseDialogCommand
                .Execute(resolveCaptchaCommandArgument, null);
        }

        /// <summary>
        ///     Called when captcha is resolved.
        /// </summary>
        /// <param name="captchaCode"></param>
        /// <param name="captchaKind"></param>
        protected virtual void ResolveCaptchaJavascriptObjectOnOnCaptchaResolved(string captchaCode,
            CaptchaKinds captchaKind)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                var resolveCaptchaCommandArgument = new ResolveCaptchaResult();
                resolveCaptchaCommandArgument.CaptchaCode = captchaCode;
                resolveCaptchaCommandArgument.IsSuccessful = true;
                resolveCaptchaCommandArgument.CaptchaKind = captchaKind;

                // Close dialog with resolved captcha.
                DialogHost
                    .CloseDialogCommand
                    .Execute(resolveCaptchaCommandArgument, null);
            });
        }

        #endregion
    }
}