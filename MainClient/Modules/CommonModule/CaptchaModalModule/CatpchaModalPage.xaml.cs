﻿using System.Windows.Controls;

namespace Sodakoq.Main.Modules.CommonModule.CaptchaModalModule
{
    /// <summary>
    ///     Interaction logic for CaptchaDialogView.xaml
    /// </summary>
    public partial class CaptchaDialogView : UserControl
    {
        public CaptchaDialogView()
        {
            InitializeComponent();
        }
    }
}