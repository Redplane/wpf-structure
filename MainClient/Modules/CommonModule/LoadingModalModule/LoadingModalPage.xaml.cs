﻿using System.Windows.Controls;

namespace Sodakoq.Main.Modules.CommonModule.LoadingModalModule
{
    /// <summary>
    ///     Interaction logic for LoadingIndicatorDialogView.xaml
    /// </summary>
    public partial class LoadingIndicatorDialogView : UserControl
    {
        public LoadingIndicatorDialogView()
        {
            InitializeComponent();
        }
    }
}