﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MaterialDesignThemes.Wpf;
using Sodakoq.Main.Models.DialogResults;

namespace Sodakoq.Main.Modules.CommonModule.LoadingModalModule
{
    public class LoadingModalViewModel : ViewModelBase
    {
        #region Constructor

        public LoadingModalViewModel()
        {
            CancelModalDialogRelayCommand = new RelayCommand(OnModalDialogCancelled);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Called when modal dialog is cancelled.
        /// </summary>
        protected virtual void OnModalDialogCancelled()
        {
            var loadingIndicatorDialogResultModel = new LoadingModalResult<object>();
            loadingIndicatorDialogResultModel.IsCancelled = true;
            loadingIndicatorDialogResultModel.AdditionalInfo = null;
            DialogHost.CloseDialogCommand.Execute(loadingIndicatorDialogResultModel, null);
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Command which is for closing modal dialog.
        /// </summary>
        public RelayCommand CancelModalDialogRelayCommand { get; }

        /// <summary>
        ///     Whether cancel is available or not.
        /// </summary>
        private bool _bIsCancelAvailable = true;

        /// <summary>
        ///     Whether cancel is available or not.
        /// </summary>
        public bool IsCancelAvailable
        {
            get => _bIsCancelAvailable;
            set => Set(nameof(IsCancelAvailable), ref _bIsCancelAvailable, value);
        }

        #endregion
    }
}