﻿using System.Windows.Controls;

namespace Sodakoq.Main.Views.Dialogs
{
    /// <summary>
    ///     Interaction logic for DeleteProjectDialogView.xaml
    /// </summary>
    public partial class YesNoDialogView : UserControl
    {
        public YesNoDialogView()
        {
            InitializeComponent();
        }
    }
}