﻿using AutoMapper;
using Sodakoq.Main.DataTransferObjects.Projects;

namespace Sodakoq.Main.Models.AutoMapperProfiles
{
    public class AutoMapperMainProfile : Profile
    {
        public AutoMapperMainProfile()
        {
            Configure();
        }


        public void Configure()
        {
            CreateMap<ProjectViewModel, AddEditProjectViewModel>()
                .ForMember(s => s.Responsibilities, c => c.MapFrom(m => m.Responsibilities));
            CreateMap<ProjectViewModel, AddEditProjectViewModel>()
                .ForMember(s => s.Responsibilities, c => c.MapFrom(m => m.Responsibilities))
                .ReverseMap();
            CreateMap<ProjectViewModel, SkilledProjectViewModel>();
        }
    }
}