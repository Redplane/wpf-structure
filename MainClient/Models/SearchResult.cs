﻿namespace Sodakoq.Main.Models
{
    public class SearchResult<T>
    {
        #region Properties

        /// <summary>
        ///     List of records returned to client.
        /// </summary>
        public T Records { get; set; }

        /// <summary>
        ///     TotalRecords condition matched records.
        /// </summary>
        public int TotalRecords { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Default constructor.
        /// </summary>
        public SearchResult()
        {
        }

        /// <summary>
        ///     Constructor with injectors.
        /// </summary>
        /// <param name="records"></param>
        /// <param name="totalRecords"></param>
        public SearchResult(T records, int totalRecords)
        {
            Records = records;
            TotalRecords = totalRecords;
        }

        #endregion
    }
}