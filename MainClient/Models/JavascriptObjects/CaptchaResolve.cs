﻿using Sodakoq.Main.Enumerations;

namespace Sodakoq.Main.Models.JavascriptObjects
{
    public class CaptchaResolve
    {
        #region Methods

        /// <summary>
        ///     Resolve captcha from browser.
        /// </summary>
        public virtual void ResolveCaptcha(string captcha, CaptchaKinds captchaKind)
        {
            OnCaptchaResolved?.Invoke(captcha, captchaKind);
        }

        #endregion

        #region Constructor

        #endregion

        #region Properties

        /// <summary>
        ///     Instance which is for point to function to handle resolve captcha.
        /// </summary>
        /// <param name="captchaCode"></param>
        public delegate void OnCaptchaResolvedDelegate(string captchaCode, CaptchaKinds captchaKind);

        /// <summary>
        ///     Event which will be raised when captcha resolved.
        /// </summary>
        public virtual event OnCaptchaResolvedDelegate OnCaptchaResolved;

        #endregion
    }
}