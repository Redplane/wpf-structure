﻿namespace Sodakoq.Main.Models
{
    public class UserModel
    {
        #region Properties

        /// <summary>
        ///     Id of users.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Full name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        ///     User last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        ///     Birthday (unix time)
        /// </summary>
        public double Birthday { get; set; }

        /// <summary>
        ///     User role.
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        ///     Photo.
        /// </summary>
        public string Photo { get; set; }

        #endregion
    }
}