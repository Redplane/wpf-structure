﻿using System;

namespace Sodakoq.Main.Models
{
    public class SnackbarMessageModel
    {
        #region Properties

        /// <summary>
        ///     Message content.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        ///     Action button content.
        /// </summary>
        public string ActionContent { get; set; }

        /// <summary>
        ///     Action that will be executed when button is clicked.
        /// </summary>
        public Action<bool> ActionHandler { get; set; }

        /// <summary>
        ///     Whether message should be promoted or not.
        /// </summary>
        public bool IsPromoted { get; set; }

        /// <summary>
        ///     Whether message can be duplicated or not.
        /// </summary>
        public bool IsDuplicatable { get; set; }

        /// <summary>
        ///     Whether content should be translated or not.
        /// </summary>
        public bool ShouldContentTranslated { get; set; }

        #endregion

        #region Constructor

        public SnackbarMessageModel()
        {
            ShouldContentTranslated = true;
        }

        public SnackbarMessageModel(string content) : this()
        {
            Content = content;
            ActionContent = null;
            ActionHandler = null;
        }

        public SnackbarMessageModel(string content, string actionContent) : this(content)
        {
            ActionContent = actionContent;
        }

        public SnackbarMessageModel(string content, string actionContent, Action<bool> actionHandler) : this(content,
            actionContent)
        {
            ActionHandler = actionHandler;
        }


        public SnackbarMessageModel(string content, string actionContent, Action<bool> actionHandler, bool isPromoted) :
            this(content, actionContent, actionHandler)
        {
            IsPromoted = isPromoted;
        }

        public SnackbarMessageModel(string content, string actionContent, Action<bool> actionHandler, bool isPromoted,
            bool isDuplicatable) : this(content, actionContent, actionHandler, isPromoted)
        {
            IsDuplicatable = isDuplicatable;
        }

        #endregion
    }
}