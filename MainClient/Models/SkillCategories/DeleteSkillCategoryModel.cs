﻿using System;

namespace Sodakoq.Main.Models.SkillCategories
{
    public class DeleteSkillCategoryModel
    {
        #region Constructor

        #endregion

        #region Properties

        /// <summary>
        ///     Id of user whose skill category needs removing.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        ///     Id of skill category.
        /// </summary>
        public Guid SkillCategoryId { get; set; }

        /// <summary>
        ///     Skill category name.
        /// </summary>
        public string SkillCategoryName { get; set; }

        /// <summary>
        ///     Whether related skills should be deleted or not.
        /// </summary>
        public bool IsSkillDetachedFromUser { get; set; }

        /// <summary>
        ///     Whether skill category should be completely deleted or not.
        /// </summary>
        public bool IsSkillCategoryCompletelyDeleted { get; set; }

        #endregion
    }
}