﻿using System;

namespace Sodakoq.Main.Models.SkillCategories
{
    public class AddEditSkillCategoryModel
    {
        #region Properties

        /// <summary>
        ///     Id of skill category.
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        ///     Name of skill category.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Time when skill category created.
        /// </summary>
        public double? CreatedTime { get; set; }

        #endregion

        #region Constructor

        #endregion
    }
}