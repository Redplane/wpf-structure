﻿using System;
using System.Collections.Generic;
using Sodakoq.Main.DataTransferObjects;

namespace Sodakoq.Main.Models.SkillCategories
{
    public class LoadCategorizedSkillModel
    {
        #region Properties

        /// <summary>
        ///     List of skill category ids.
        /// </summary>
        public HashSet<Guid> SkillCategoryIds { get; set; }

        /// <summary>
        ///     User id that skill category belongs to.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        ///     Pagination information.
        /// </summary>
        public PaginationViewModel Pagination { get; set; }

        #endregion
    }
}