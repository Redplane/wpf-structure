﻿namespace Sodakoq.Main.Models
{
    public class OpenFileDialogModel
    {
        #region Properties

        public bool IsMultipleSelectionSupported { get; set; }

        public bool IsReadOnlyChecked { get; set; }

        public bool IsReadOnlyShown { get; set; }

        public bool IsExtensionAdded { get; set; }

        public bool IsFileExistenceChecked { get; set; } = true;

        public bool IsPathExistenceChecked { get; set; } = true;

        public string DefaultExtension { get; set; } = "*.*";

        public string InitialDirectory { get; set; } = null;

        public string Title { get; set; }

        public string Filter { get; set; } = null;

        #endregion
    }
}