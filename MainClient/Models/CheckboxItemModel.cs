﻿namespace Sodakoq.Main.Models
{
    public class CheckboxItemModel<TValue>
    {
        #region Properties

        /// <summary>
        ///     Name of checkbox items.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Value of checked item.
        /// </summary>
        public TValue Value { get; set; }

        /// <summary>
        ///     Whether item is selected or not.
        /// </summary>
        private bool _bIsSelected;

        /// <summary>
        ///     Whether item is selected or not.
        /// </summary>
        public bool IsSelected
        {
            get => _bIsSelected;
            set
            {
                if (value == _bIsSelected)
                    return;

                _bIsSelected = value;
                OnItemSelectionChanged?.Invoke(_bIsSelected, this);
            }
        }

        /// <summary>
        ///     Called when item selection is changed.
        /// </summary>
        public delegate void OnItemSelectionChangedEventHandler(bool bIsChecked, CheckboxItemModel<TValue> item);

        /// <summary>
        ///     Raised when item selection is changed.
        /// </summary>
        public OnItemSelectionChangedEventHandler OnItemSelectionChanged;

        #endregion

        #region Constructor

        public CheckboxItemModel()
        {
        }

        public CheckboxItemModel(string name, TValue value)
        {
            Name = name;
            Value = value;
        }

        public CheckboxItemModel(string name, TValue value, bool isSelected)
        {
            Name = name;
            Value = value;
            IsSelected = isSelected;
        }

        #endregion
    }
}