﻿using Sodakoq.Main.Enumerations;

namespace Sodakoq.Main.Models
{
    public class MainFrameNavigationModel
    {
        #region Properties

        public MainFramePages TargetedPage { get; set; }

        public object AdditionalInfo { get; set; }

        #endregion

        #region Constructor

        public MainFrameNavigationModel()
        {
        }

        public MainFrameNavigationModel(MainFramePages targetPage)
        {
            TargetedPage = targetPage;
        }

        #endregion
    }
}