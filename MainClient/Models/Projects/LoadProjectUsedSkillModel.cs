﻿using System;
using System.Collections.Generic;
using Sodakoq.Main.DataTransferObjects;

namespace Sodakoq.Main.Models.Projects
{
    public class LoadProjectUsedSkillModel
    {
        #region Properties

        /// <summary>
        ///     Ids of project.
        /// </summary>
        public List<Guid> ProjectIds { get; set; }

        /// <summary>
        ///     Project owner id.
        /// </summary>
        public Guid? UserId { get; set; }

        /// <summary>
        ///     Pagination information.
        /// </summary>
        public PaginationViewModel Pagination { get; set; }

        #endregion
    }
}