﻿using System.Windows;
using System.Windows.Controls;

namespace Sodakoq.Main.Models.CommandArguments
{
    public class RedirectToUriCommandArgument : DependencyObject
    {
        #region Properties

        /// <summary>
        ///     Navigation service dependency property.
        /// </summary>
        public static readonly DependencyProperty PageDependencyProperty =
            DependencyProperty.Register(nameof(Container), typeof(Page), typeof(RedirectToUriCommandArgument),
                new PropertyMetadata(null));

        /// <summary>
        ///     Navigation service accessor.
        /// </summary>
        public Page Container
        {
            get => (Page) GetValue(PageDependencyProperty);
            set => SetValue(PageDependencyProperty, value);
        }


        /// <summary>
        ///     Uri dependency property.
        /// </summary>
        public static readonly DependencyProperty UriProperty =
            DependencyProperty.Register(nameof(Uri), typeof(string), typeof(RedirectToUriCommandArgument),
                new PropertyMetadata(null));

        /// <summary>
        ///     Uri property.
        /// </summary>
        public string Uri
        {
            get => (string) GetValue(UriProperty);
            set => SetValue(UriProperty, value);
        }

        #endregion
    }
}