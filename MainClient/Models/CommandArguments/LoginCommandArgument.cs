﻿namespace Sodakoq.Main.Models.CommandArguments
{
    public class LoginCommandArgument
    {
        #region Properties

        public string Username { get; set; }

        public string Password { get; set; }

        #endregion
    }
}