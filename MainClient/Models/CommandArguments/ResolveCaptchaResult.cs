﻿using Sodakoq.Main.Enumerations;

namespace Sodakoq.Main.Models.CommandArguments
{
    public class ResolveCaptchaResult
    {
        #region Properties

        /// <summary>
        ///     Captcha code.
        /// </summary>
        public string CaptchaCode { get; set; }

        /// <summary>
        ///     Whether resolve is successful or not.
        /// </summary>
        public bool IsSuccessful { get; set; }

        /// <summary>
        ///     Kinda captcha.
        /// </summary>
        public CaptchaKinds CaptchaKind { get; set; }

        #endregion

        #region Constructor

        public ResolveCaptchaResult()
        {
            IsSuccessful = false;
            CaptchaKind = CaptchaKinds.None;
        }

        public ResolveCaptchaResult(string captchaCode, bool isSuccessful)
        {
            CaptchaCode = captchaCode;
            IsSuccessful = isSuccessful;
            CaptchaKind = CaptchaKinds.None;
        }

        public ResolveCaptchaResult(string captchaCode, bool isSuccessful, CaptchaKinds captchaKind)
        {
            CaptchaCode = captchaCode;
            IsSuccessful = isSuccessful;
            CaptchaKind = captchaKind;
        }

        #endregion

        #region Methods

        #endregion
    }
}