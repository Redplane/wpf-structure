﻿using CefSharp;
using CefSharp.Wpf;

namespace Sodakoq.Main.Models.CommandArguments
{
    public class OnChromiumLoadingStateChangedCommandArgument
    {
        #region Properties

        /// <summary>
        ///     Instance which invoke the event.
        /// </summary>
        public ChromiumWebBrowser Sender { get; set; }

        /// <summary>
        ///     Chromium state changed event arguments.
        /// </summary>
        public LoadingStateChangedEventArgs EventArgs { get; set; }

        #endregion

        #region Constructor

        public OnChromiumLoadingStateChangedCommandArgument()
        {
        }

        public OnChromiumLoadingStateChangedCommandArgument(ChromiumWebBrowser sender,
            LoadingStateChangedEventArgs eventArgs)
        {
            Sender = sender;
            EventArgs = eventArgs;
        }

        #endregion
    }
}