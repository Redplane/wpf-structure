﻿using System.Runtime.InteropServices;
using System.Windows;

namespace Sodakoq.Main.Models
{
    [ComVisible(true)]
    public class LoginScriptModel
    {
        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }
    }
}