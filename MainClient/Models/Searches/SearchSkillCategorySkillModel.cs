﻿using System.Collections.Generic;
using Sodakoq.Main.DataTransferObjects;

namespace Sodakoq.Main.Models.Searches
{
    public class SearchSkillCategorySkillModel
    {
        #region Properties

        /// <summary>
        ///     Indexes of skill category.
        /// </summary>
        public HashSet<int> SkillCategoryIds { get; set; }

        /// <summary>
        ///     Indexes of skill.
        /// </summary>
        public HashSet<int> SkillIds { get; set; }

        /// <summary>
        ///     Point of skill category.
        /// </summary>
        public RangeViewModel<int?, int?> Point { get; set; }

        /// <summary>
        ///     Time when the skill category was created.
        /// </summary>
        public RangeViewModel<double?, double?> CreatedTime { get; set; }

        /// <summary>
        ///     Pagination information.
        /// </summary>
        public PaginationViewModel Pagination { get; set; }

        #endregion
    }
}