﻿using System.Collections.Generic;
using Sodakoq.Main.DataTransferObjects;

namespace Sodakoq.Main.Models.Searches
{
    public class SearchResponsibilityModel
    {
        #region Properties

        /// <summary>
        ///     Responsibility indexes.
        /// </summary>
        public HashSet<int> Ids { get; set; }

        /// <summary>
        ///     List of responsibility names.
        /// </summary>
        public HashSet<string> Names { get; set; }

        /// <summary>
        ///     Created time range.
        /// </summary>
        public RangeViewModel<double?, double?> Created { get; set; }

        /// <summary>
        ///     Time when responsibility was modified.
        /// </summary>
        public RangeViewModel<double?, double?> LastModified { get; set; }

        /// <summary>
        ///     Pagination information.
        /// </summary>
        public PaginationViewModel Pagination { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize search model with injectors.
        /// </summary>
        public SearchResponsibilityModel()
        {
        }

        /// <summary>
        ///     Initialize search model with injectors.
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="names"></param>
        /// <param name="created"></param>
        /// <param name="lastModified"></param>
        /// <param name="pagination"></param>
        public SearchResponsibilityModel(HashSet<int> ids, HashSet<string> names = null,
            RangeViewModel<double?, double?> created = null, RangeViewModel<double?, double?> lastModified = null,
            PaginationViewModel pagination = null)
        {
            Ids = ids;
            Names = names;
            Created = created;
            LastModified = lastModified;
            Pagination = pagination;
        }

        #endregion
    }
}