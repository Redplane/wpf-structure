﻿using System;
using Sodakoq.Main.DataTransferObjects;

namespace Sodakoq.Main.Models.Searches
{
    public class LoadUserSkillCategoryModel
    {
        #region Properties

        /// <summary>
        ///     Name of skill category.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     User id.
        /// </summary>
        public Guid? UserId { get; set; }

        /// <summary>
        ///     Pagination information.
        /// </summary>
        public PaginationViewModel Pagination { get; set; }

        #endregion
    }
}