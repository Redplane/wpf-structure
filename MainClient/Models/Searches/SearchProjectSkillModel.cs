﻿using System.Collections.Generic;
using Sodakoq.Main.DataTransferObjects;

namespace Sodakoq.Main.Models.Searches
{
    public class SearchProjectSkillModel
    {
        #region Properties

        /// <summary>
        ///     List of project indexes.
        /// </summary>
        public HashSet<int> ProjectIds { get; set; }

        /// <summary>
        ///     List of skill indexes.
        /// </summary>
        public HashSet<int> SkillIds { get; set; }

        /// <summary>
        ///     List of pagination.
        /// </summary>
        public PaginationViewModel Pagination { get; set; }

        #endregion

        #region Costructors

        /// <summary>
        ///     Initialize model with no setting.
        /// </summary>
        public SearchProjectSkillModel()
        {
        }

        /// <summary>
        ///     Initialize model with settings.
        /// </summary>
        /// <param name="projectIds"></param>
        /// <param name="skillIds"></param>
        /// <param name="pagination"></param>
        public SearchProjectSkillModel(HashSet<int> projectIds, HashSet<int> skillIds, PaginationViewModel pagination)
        {
            ProjectIds = projectIds;
            SkillIds = skillIds;
            Pagination = pagination;
        }

        #endregion
    }
}