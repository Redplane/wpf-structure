﻿using System.Collections.Generic;
using Sodakoq.Main.DataTransferObjects;

namespace Sodakoq.Main.Models.Searches
{
    public class SearchUserDescriptionModel
    {
        #region Properties

        /// <summary>
        ///     User description indexes.
        /// </summary>
        public HashSet<int> Ids { get; set; }

        /// <summary>
        ///     User indexes.
        /// </summary>
        public HashSet<int> UserIds { get; set; }

        /// <summary>
        ///     Pagination information.
        /// </summary>
        public PaginationViewModel Pagination { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize search condition with no setting.
        /// </summary>
        public SearchUserDescriptionModel()
        {
        }

        /// <summary>
        ///     Initialize search condition with settings.
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="userIds"></param>
        /// <param name="pagination"></param>
        public SearchUserDescriptionModel(HashSet<int> ids, HashSet<int> userIds = null,
            PaginationViewModel pagination = null)
        {
            Ids = ids;
            UserIds = userIds;
            Pagination = pagination;
        }

        #endregion
    }
}