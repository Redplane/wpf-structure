﻿using System.Collections.Generic;
using Sodakoq.Main.DataTransferObjects;

namespace Sodakoq.Main.Models.Searches
{
    public class SearchProjectModel
    {
        #region Properties

        /// <summary>
        ///     Index of projects.
        /// </summary>
        public HashSet<int> Ids { get; set; }

        /// <summary>
        ///     User indexes.
        /// </summary>
        public HashSet<int> UserIds { get; set; }

        /// <summary>
        ///     Project names.
        /// </summary>
        public HashSet<string> Names { get; set; }

        /// <summary>
        ///     Project started time.
        /// </summary>
        public RangeViewModel<double?, double?> Started { get; set; }

        /// <summary>
        ///     Project finished time.
        /// </summary>
        public RangeViewModel<double?, double?> Finished { get; set; }

        /// <summary>
        ///     Pagination.
        /// </summary>
        public PaginationViewModel Pagination { get; set; }

        #endregion
    }
}