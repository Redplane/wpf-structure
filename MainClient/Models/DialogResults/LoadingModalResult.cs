﻿namespace Sodakoq.Main.Models.DialogResults
{
    public class LoadingModalResult<T>
    {
        #region Properties

        /// <summary>
        ///     Whether loading indicator dialog is cancelled or not.
        /// </summary>
        public bool IsCancelled { get; set; }

        public T AdditionalInfo { get; set; }

        #endregion

        #region Constructor

        public LoadingModalResult()
        {
            AdditionalInfo = default(T);
        }

        public LoadingModalResult(bool isCancelled, T additionalInfo)
        {
            IsCancelled = isCancelled;
            AdditionalInfo = additionalInfo;
        }

        #endregion
    }
}