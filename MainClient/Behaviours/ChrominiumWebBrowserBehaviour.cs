﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;
using CefSharp;
using CefSharp.Wpf;
using Sodakoq.Main.Models.CommandArguments;

namespace Sodakoq.Main.Behaviours
{
    public class ChrominiumWebBrowserBehaviour : Behavior<ChromiumWebBrowser>
    {
        #region Constructor

        #endregion

        #region Properties

        /// <summary>
        ///     Command which is raised when browser initialization is changed.
        /// </summary>
        public ICommand IsBrowserInitializedChangedCommand
        {
            get => (ICommand) GetValue(IsBrowserInitializedChangedCommandProperty);
            set => SetValue(IsBrowserInitializedChangedCommandProperty, value);
        }

        /// <summary>
        ///     Called when loading state changed command.
        /// </summary>
        public ICommand LoadingStateChangedCommand
        {
            get => Dispatcher.Invoke(() => (ICommand) GetValue(LoadingStateChangedCommandProperty));
            set => Dispatcher.Invoke(() => SetValue(LoadingStateChangedCommandProperty, value));
        }

        /// <summary>
        ///     Command which is used when Chromium browser initialization is changed.
        /// </summary>
        protected static readonly DependencyProperty IsBrowserInitializedChangedCommandProperty =
            DependencyProperty.Register(nameof(IsBrowserInitializedChangedCommand), typeof(ICommand),
                typeof(ChrominiumWebBrowserBehaviour), new PropertyMetadata(null));

        /// <summary>
        ///     Command which is used when Chromium browser loading state is changed.
        /// </summary>
        protected static readonly DependencyProperty LoadingStateChangedCommandProperty =
            DependencyProperty.Register(nameof(LoadingStateChangedCommand), typeof(ICommand),
                typeof(ChrominiumWebBrowserBehaviour), new PropertyMetadata(null));

        #endregion

        #region Methods

        /// <summary>
        ///     Called when behaviour is attached to the target.
        /// </summary>
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.IsBrowserInitializedChanged += OnIsBrowserInitializedChanged;
            AssociatedObject.LoadingStateChanged += OnLoadingStateChanged;
        }

        /// <summary>
        ///     Called when IsInitializedChanged event is called.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="dependencyPropertyChangedEventArgs"></param>
        protected virtual void OnIsBrowserInitializedChanged(object sender,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            IsBrowserInitializedChangedCommand?.Execute(sender);
        }

        /// <summary>
        ///     Called when loading state is changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="loadingStateChangedEventArgs"></param>
        protected void OnLoadingStateChanged(object sender, LoadingStateChangedEventArgs loadingStateChangedEventArgs)
        {
            // Cast the sender to Chromium web browser instance.
            var webChromiumWebBrowser = sender as ChromiumWebBrowser;

            var loadStateChangedEventCommandArgument = new OnChromiumLoadingStateChangedCommandArgument();
            loadStateChangedEventCommandArgument.Sender = webChromiumWebBrowser;
            loadStateChangedEventCommandArgument.EventArgs = loadingStateChangedEventArgs;

            LoadingStateChangedCommand?.Execute(loadStateChangedEventCommandArgument);
        }

        #endregion
    }
}