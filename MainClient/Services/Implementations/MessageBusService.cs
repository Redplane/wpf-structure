﻿using System.Collections.Concurrent;
using System.Reactive.Subjects;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Services.Implementations
{
    public class MessageBusService : IMessageBusService
    {
        #region Properties

        /// <summary>
        ///     Message channel.
        /// </summary>
        private readonly ConcurrentDictionary<string, ISubject<object>> _messageChannels;

        #endregion

        #region Constructor

        public MessageBusService()
        {
            // Initialize message channel.
            _messageChannels = new ConcurrentDictionary<string, ISubject<object>>();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public virtual void PublishEvent(string eventName, object data)
        {
            if (!_messageChannels.TryGetValue(eventName, out var messageChannel))
            {
                messageChannel = new BehaviorSubject<object>(data);
                _messageChannels.TryAdd(eventName, messageChannel);
            }

            messageChannel
                .OnNext(data);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="eventName"></param>
        /// <returns></returns>
        public virtual ISubject<object> FindEventSubject(string eventName)
        {
            if (!_messageChannels.TryGetValue(eventName, out var messageChannel))
            {
                messageChannel = new BehaviorSubject<object>(null);
                _messageChannels.TryAdd(eventName, messageChannel);
            }

            return messageChannel;
        }

        #endregion
    }
}