﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Services.Implementations
{
    public class ValidationService : IValidationService
    {
        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="dependencyObject"></param>
        /// <returns></returns>
        public virtual bool IsControlValid(DependencyObject dependencyObject)
        {
            // Validate all the bindings on the parent
            var valid = true;
            var localValues = dependencyObject.GetLocalValueEnumerator();
            while (localValues.MoveNext())
            {
                var entry = localValues.Current;
                if (!BindingOperations.IsDataBound(dependencyObject, entry.Property))
                    continue;

                var binding = BindingOperations.GetBinding(dependencyObject, entry.Property);
                if (binding == null)
                    continue;

                foreach (var rule in binding.ValidationRules)
                {
                    var result = rule.Validate(dependencyObject.GetValue(entry.Property), null);
                    if (result.IsValid)
                        continue;

                    var expression = BindingOperations.GetBindingExpression(dependencyObject, entry.Property);
                    if (expression == null)
                        continue;

                    Validation.MarkInvalid(expression,
                        new ValidationError(rule, expression, result.ErrorContent, null));
                    valid = false;
                }
            }

            // Validate all the bindings on the children
            for (var i = 0; i != VisualTreeHelper.GetChildrenCount(dependencyObject); ++i)
            {
                var child = VisualTreeHelper.GetChild(dependencyObject, i);
                if (!IsControlValid(child)) valid = false;
            }

            return valid;
        }
    }
}