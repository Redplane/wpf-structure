﻿using System.Collections.Concurrent;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Services.Implementations
{
    public class KeyValueCacheService : IKeyValueCacheService
    {
        #region Properties

        /// <summary>
        ///     List of cached items.
        /// </summary>
        private readonly ConcurrentDictionary<string, object> _items;

        #endregion

        #region Constructor

        public KeyValueCacheService()
        {
            _items = new ConcurrentDictionary<string, object>();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public virtual void AddOrUpdateItem(string key, object value)
        {
            _items.AddOrUpdate(key, value, (s, o) => o = value);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual T GetItem<T>(string key)
        {
            // Get raw item.
            if (!_items.TryGetValue(key, out var item))
                return default(T);

            if (item == null)
                return default(T);

            if (item.GetType() != typeof(T))
                return default(T);

            var typedItem = (T) item;
            return typedItem;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="key"></param>
        public void RemoveItem(string key)
        {
            _items.TryRemove(key, out var _);
        }

        #endregion
    }
}