﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Sodakoq.Main.DataTransferObjects.Projects;
using Sodakoq.Main.DataTransferObjects.Skills;
using Sodakoq.Main.Models;
using Sodakoq.Main.Models.Projects;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Services.Implementations
{
    public class ProjectService : IProjectService
    {
        #region Properties

        /// <summary>
        ///     Http client service.
        /// </summary>
        private readonly HttpClient _httpClient;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize service with injectors.
        /// </summary>
        public ProjectService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="conditions"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<SearchResult<List<ProjectViewModel>>> LoadProjectsAsync(LoadProjectViewModel conditions,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Serialize search condition to json content.
            var json = JsonConvert.SerializeObject(conditions);
            var httpResponseMessage = await _httpClient.PostAsync(
                new Uri("api/project/search", UriKind.Relative),
                new StringContent(json, Encoding.UTF8, "application/json"),
                cancellationToken);

            var httpContent = httpResponseMessage.Content;
            if (httpContent == null)
                return null;

            var content = await httpContent.ReadAsStringAsync();
            if (string.IsNullOrEmpty(content))
                return null;

            return JsonConvert.DeserializeObject<SearchResult<List<ProjectViewModel>>>(content);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<SearchResult<List<SkilledProjectViewModel>>> LoadSkilledProjectsAsync(
            LoadProjectUsedSkillModel condition,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Serialize search condition to json content.
            var json = JsonConvert.SerializeObject(condition);
            var httpResponseMessage = await _httpClient.PostAsync(
                new Uri("api/project/load-used-skills", UriKind.Relative),
                new StringContent(json, Encoding.UTF8, "application/json"),
                cancellationToken);

            var httpContent = httpResponseMessage.Content;
            if (httpContent == null)
                return null;

            var content = await httpContent.ReadAsStringAsync();
            if (string.IsNullOrEmpty(content))
                return null;

            return JsonConvert.DeserializeObject<SearchResult<List<SkilledProjectViewModel>>>(content);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<bool> DeleteProjectAsync(Guid id,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var httpResponseMessage = await _httpClient.DeleteAsync(
                new Uri($"api/project/{id}", UriKind.Relative),
                cancellationToken);

            var httpContent = httpResponseMessage.Content;
            if (httpContent == null)
                return false;

            var content = await httpContent.ReadAsStringAsync();
            if (string.IsNullOrEmpty(content))
                return false;

            return true;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<ProjectViewModel> AddProjectAsync(AddProjectViewModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Serialize model.
            var serializedModel = JsonConvert.SerializeObject(model);

            var httpResponseMessage = await _httpClient
                .PostAsync(new Uri("api/project", UriKind.Relative),
                    new StringContent(serializedModel, Encoding.UTF8, "application/json"),
                    cancellationToken);

            // Get response content.
            var httpContent = httpResponseMessage.Content;
            if (httpContent == null)
                throw new Exception("Failed to add project");

            var content = await httpContent.ReadAsStringAsync();
            if (string.IsNullOrEmpty(content))
                throw new Exception("Failed to add project");

            return JsonConvert.DeserializeObject<ProjectViewModel>(content);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<List<SkillViewModel>> UpdateProjectSkillsAsync(AddSkillIntoProjectViewModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Serialize model.
            var serializedModel = JsonConvert.SerializeObject(model);

            var httpResponseMessage = await _httpClient
                .PostAsync(new Uri("api/project/skills", UriKind.Relative),
                    new StringContent(serializedModel, Encoding.UTF8, "application/json"),
                    cancellationToken);

            // Get response content.
            var httpContent = httpResponseMessage.Content;
            if (httpContent == null)
                throw new Exception("Failed to add skills into the project");

            var content = await httpContent.ReadAsStringAsync();
            if (string.IsNullOrEmpty(content))
                throw new Exception("Failed to add skills into the project project");

            return JsonConvert.DeserializeObject<List<SkillViewModel>>(content);
        }


        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public virtual async Task<ProjectViewModel> EditProjectAsync(Guid projectId, EditProjectViewModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Serialize model.
            var serializedModel = JsonConvert.SerializeObject(model);

            var httpResponseMessage = await _httpClient
                .PutAsync(new Uri($"api/project/{projectId}", UriKind.Relative),
                    new StringContent(serializedModel, Encoding.UTF8, "application/json"),
                    cancellationToken);

            var httpContent = httpResponseMessage.Content;
            if (httpContent == null)
                throw new Exception("Failed to edit project");

            var content = await httpContent.ReadAsStringAsync();
            if (string.IsNullOrEmpty(content))
                throw new Exception("Failed to edit project");

            return JsonConvert.DeserializeObject<ProjectViewModel>(content);
        }

        #endregion
    }
}