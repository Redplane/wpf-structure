﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Sodakoq.Main.DataTransferObjects.SkillCategories;
using Sodakoq.Main.Models;
using Sodakoq.Main.Models.Searches;
using Sodakoq.Main.Models.SkillCategories;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Services.Implementations
{
    public class SkillCategoryService : ISkillCategoryService
    {
        #region Properties

        /// <summary>
        ///     Instance for sending http requests.
        /// </summary>
        private readonly HttpClient _httpClient;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize service with injectors.
        /// </summary>
        public SkillCategoryService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<SearchResult<List<SkillCategoryViewModel>>> LoadCategorizedSkillsAsync(
            LoadCategorizedSkillModel condition,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var httpResponseMessage = await _httpClient.PostAsync(
                new Uri("api/skill-category/load-categorized-skills", UriKind.Relative),
                new StringContent(JsonConvert.SerializeObject(condition), Encoding.UTF8, "application/json"),
                cancellationToken);

            if (!httpResponseMessage.IsSuccessStatusCode)
                throw new Exception("Failed to get skill categories' skills list.");

            var httpContent = await httpResponseMessage.Content.ReadAsStringAsync();
            var loadSkillCategoriesSkillsResult =
                JsonConvert.DeserializeObject<SearchResult<List<SkillCategoryViewModel>>>(httpContent);
            return loadSkillCategoriesSkillsResult;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<SearchResult<List<SkillCategoryViewModel>>> LoadUserSkillCategoriesAsync(
            LoadUserSkillCategoryModel condition,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var httpResponseMessage = await _httpClient.PostAsync(
                new Uri("api/skill-category/search", UriKind.Relative),
                new StringContent(JsonConvert.SerializeObject(condition), Encoding.UTF8, "application/json"),
                cancellationToken);

            if (!httpResponseMessage.IsSuccessStatusCode)
                return new SearchResult<List<SkillCategoryViewModel>>();

            var httpContent = await httpResponseMessage.Content.ReadAsStringAsync();
            var loadUserSkillCategoriesResult =
                JsonConvert.DeserializeObject<SearchResult<List<SkillCategoryViewModel>>>(httpContent);
            return loadUserSkillCategoriesResult;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<SkillCategoryViewModel> AddSkillCategoryAsync(AddEditSkillCategoryModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var httpResponseMessage = await _httpClient.PostAsync(
                new Uri("api/skill-category", UriKind.Relative),
                new StringContent(JsonConvert.SerializeObject(model)), cancellationToken);

            if (!httpResponseMessage.IsSuccessStatusCode)
                throw new Exception("Failed to add skill category");

            var httpContent = await httpResponseMessage.Content.ReadAsStringAsync();
            var skillCategory =
                JsonConvert.DeserializeObject<SkillCategoryViewModel>(httpContent);
            return skillCategory;
        }

        /// <summary>
        ///     Edit skill category asynchronously.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<SkillCategoryViewModel> EditSkillCategoryAsync(AddEditSkillCategoryModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var httpResponseMessage = await _httpClient.PutAsync(
                new Uri($"api/skill-category/{model.Id}", UriKind.Relative),
                new StringContent(JsonConvert.SerializeObject(model)), cancellationToken);

            if (!httpResponseMessage.IsSuccessStatusCode)
                throw new Exception("Failed to edit skill category");

            var httpContent = await httpResponseMessage.Content.ReadAsStringAsync();
            var skillCategory =
                JsonConvert.DeserializeObject<SkillCategoryViewModel>(httpContent);
            return skillCategory;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="skillCategoryId"></param>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<SkillCategoryViewModel> DeleteSkillCategoryAsync(Guid skillCategoryId,
            DeleteSkillCategoryModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var httpRequestMessage = new HttpRequestMessage();
            httpRequestMessage.Content = new StringContent(JsonConvert.SerializeObject(model));
            httpRequestMessage.Method = HttpMethod.Delete;
            httpRequestMessage.RequestUri = new Uri($"api/skill-category/{skillCategoryId}", UriKind.Relative);

            // Send request and get response.
            var httpResponseMessage = await _httpClient.SendAsync(httpRequestMessage, cancellationToken);
            if (!httpResponseMessage.IsSuccessStatusCode)
                throw new Exception("Failed to edit skill category");

            var httpContent = await httpResponseMessage.Content.ReadAsStringAsync();
            var skillCategory =
                JsonConvert.DeserializeObject<SkillCategoryViewModel>(httpContent);

            return skillCategory;
        }

        #endregion
    }
}