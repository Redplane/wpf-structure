﻿using System;
using System.Windows;
using Microsoft.Win32;
using SkiaSharp;
using Sodakoq.Main.Constants;
using Sodakoq.Main.Exceptions;
using Sodakoq.Main.Models;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Services.Implementations
{
    public class FileService : IFileService
    {
        #region Constructor

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="container"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public virtual string SelectFile(Window container = null,
            OpenFileDialogModel options = default(OpenFileDialogModel))
        {
            var dialog = BuildOpenFileDialog(options);
            if (container == null)
                dialog.ShowDialog();
            else
                dialog.ShowDialog(container);

            return dialog.FileName;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public SKBitmap ProceedProfilePhoto(string path)
        {
            using (var bitmap = SKBitmap.Decode(path))
            {
                if (bitmap == null)
                    throw new BadImageFormatException();

                if (bitmap.Height / bitmap.Width != 1)
                    throw new InvalidImageSizeException(InternalExceptionMessageConstant.ImageMustBeSquare);

                var resizedBitmap = bitmap.Resize(new SKImageInfo(512, 512), SKFilterQuality.High);
                return resizedBitmap;
            }
        }

        /// <summary>
        ///     Build open file dialog.
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        protected virtual OpenFileDialog BuildOpenFileDialog(OpenFileDialogModel options)
        {
            var dialog = new OpenFileDialog();
            if (options != null)
            {
                dialog.Multiselect = options.IsMultipleSelectionSupported;
                dialog.ReadOnlyChecked = options.IsReadOnlyChecked;
                dialog.ShowReadOnly = options.IsReadOnlyShown;
                dialog.AddExtension = options.IsExtensionAdded;
                dialog.CheckFileExists = options.IsFileExistenceChecked;
                dialog.CheckPathExists = options.IsPathExistenceChecked;
                dialog.DefaultExt = options.DefaultExtension;
                dialog.InitialDirectory = options.InitialDirectory;
                dialog.Title = options.Title;
                dialog.Filter = options.Filter;
            }

            return dialog;
        }

        #endregion
    }
}