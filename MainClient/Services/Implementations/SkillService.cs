﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Sodakoq.Main.DataTransferObjects.Skills;
using Sodakoq.Main.Models;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Services.Implementations
{
    public class SkillService : ISkillService
    {
        #region Properties

        /// <summary>
        ///     Http client module for connecting to api end-point.
        /// </summary>
        private readonly HttpClient _httpClient;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize service with injector.
        /// </summary>
        public SkillService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<SearchResult<List<SkillViewModel>>> LoadUserSkillsAsync(LoadSkillViewModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Serialize model to json content.
            var httpContent = JsonConvert.SerializeObject(model);

            // Make request and catch the response.
            var httpResponseMessage = await _httpClient
                .PostAsync(new Uri("api/skill/search", UriKind.Relative),
                    new StringContent(httpContent, Encoding.UTF8, "application/json"),
                    cancellationToken);

            // Content is invalid.
            if (httpResponseMessage == null || httpResponseMessage.Content == null)
                return null;

            var content = await httpResponseMessage.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(content))
                return null;

            return JsonConvert.DeserializeObject<SearchResult<List<SkillViewModel>>>(content);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="skillModel"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<SkillViewModel> AddUserSkillAsync(AddUserSkillViewModel skillModel,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Serialize model to json content.
            var httpContent = JsonConvert.SerializeObject(skillModel);

            // Make request and catch the response.
            var httpResponseMessage = await _httpClient
                .PostAsync(new Uri("api/skill", UriKind.Relative),
                    new StringContent(httpContent, Encoding.UTF8, "application/json"),
                    cancellationToken);

            // Content is invalid.
            if (httpResponseMessage == null || httpResponseMessage.Content == null)
                return null;

            var content = await httpResponseMessage.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(content))
                return null;

            return JsonConvert.DeserializeObject<SkillViewModel>(content);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="skillId"></param>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task DeleteUserSkillAsync(Guid skillId, DeleteSkillViewModel model,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var httpRequestMessage = new HttpRequestMessage();
            httpRequestMessage.Content =
                new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            httpRequestMessage.Method = HttpMethod.Delete;
            httpRequestMessage.RequestUri = new Uri($"api/skill/{skillId}", UriKind.Relative);

            // Send request and get response.
            var httpResponseMessage = await _httpClient.SendAsync(httpRequestMessage, cancellationToken);
            if (!httpResponseMessage.IsSuccessStatusCode)
                throw new Exception("Failed to delete skill");
        }

        #endregion
    }
}