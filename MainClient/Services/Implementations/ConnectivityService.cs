﻿using System.Net;
using Sodakoq.Main.Services.Interfaces;

namespace Sodakoq.Main.Services.Implementations
{
    public class ConnectivityService : IConnectivityService
    {
        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public virtual bool HasInternetConnection()
        {
            try
            {
                // Try connecting to an internet end-point.
                // If there is response, that means user device has internet connection.
                using (var client = new WebClient())
                using (client.OpenRead("http://clients3.google.com/generate_204"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        #endregion
    }
}