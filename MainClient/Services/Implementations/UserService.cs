﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MaterialDesignThemes.Wpf;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Sodakoq.Domain.Models.Contexts;
using Sodakoq.Domain.Models.Entities;
using Sodakoq.Main.Constants;
using Sodakoq.Main.Controllers.Dialogs;
using Sodakoq.Main.DataTransferObjects.Users;
using Sodakoq.Main.Enumerations;
using Sodakoq.Main.Models;
using Sodakoq.Main.Models.CommandArguments;
using Sodakoq.Main.Modules.CommonModule.CaptchaModalModule;
using Sodakoq.Main.Services.Interfaces;
using Sodakoq.Main.Views.Dialogs;

namespace Sodakoq.Main.Services.Implementations
{
    public class UserService : IUserService
    {
        #region Constructors

        /// <summary>
        ///     Initialize service with injectors.
        /// </summary>
        public UserService(HttpClient httpClient, CvManagementDbContext dbContext)
        {
            _httpClient = httpClient;
            _dbContext = dbContext;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Instance for making http request.
        /// </summary>
        private readonly HttpClient _httpClient;

        private readonly CvManagementDbContext _dbContext;

        #endregion

        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="bIsExceptionSuppressed"></param>
        /// <returns></returns>
        public async Task<SearchResult<List<UserViewModel>>> SearchAsync(SearchUserViewModel condition,
            CancellationToken cancellationToken, bool bIsExceptionSuppressed)
        {
            try
            {
                var httpResponseMessage = await _httpClient.PostAsync(
                    new Uri("api/user/search", UriKind.Relative),
                    new StringContent(JsonConvert.SerializeObject(condition)), cancellationToken);

                if (!httpResponseMessage.IsSuccessStatusCode)
                    throw new Exception("Failed to get user descriptions list.");

                var httpContent = await httpResponseMessage.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<SearchResult<List<UserViewModel>>>(httpContent);
            }
            catch
            {
                if (!bIsExceptionSuppressed)
                    throw;

                return new SearchResult<List<UserViewModel>>(new List<UserViewModel>(), 0);
            }
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="basicLoginModel"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<BasicLoginResultViewModel> BasicLoginAsync(BasicLoginViewModel basicLoginModel,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var httpResponseMessage = await _httpClient.PostAsync(
                new Uri("api/user/basic-login", UriKind.Relative),
                new StringContent(JsonConvert.SerializeObject(basicLoginModel), Encoding.UTF8, "application/json"),
                cancellationToken);

            if (!httpResponseMessage.IsSuccessStatusCode)
                throw new Exception("Failed to do basic login");

            var httpContent = await httpResponseMessage.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<BasicLoginResultViewModel>(httpContent);
        }

        /// <summary>
        ///     Update login result asynchronously.
        /// </summary>
        /// <param name="basicLoginResultModel"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<bool> StoreLoginResultIntoDbAsync(BasicLoginResultViewModel basicLoginResultModel,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                // Find existing item with the same key.
                var existingKeyValueItems = _dbContext.KeyValueCacheItems
                    .Where(x => x.Key == DbKeyConstant.Authentication);

                // Remove all existing key value items.
                _dbContext.KeyValueCacheItems.RemoveRange(existingKeyValueItems);

                // Initialize cache key item.
                var item = new KeyValueCacheItem(DbKeyConstant.Authentication,
                    JsonConvert.SerializeObject(basicLoginResultModel), null);

                // Insert the new item into database.
                _dbContext.KeyValueCacheItems.Add(item);

                await _dbContext.SaveChangesAsync(cancellationToken);

                return true;
            }
            catch (Exception exception)
            {
                return false;
            }
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<BasicLoginResultViewModel> LoadLoginResultFromDbAsync(
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Find authorization information from database.
            var item = await _dbContext
                .KeyValueCacheItems
                .FirstOrDefaultAsync(x => x.Key == DbKeyConstant.Authentication, cancellationToken);

            // No stored authorization is stored.
            if (item == null || string.IsNullOrEmpty(item.Value))
                return null;

            return JsonConvert.DeserializeObject<BasicLoginResultViewModel>(item.Value);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task ClearLoginResultFromDbAsync(CancellationToken cancellationToken)
        {
            var items = _dbContext
                .KeyValueCacheItems
                .Where(x => x.Key == DbKeyConstant.Authentication);

            // Delete all items.
            _dbContext.KeyValueCacheItems.RemoveRange(items);
            await _dbContext.SaveChangesAsync(cancellationToken);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="dialogHostContainer"></param>
        /// <param name="captchaKind"></param>
        /// <returns></returns>
        public virtual async Task<ResolveCaptchaResult> ResolveCaptchaAsync(string dialogHostContainer,
            CaptchaKinds captchaKind)
        {
            // Display captcha modal
            var captchaDialogViewModel = new CaptchaModalViewModel();
            captchaDialogViewModel.CaptchaEndPoint = "https://www.google.com";

            var captchaDialogView = new CaptchaDialogView();
            captchaDialogView.DataContext = captchaDialogViewModel;
            var originalResolveCaptchaRawResult = await DialogHost.Show(captchaDialogView);
            if (!(originalResolveCaptchaRawResult is ResolveCaptchaResult resolveCaptchaRawResult))
            {
                resolveCaptchaRawResult = new ResolveCaptchaResult();
                return resolveCaptchaRawResult;
            }

#if DEBUG
            resolveCaptchaRawResult = new ResolveCaptchaResult("123", true, captchaKind);
#endif
            return resolveCaptchaRawResult;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<string> FindUserAccessTokenAsync(CancellationToken cancellationToken)
        {
            // Get access token.
            var accessToken = await _dbContext
                .KeyValueCacheItems
                .Where(x => x.Key.Equals(DbKeyConstant.Authentication,
                    StringComparison.InvariantCultureIgnoreCase))
                .Select(x => x.Value)
                .FirstOrDefaultAsync(cancellationToken);

            // Access is not found.
            if (string.IsNullOrEmpty(accessToken))
                return null;

            return accessToken;
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task<UserViewModel> FindUserProfileAsync(
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var httpResponseMessage = await _httpClient
                .GetAsync(new Uri("api/user/profile", UriKind.RelativeOrAbsolute), cancellationToken);

            var httpContent = httpResponseMessage.Content;
            if (httpContent == null)
                return null;

            var content = await httpContent.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<UserViewModel>(content);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public virtual async Task AddAccessTokenAsync(string accessToken,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            // Initialize new key-value item.
            var item = new KeyValueCacheItem(DbKeyConstant.Authentication, accessToken);

            // Remove item whose key is equal to authentication key.
            var existingKeyValueCacheItems =
                _dbContext.KeyValueCacheItems.Where(x => x.Key == DbKeyConstant.Authentication);
            _dbContext.KeyValueCacheItems.RemoveRange(existingKeyValueCacheItems);

            // Insert new key value item.
            _dbContext.KeyValueCacheItems.Add(item);

            await _dbContext.SaveChangesAsync(cancellationToken);
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <returns></returns>
        public virtual async Task<UserViewModel> UploadProfilePhotoAsync(Guid? userId, byte[] photo,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var data = new MultipartFormDataContent();
            if (userId != null)
                data.Add(new StringContent(userId.Value.ToString("D")), "userId");

            if (photo == null)
                throw new Exception($"{nameof(photo)} is required");

            // Add photo to request body.
            data.Add(new ByteArrayContent(photo), "photo", $"{Guid.NewGuid():D}.png");

            var httpResponseMessage =
                await _httpClient.PostAsync(new Uri("api/user/photo", UriKind.Relative), data,
                    cancellationToken);

            if (httpResponseMessage == null || !httpResponseMessage.IsSuccessStatusCode)
                return null;

            var httpContent = httpResponseMessage.Content;
            if (httpContent == null)
                return null;

            var content = await httpContent.ReadAsStringAsync();
            if (string.IsNullOrEmpty(content))
                return null;

            return JsonConvert.DeserializeObject<UserViewModel>(content);
        }

        #endregion
    }
}