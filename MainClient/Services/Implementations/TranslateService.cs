﻿using Mustache;
using Sodakoq.Main.Services.Interfaces;
using WPFLocalizeExtension.Engine;

namespace Sodakoq.Main.Services.Implementations
{
    public class TranslateService : ITranslateService
    {
        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="originalMessage"></param>
        /// <returns></returns>
        public virtual string LoadTranslatedMessage(string originalMessage)
        {
            var translatedItem = LocalizeDictionary.Instance
                .GetLocalizedObject(originalMessage, null, LocalizeDictionary.Instance.Culture);

            if (translatedItem == null)
                return originalMessage;

            return translatedItem.ToString();
        }

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="originalMessage"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public virtual string LoadTranslatedMessage(string originalMessage, object parameters)
        {
            var translatedMessage = LoadTranslatedMessage(originalMessage);
            if (translatedMessage == null)
                return originalMessage;

            var formatCompiler = new FormatCompiler();
            var templateGenerator = formatCompiler.Compile(translatedMessage);
            return templateGenerator.Render(parameters);
        }

        #endregion
    }
}