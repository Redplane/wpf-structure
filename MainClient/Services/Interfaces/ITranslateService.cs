﻿namespace Sodakoq.Main.Services.Interfaces
{
    public interface ITranslateService
    {
        #region Methods

        /// <summary>
        ///     Load translated message from resource file.
        /// </summary>
        /// <param name="originalMessage"></param>
        /// <returns></returns>
        string LoadTranslatedMessage(string originalMessage);

        /// <summary>
        ///     Load translated message with params replacement.
        /// </summary>
        /// <param name="originalMessage"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        string LoadTranslatedMessage(string originalMessage, object parameters);

        #endregion
    }
}