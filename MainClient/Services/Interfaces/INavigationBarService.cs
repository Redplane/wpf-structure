﻿using System;

namespace Sodakoq.Main.Services.Interfaces
{
    public interface INavigationBarService
    {
        #region Properties

        event EventHandler OnItemSelected;

        #endregion
    }
}