﻿using System.Reactive.Subjects;

namespace Sodakoq.Main.Services.Interfaces
{
    public interface IMessageBusService
    {
        #region Methods

        /// <summary>
        ///     Get message channel.
        /// </summary>
        /// <param name="eventName"></param>
        /// <returns></returns>
        ISubject<object> FindEventSubject(string eventName);

        /// <summary>
        ///     Publish event asynchronously.
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        void PublishEvent(string eventName, object data);

        #endregion
    }
}