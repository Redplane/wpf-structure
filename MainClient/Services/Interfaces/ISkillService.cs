﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sodakoq.Main.DataTransferObjects.Skills;
using Sodakoq.Main.Models;

namespace Sodakoq.Main.Services.Interfaces
{
    public interface ISkillService
    {
        #region Methods

        /// <summary>
        ///     Search skills by using specific conditions.
        /// </summary>
        /// <returns></returns>
        Task<SearchResult<List<SkillViewModel>>> LoadUserSkillsAsync(LoadSkillViewModel model,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Add user skill asynchronously.
        /// </summary>
        /// <param name="skillModel"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<SkillViewModel> AddUserSkillAsync(AddUserSkillViewModel skillModel,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Delete skill and user relationship.
        /// </summary>
        /// <param name="skillId"></param>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task DeleteUserSkillAsync(Guid skillId, DeleteSkillViewModel model,
            CancellationToken cancellationToken = default(CancellationToken));
        
        #endregion
    }
}