﻿namespace Sodakoq.Main.Services.Interfaces
{
    public interface IConnectivityService
    {
        #region Methods

        /// <summary>
        ///     Check whether device has internet connection or not.
        /// </summary>
        /// <returns></returns>
        bool HasInternetConnection();

        #endregion
    }
}