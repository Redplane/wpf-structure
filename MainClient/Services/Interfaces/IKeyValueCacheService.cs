﻿namespace Sodakoq.Main.Services.Interfaces
{
    public interface IKeyValueCacheService
    {
        #region Methods

        /// <summary>
        ///     Add or update in-memory storage item.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        void AddOrUpdateItem(string key, object value);

        /// <summary>
        ///     Get in-memory stored item.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        T GetItem<T>(string key);

        /// <summary>
        ///     Remove in-memory item.
        /// </summary>
        /// <param name="key"></param>
        void RemoveItem(string key);

        #endregion
    }
}