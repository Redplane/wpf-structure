﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sodakoq.Main.DataTransferObjects.SkillCategories;
using Sodakoq.Main.Models;
using Sodakoq.Main.Models.Searches;
using Sodakoq.Main.Models.SkillCategories;

namespace Sodakoq.Main.Services.Interfaces
{
    public interface ISkillCategoryService
    {
        #region Methods

        /// <summary>
        ///     Load skill category with their related skills.
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<SearchResult<List<SkillCategoryViewModel>>> LoadCategorizedSkillsAsync(LoadCategorizedSkillModel condition,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Load user skill categories asynchronously.
        /// </summary>
        Task<SearchResult<List<SkillCategoryViewModel>>> LoadUserSkillCategoriesAsync(
            LoadUserSkillCategoryModel condition, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Add skill category asynchronously.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<SkillCategoryViewModel> AddSkillCategoryAsync(AddEditSkillCategoryModel model,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Edit skill category asynchronously.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<SkillCategoryViewModel> EditSkillCategoryAsync(AddEditSkillCategoryModel model,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Delete skill category asynchronously.
        /// </summary>
        /// <param name="skillCategoryId"></param>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<SkillCategoryViewModel> DeleteSkillCategoryAsync(Guid skillCategoryId, DeleteSkillCategoryModel model,
            CancellationToken cancellationToken = default(CancellationToken));

        #endregion
    }
}