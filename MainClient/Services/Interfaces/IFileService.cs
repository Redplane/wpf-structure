﻿using System.Windows;
using SkiaSharp;
using Sodakoq.Main.Models;

namespace Sodakoq.Main.Services.Interfaces
{
    public interface IFileService
    {
        #region Methods

        /// <summary>
        ///     Select a file synchronously.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        string SelectFile(Window container = null,
            OpenFileDialogModel options = default(OpenFileDialogModel));

        /// <summary>
        ///     Load image bytes asynchronously.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        SKBitmap ProceedProfilePhoto(string path);

        #endregion
    }
}