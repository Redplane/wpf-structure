﻿using System.Windows;

namespace Sodakoq.Main.Services.Interfaces
{
    public interface IValidationService
    {
        #region Methods

        bool IsControlValid(DependencyObject dependencyObject);

        #endregion
    }
}