﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sodakoq.Main.DataTransferObjects.Users;
using Sodakoq.Main.Enumerations;
using Sodakoq.Main.Models;
using Sodakoq.Main.Models.CommandArguments;

namespace Sodakoq.Main.Services.Interfaces
{
    public interface IUserService
    {
        #region Methods

        /// <summary>
        ///     Search for users using specific conditions.
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="bIsExceptionSuppressed"></param>
        /// T
        /// <returns></returns>
        Task<SearchResult<List<UserViewModel>>> SearchAsync(SearchUserViewModel condition, CancellationToken cancellationToken,
            bool bIsExceptionSuppressed = false);

        /// <summary>
        ///     Basic login asynchronously.
        /// </summary>
        /// <param name="basicLoginModel"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<BasicLoginResultViewModel> BasicLoginAsync(BasicLoginViewModel basicLoginModel,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Update login result asynchronously.
        /// </summary>
        /// <param name="basicLoginModel"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<bool> StoreLoginResultIntoDbAsync(BasicLoginResultViewModel basicLoginModel,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Load login result from database asynchronously.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<BasicLoginResultViewModel> LoadLoginResultFromDbAsync(
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Clear login result from database asynchronously.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task ClearLoginResultFromDbAsync(CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Resolve captcha base on kind.
        /// </summary>
        /// <param name="dialogHostContainer"></param>
        /// <param name="captchaKind"></param>
        /// <returns></returns>
        Task<ResolveCaptchaResult> ResolveCaptchaAsync(string dialogHostContainer, CaptchaKinds captchaKind);

        /// <summary>
        ///     Get user profile by exchanging local stored information with profile information.
        /// </summary>
        /// <returns></returns>
        Task<string> FindUserAccessTokenAsync(CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Exchange access token with user profile.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<UserViewModel> FindUserProfileAsync(CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Add access token to local database.
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task AddAccessTokenAsync(string accessToken, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Upload profile photo asynchronously.
        /// </summary>
        /// <returns></returns>
        Task<UserViewModel> UploadProfilePhotoAsync(Guid? userId, byte[] photo,
            CancellationToken cancellationToken = default(CancellationToken));

        #endregion
    }
}