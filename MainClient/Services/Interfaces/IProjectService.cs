﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Sodakoq.Main.DataTransferObjects.Projects;
using Sodakoq.Main.DataTransferObjects.Skills;
using Sodakoq.Main.Models;
using Sodakoq.Main.Models.Projects;

namespace Sodakoq.Main.Services.Interfaces
{
    public interface IProjectService
    {
        #region Methods

        /// <summary>
        ///     Load projects asynchronously.
        /// </summary>
        /// <param name="conditions"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<SearchResult<List<ProjectViewModel>>> LoadProjectsAsync(LoadProjectViewModel conditions,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Load skilled projects asynchronously.
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<SearchResult<List<SkilledProjectViewModel>>> LoadSkilledProjectsAsync(LoadProjectUsedSkillModel condition,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Delete project asynchronously.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<bool> DeleteProjectAsync(Guid id, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        ///     Add project asynchronously.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<ProjectViewModel> AddProjectAsync(AddProjectViewModel model,
            CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Add skills into project asynchronously.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<List<SkillViewModel>> UpdateProjectSkillsAsync(AddSkillIntoProjectViewModel model, CancellationToken cancellationToken = default(CancellationToken));

            /// <summary>
        ///     Edit project asynchronously.
        /// </summary>
        /// <returns></returns>
        Task<ProjectViewModel> EditProjectAsync(Guid projectId, EditProjectViewModel model,
            CancellationToken cancellationToken = default(CancellationToken));

        #endregion
    }
}