﻿using System;

namespace Sodakoq.Main.DataTransferObjects.Skills
{
    public class AddUserSkillViewModel
    {
        #region Properties

        /// <summary>
        ///     Id of user that skill needs adding to.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        ///     Name of skill.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Skill point.
        /// </summary>
        public int Point { get; set; }

        /// <summary>
        ///     Skill category that skill should be attached to.
        /// </summary>
        public Guid SkillCategoryId { get; set; }

        #endregion

        #region Constructor

        public AddUserSkillViewModel()
        {
        }

        public AddUserSkillViewModel(Guid userId, string name, int point, Guid skillCategoryId)
        {
            UserId = userId;
            Name = name;
            Point = point;
            SkillCategoryId = skillCategoryId;
        }

        #endregion
    }
}