﻿using System;

namespace Sodakoq.Main.DataTransferObjects.Skills
{
    public class SkillViewModel
    {
        #region Properties

        public Guid Id { get; set; }

        public string Name { get; set; }

        public double CreatedTime { get; set; }

        #endregion

        #region Constructor

        public SkillViewModel()
        {
        }

        public SkillViewModel(Guid id, string name, double createdTime)
        {
            Id = id;
            Name = name;
            CreatedTime = createdTime;
        }

        #endregion
    }
}