﻿using System.Collections.Generic;
using Sodakoq.Main.DataTransferObjects.Skills;

namespace Sodakoq.Main.DataTransferObjects.Projects
{
    public class SkilledProjectViewModel : ProjectViewModel
    {
        #region Constructor

        public SkilledProjectViewModel()
        {
            Skills = new List<SkillViewModel>();
        }

        #endregion

        #region Properties

        /// <summary>
        ///     List of used skills.
        /// </summary>
        public List<SkillViewModel> Skills { get; set; }

        #endregion
    }
}