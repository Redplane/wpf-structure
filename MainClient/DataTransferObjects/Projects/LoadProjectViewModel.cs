﻿using System;
using System.Collections.Generic;

namespace Sodakoq.Main.DataTransferObjects.Projects
{
    public class LoadProjectViewModel
    {
        #region Properties

        /// <summary>
        ///     Id of projects.
        /// </summary>
        public HashSet<Guid> Ids { get; set; }

        /// <summary>
        ///     User id that projects belong to.
        /// </summary>
        public Guid? UserId { get; set; }

        /// <summary>
        ///     Pagination information.
        /// </summary>
        public PaginationViewModel Pagination { get; set; }

        #endregion
    }
}