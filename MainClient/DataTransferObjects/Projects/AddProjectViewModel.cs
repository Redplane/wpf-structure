﻿using System.Collections.Generic;

namespace Sodakoq.Main.DataTransferObjects.Projects
{
    public class AddProjectViewModel
    {
        #region Properties

        public string Name { get; set; }

        public string Description { get; set; }

        public double StartedTime { get; set; }

        public double? FinishedTime { get; set; }

        public List<string> Responsibilities { get; set; }

        #endregion

        #region Constructor

        public AddProjectViewModel()
        {
        }

        public AddProjectViewModel(string name, string description, double startedTime, double? finishedTime,
            List<string> responsibilities)
        {
            Name = name;
            Description = description;
            StartedTime = startedTime;
            FinishedTime = finishedTime;
            Responsibilities = responsibilities;
        }

        #endregion
    }
}