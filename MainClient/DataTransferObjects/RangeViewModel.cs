﻿namespace Sodakoq.Main.DataTransferObjects
{
    public class RangeViewModel<TFrom, TTo>
    {
        #region Properties

        /// <summary>
        ///     Time from which records will be filtered.
        /// </summary>
        public TFrom From { get; set; }

        /// <summary>
        ///     Time to which records will be filtererd.
        /// </summary>
        public TTo To { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialize time range with no setting.
        /// </summary>
        public RangeViewModel()
        {
        }

        /// <summary>
        ///     Initialize time range with injectors.
        /// </summary>
        /// <param name="from"></param>
        public RangeViewModel(TFrom from)
        {
            From = from;
        }

        /// <summary>
        ///     Initialize time range with settings.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public RangeViewModel(TFrom from, TTo to)
        {
            From = from;
            To = to;
        }

        #endregion
    }
}