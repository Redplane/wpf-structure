﻿using System;
using System.Collections.Generic;
using Sodakoq.Main.DataTransferObjects.Skills;

namespace Sodakoq.Main.DataTransferObjects.SkillCategories
{
    public class SkillCategoryViewModel
    {
        #region Properties

        public Guid Id { get; set; }

        public string Name { get; set; }

        public double CreatedTime { get; set; }

        public List<SkillViewModel> Skills { get; set; }

        #endregion

        #region Constructor

        public SkillCategoryViewModel()
        {
            Skills = new List<SkillViewModel>();
        }

        public SkillCategoryViewModel(Guid id, string name, double createdTime) : this()
        {
            Id = id;
            Name = name;
            CreatedTime = createdTime;
        }

        #endregion
    }
}