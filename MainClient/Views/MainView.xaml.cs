﻿using System.Windows.Controls;

namespace Sodakoq.Main.Views
{
    /// <summary>
    ///     Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : Page
    {
        #region Constructors

        /// <summary>
        ///     Initialize view components.
        /// </summary>
        public MainView()
        {
            InitializeComponent();
        }

        #endregion
    }
}