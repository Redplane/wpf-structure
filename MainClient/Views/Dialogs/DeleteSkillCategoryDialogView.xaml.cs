﻿using System.Windows.Controls;

namespace Sodakoq.Main.Views.Dialogs
{
    /// <summary>
    ///     Interaction logic for DeleteSkillCategoryDialogView.xaml
    /// </summary>
    public partial class DeleteSkillCategoryDialogView : UserControl
    {
        public DeleteSkillCategoryDialogView()
        {
            InitializeComponent();
        }
    }
}