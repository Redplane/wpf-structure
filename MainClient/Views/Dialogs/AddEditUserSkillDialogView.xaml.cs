﻿using System.Windows.Controls;

namespace Sodakoq.Main.Views.Dialogs
{
    /// <summary>
    ///     Interaction logic for AddEditUserSkillDialogView.xaml
    /// </summary>
    public partial class AddEditUserSkillDialogView : UserControl
    {
        public AddEditUserSkillDialogView()
        {
            InitializeComponent();
        }
    }
}