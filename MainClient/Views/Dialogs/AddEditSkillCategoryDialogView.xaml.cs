﻿using System.Windows.Controls;

namespace Sodakoq.Main.Views.Dialogs
{
    /// <summary>
    ///     Interaction logic for AddEditSkillCategoryDialogView.xaml
    /// </summary>
    public partial class AddEditSkillCategoryDialogView : UserControl
    {
        public AddEditSkillCategoryDialogView()
        {
            InitializeComponent();
        }
    }
}