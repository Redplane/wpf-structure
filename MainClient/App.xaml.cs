﻿using System.Net.Http;
using System.Windows;
using System.Windows.Threading;
using CefSharp;
using CefSharp.Wpf;

namespace Sodakoq.Main
{
    /// <summary>
    ///     Interaction logic for AppContext.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Methods

        /// <summary>
        ///     <inheritdoc />
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            var settings = new CefSettings();
            settings.BrowserSubprocessPath = @"x86\CefSharp.BrowserSubprocess.exe";
            settings.SetOffScreenRenderingBestPerformanceArgs();
            settings.CefCommandLineArgs.Add("disable-gpu-vsync", "1");
#if DEBUG
            settings.IgnoreCertificateErrors = true;
#endif

            Cef.Initialize(settings, false, null);

            base.OnStartup(e);

            // Initialize exception handler.
            DispatcherUnhandledException += OnDispatcherUnhandledException;
        }

        /// <summary>
        ///     Called when dispatcher dispatches an unhandled exception.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="dispatcherUnhandledExceptionEventArgs"></param>
        protected virtual void OnDispatcherUnhandledException(object sender,
            DispatcherUnhandledExceptionEventArgs dispatcherUnhandledExceptionEventArgs)
        {
            var exception = dispatcherUnhandledExceptionEventArgs.Exception;
            if (exception is HttpRequestException httpRequestException)
            {
            }
        }

        #endregion
    }
}